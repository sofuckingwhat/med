﻿using System.ComponentModel;

namespace Domain.Enums
{
    /// <summary>
    /// Половая принадлежность
    /// </summary>
    public enum Gender
    {
        /// <summary>
        /// Пол не указан.
        /// Нулевое значение поля.
        /// </summary>
        [Description("Неизвестно")]
        Unknown = 0,
        /// <summary>
        /// Женский пол
        /// </summary>
        [Description("Женский")]
        Female,
        /// <summary>
        /// Мужской пол
        /// </summary>
        [Description("Мужской")]
        Male
    }
}
