﻿using System.Collections.Generic;

namespace Domain.Abstract
{
    public interface IResponseList<T>
    {
        int Page { get; set; }
        int PerPage { get; set; }
        int TotalPages { get; }
        int TotalItems { get; set; }
        IEnumerable<T> Items { get; set; }
    }
}
