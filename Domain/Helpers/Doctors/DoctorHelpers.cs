﻿using Domain.CQRS.Commands.Doctors;
using Domain.Entities.Doctors;

namespace Domain.Helpers.Doctors
{
    /// <summary>
    /// Вспомогательные методы для докторов
    /// </summary>
    public static class DoctorHelpers
    {
        /// <summary>
        /// Обновить данные доктора.
        /// </summary>
        /// <param name="doctor">Entity доктора</param>
        /// <param name="command">Данные</param>
        /// <returns></returns>
        public static Doctor UpdateDoctorData(this Doctor doctor, UpdateDoctorCommand command)
        {
            doctor.Fio = command.Fio;

            doctor.Updated = System.DateTime.Now;

            return doctor;
        }
    }
}
