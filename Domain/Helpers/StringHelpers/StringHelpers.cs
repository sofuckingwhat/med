﻿using System.Text.RegularExpressions;
using NickBuhro.Translit;

namespace Domain.Helpers.StringHelpers
{
    /// <summary>
    /// Вспомогательные методы для работы со строками
    /// </summary>
    public static class StringHelpers
    {
        /// <summary>
        /// Транслируем русский текст в латинскую раскладку
        /// Пример: Привет мир => privet-mir
        /// </summary>
        /// <param name="source">Исходная строка</param>
        /// <returns>string</returns>
        public static string TranslitToEnglish(this string source)
        {
            if (string.IsNullOrEmpty(source.Trim()))
            {
                return "";
            }
            var latin = Transliteration.CyrillicToLatin(source, Language.Russian);

            var rgx = new Regex("[^a-zA-Z0-9 -]");
            var cleanLatin = rgx.Replace(latin, "");

            var result = cleanLatin.ToLower().Replace(' ', '-');
            return result;
        }

    }
}
