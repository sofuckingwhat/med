﻿using System.Linq;
using Domain.ViewModels.Common;

namespace Domain.Helpers.Address
{
    /// <summary>
    /// Вспомогательные методы для адреса.
    /// </summary>
    public static class AddressHelper
    {
        /// <summary>
        /// Проверка адреса на заполнение.
        /// Если все поля адреса пустые, считаем его не заполненным
        /// </summary>
        /// <param name="adr">DTO Адреса</param>
        /// <returns></returns>
        public static bool AddressIsNullOrEmpty(this AddressDto adr)
        {
            var fields = string.Concat(new[] { adr.Region, adr.City, adr.Street, adr.Building, adr.Room }.Where(s => !string.IsNullOrEmpty(s)));

            return string.IsNullOrEmpty(fields);
        }
    }
}