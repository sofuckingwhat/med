﻿namespace Domain.Helpers.DateTime
{
    /// <summary>
    /// Вспомогательные методы для работы с датой и временем
    /// </summary>
    public static class DateHelpers
    {
        /// <summary>
        /// Конвертирует дату в формат для input type date.
        /// 1984-12-31
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string ToHtmlInputDateFormat(this System.DateTime date)
        {

            return date.ToString("yyyy-MM-dd");
        }
    }
}
