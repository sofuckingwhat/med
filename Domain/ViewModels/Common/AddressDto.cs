﻿using System;
using Domain.Entities.Common;

namespace Domain.ViewModels.Common
{
    public class AddressDto
    {
        #region Properties

        public string Id { get; set; }
        public string Address { get; set; }
        public string Region { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }
        public string Room { get; set; }
        public string AddressType { get; set; }
        public string AddressTypeId { get; set; }

        #endregion

        #region Constructors

        public AddressDto() { }

        public AddressDto(Address address)
        {
            if (address == null) address = new Address();

            Id = address.Id == Guid.Empty ? "" : address.Id.ToString();
            Region = address.Region;
            City = address.City;
            Street = address.Street;
            Building = address.Building;
            Room = address.Room;
            AddressType = address.AddressType?.DictionaryFullValue;
            AddressTypeId = address.AddressTypeId.ToString() ?? "";
        }
        #endregion
    }
}