﻿using System;
using Domain.Entities.Patients;
using Domain.Helpers.DateTime;

namespace Domain.ViewModels.Common
{
    public class PolisDto
    {
        #region Properties

        public string Id { get; set; }
        public string Number { get; set; }
        public string IssuedDate { get; set; }
        public string Issuer { get; set; }
        public string IssuerId { get; set; }

        #endregion

        #region Constructors

        public PolisDto() { }

        public PolisDto(Polis p)
        {
            if (p == null) p = new Polis();
            Id = p.Id == Guid.Empty ? "" : p.Id.ToString();
            Number = p.Number;
            IssuedDate = p.IssuedDate == DateTime.MinValue ? "" : p.IssuedDate.ToHtmlInputDateFormat();
            Issuer = p.IssuedCompany?.DictionaryFullValue;
            IssuerId = p.IssuerCompanyId == Guid.Empty ? "" : p.IssuerCompanyId.ToString();
        }

        #endregion
    }
}