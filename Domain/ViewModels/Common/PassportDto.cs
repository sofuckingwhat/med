﻿using System;
using Domain.Entities.Patients;
using Domain.Helpers.DateTime;

namespace Domain.ViewModels.Common
{
    public class PassportDto
    {
        #region Properties

        public string Id { get; set; }
        public string Series { get; set; }
        public string Number { get; set; }
        public string IssuedDate { get; set; }
        public string IssuerDepartment { get; set; }
        public string DepartmentCode { get; set; }
        #endregion

        #region Constructors

        public PassportDto() { }

        public PassportDto(Passport p)
        {
            if (p == null) p = new Passport();

            Id = p.Id == Guid.Empty ? "" : p.Id.ToString();
            Series = p.Series;
            Number = p.Number;
            IssuedDate = p.IssuedDate == DateTime.MinValue ? "" : p.IssuedDate.ToHtmlInputDateFormat();
            IssuerDepartment = p.DepartmentName;
            DepartmentCode = p.DepartmentCode;
        }

        #endregion

    }
}
