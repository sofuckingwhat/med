﻿using System;
using Domain.Entities.Dictionaries;

namespace Domain.ViewModels.Dictionaries
{
    /// <summary>
    /// Модель записи в словаре
    /// </summary>
    public class DictionaryItemViewModel
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string ShortName { get; set; }
        public string Code { get; set; }
        public int SortOrder { get; set; }
        public DictionaryItemViewModel()
        {

        }

        public DictionaryItemViewModel(DictionaryItem item)
        {
            Id = item.Id;
            FullName = item.DictionaryFullValue;
            ShortName = item.DictionaryShortValue;
            Code = item.Code804;
            SortOrder = item.SortOrder;
        }
    }
}