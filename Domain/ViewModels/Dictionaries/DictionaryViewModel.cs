﻿using System;
using Domain.Entities.Dictionaries;

namespace Domain.ViewModels.Dictionaries
{
    /// <summary>
    /// Модель представления словаря для списков.
    /// Не включает в себя информацию о записях.
    /// </summary>
    public class DictionaryViewModel
    {
        #region Properties

        /// <summary>
        /// Идентификатор справочника
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Наименование справочника
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Техническое наименование справочника
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Описание справочника
        /// </summary>
        public string Description { get; set; }

        #endregion

        #region Constructors

        public DictionaryViewModel() { }

        /// <summary>
        /// Создать модель на основе сущности справочника
        /// </summary>
        /// <param name="dic">Сущность справочника</param>
        public DictionaryViewModel(Dictionary dic)
        {
            Id = dic.Id;
            Title = dic.Title;
            Name = dic.Name;
            Description = dic.Description ?? "";
        }

        #endregion
    }
}