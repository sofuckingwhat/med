﻿using System.Collections.Generic;

namespace Domain.ViewModels.Dictionaries
{
    /// <summary>
    /// Список справочников
    /// </summary>
    public class DictionariesListViewModel
    {
        public IEnumerable<DictionaryViewModel> DictionaryViewModels { get; set; }
    }
}