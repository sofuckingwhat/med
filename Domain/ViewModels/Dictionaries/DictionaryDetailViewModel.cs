﻿using System.Collections.Generic;
using System.Linq;
using Domain.Entities.Dictionaries;

namespace Domain.ViewModels.Dictionaries
{
    /// <inheritdoc />
    /// <summary>
    /// Модель словаря для просмотра на детальной страницы
    /// </summary>
    public class DictionaryDetailViewModel : DictionaryViewModel
    {
        #region Properties

        /// <summary>
        /// Содержимое справочника
        /// </summary>
        public List<DictionaryItemViewModel> Items { get; set; }

        #endregion

        #region Constructors

        public DictionaryDetailViewModel() { }

        /// <summary>
        /// Создание модели справочника на основании сущности    
        /// </summary>
        /// <param name="d">Сущность словаря</param>
        public DictionaryDetailViewModel(Dictionary d)
        {
            Id = d.Id;
            Title = d.Title;
            Description = d.Description;
            Name = d.Name;
            Items = d.Items?
                        .Where(i => !i.IsDeleted)
                        .Select(i => new DictionaryItemViewModel(i))
                        .OrderBy(dic=>dic.SortOrder)
                        .ToList() ??
                    new List<DictionaryItemViewModel>();
        }

        #endregion
    }
}