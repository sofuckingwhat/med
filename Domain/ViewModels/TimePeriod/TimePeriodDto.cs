﻿using System;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Domain.ViewModels.TimePeriod
{
    public class TimePeriodDto
    {
        #region Properties

        public Guid PeriodId { get; set; }
        /// <summary>
        /// Время начала периода
        /// </summary>
        public TimeSpan Start { get; set; }

        /// <summary>
        /// Время окончания периода
        /// </summary>
        public TimeSpan End { get; set; }

        /// <summary>
        /// Тип периода
        /// </summary>
        public Guid PeriodType { get; set; }

        /// <summary>
        /// Длительность периода в минутах
        /// </summary>
        public double Duration => (End - Start).TotalMinutes;

        #endregion

        #region Constructors

        public TimePeriodDto() {}

        public TimePeriodDto(Entities.Common.TimePeriod p)
        {
            PeriodId = p.Id;
            Start = p.Start;
            End = p.End;
            PeriodType = p.PeriodTypeId;
        }

        #endregion
    }
}