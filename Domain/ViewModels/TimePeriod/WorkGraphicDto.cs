﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain.ViewModels.TimePeriod
{
    public class WorkGraphicDto
    {
        /// <summary>
        /// Ссылка на объект, для которого предназначены интервалы
        /// </summary>
        public Guid ObjectId { get; set; }

        /// <summary>
        /// Дата всех интервалов времени
        /// </summary>
        public DateTime PeriodDate { get; set; }

        /// <summary>
        /// Интервалы времени на дату
        /// </summary>
        public List<TimePeriodDto> WorkTime { get; set; }

        /// <summary>
        /// Всего рабочих часов
        /// </summary>
        public TimeSpan TotalHours => TotalWorkTime();

        #region Constructors

        public WorkGraphicDto()
        {
            WorkTime = new List<TimePeriodDto>();
        }

        public WorkGraphicDto(Guid objectId, IReadOnlyCollection<Entities.Common.TimePeriod> periods)
        {
            if(!periods.Any()) return;

            ObjectId = objectId;

            PeriodDate = periods.FirstOrDefault()?.OnDate ?? DateTime.Today;

            WorkTime = periods.Select(p => new TimePeriodDto(p)).ToList();
        }
        #endregion

        /// <summary>
        /// Всего рабочих часов
        /// </summary>
        /// <returns></returns>
        private TimeSpan TotalWorkTime()
        {
            var totalMinutes = WorkTime.Select(p => p.Duration).Sum();

            return TimeSpan.FromMinutes(totalMinutes);
        }
    }
}
