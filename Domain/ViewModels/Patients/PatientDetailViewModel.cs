﻿using System.Collections.Generic;
using System.Linq;
using Domain.Entities.Common;
using Domain.Entities.Patients;
using Domain.Helpers.DateTime;
using Domain.ViewModels.Common;

namespace Domain.ViewModels.Patients
{
    public class PatientDetailViewModel : PatientItemViewModel
    {
        #region Properties

        public string Phones { get; set; }
        public string Email { get; set; }
        public string Inn { get; set; }
        public string Snils { get; set; }

        public List<AddressDto> Address { get; set; }
        public List<PolisDto> Polises { get; set; }
        public PassportDto Passport { get; set; }

        public List<NamedComment> CommentList { get; set; }

        #endregion

        #region Constructors

        public PatientDetailViewModel() { }

        public PatientDetailViewModel(Patient p) : base(p)
        {
            AmbulanceCard = p.ClinicUID;
            Phones = p.Phones;
            Email = p.Email;
            Inn = p.Inn;
            Snils = p.Snils;
            Polises = p.Polises.Any()
                      ? p.Polises.Select(x => new PolisDto(x)).ToList()
                      : new List<PolisDto>();

            Address = p.Addresses.Any()
                ? p.Addresses.Select(a => new AddressDto(a)).ToList()
                : new List<AddressDto>();

            Birthday = p.BirthDay.ToHtmlInputDateFormat();
            Passport = new PassportDto(p.Passport);
        }

        #endregion


    }
}