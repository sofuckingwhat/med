﻿using System;
using Domain.Entities.Patients;
using MediatR;
using MongoDB.Bson.Serialization.Attributes;

namespace Domain.ViewModels.Patients
{
    public class PatientItemViewModel : INotification
    {
        #region Properties

        [BsonId]
        public Guid Id { get; set; }
        /// <summary>
        /// Фамилия Имя Отчество пациента
        /// </summary>
        public string Fio { get; set; }
        /// <summary>
        /// Возраст / полных лет
        /// </summary>
        public int Age { get; set; }
        /// <summary>
        /// Пол
        /// </summary>
        public string Gender { get; set; }
        /// <summary>
        /// Дата рождения 
        /// </summary>
        public string Birthday { get; set; }
        /// <summary>
        /// Номер амбулаторной карты
        /// </summary>
        public string AmbulanceCard { get; set; }
        /// <summary>
        /// Дата регистрации
        /// </summary>
        public string Registered { get; set; }
        /// <summary>
        /// Источник информации о клиники
        /// </summary>
        public string InformSource { get; set; }
        /// <summary>
        /// Комментарий
        /// </summary>
        public string Comments { get; set; }

        #endregion

        #region Constructors

        public PatientItemViewModel() { }

        public PatientItemViewModel(Patient p)
        {
            Id = p.Id;
            Fio = p.Fio;
            Age = (DateTime.Today.Year - p.BirthDay.Year);
            Gender = p.Gender.ToString();
            Birthday = p.BirthDay.ToLongDateString();
            AmbulanceCard = p.ClinicUID;
            Registered = p.Created.ToShortDateString();
            InformSource = p.SourceOfInformation.DictionaryFullValue;
            Comments = p.Description;
        }

        #endregion
    }
}
