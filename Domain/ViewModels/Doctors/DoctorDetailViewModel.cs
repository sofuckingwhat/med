﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Entities.Common;
using Domain.Entities.Doctors;
using Domain.ViewModels.TimePeriod;
using MediatR;

namespace Domain.ViewModels.Doctors
{
    public class DoctorDetailDto : DoctorListItemDto, INotification
    {
        public List<WorkGraphicDto> WorkGraphic { get; set; }
        #region Constructors

        public DoctorDetailDto()
        {
            WorkGraphic = new List<WorkGraphicDto>();
        }

        public DoctorDetailDto(Doctor doc)
        {
            Id = doc.Id;
            Fio = doc.Fio;

            WorkGraphic = CreateGraphic(Id, doc.WorkTimes.ToList());
        }

        #endregion

        #region PrivateMethods

        private static List<WorkGraphicDto> CreateGraphic(Guid id, IReadOnlyCollection<Entities.Common.TimePeriod> periods)
        {
            var result = new List<WorkGraphicDto>();
            if (!periods.Any())
            {
                return result;
            }

            var periodsGroupByDate = periods.GroupBy(p => p.OnDate)
                .Select(g => new
                {
                    Date = g.Key,
                    Periods = g.Select(p => p).ToList()
                });

            result.AddRange(periodsGroupByDate.Select(workDay => new WorkGraphicDto(id, workDay.Periods)));

            return result;
        }

        #endregion
    }
}