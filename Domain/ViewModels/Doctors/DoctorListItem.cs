﻿using System;
using MediatR;
using MongoDB.Bson.Serialization.Attributes;

namespace Domain.ViewModels.Doctors
{
    public class DoctorListItemDto
    {
        public Guid Id { get; set; }
        public string Fio { get; set; }
    }
}
