﻿using System;
using System.Collections.Generic;
using Domain.Abstract;

namespace Domain.Implementations
{
    public class ResponseList<T> : IResponseList<T>
    {
        public int Page { get; set; }
        public int PerPage { get; set; }
        public int TotalPages => GetTotalPages();
        public int TotalItems { get; set; }
        public IEnumerable<T> Items { get; set; }

        /// <summary>
        /// Возвращает количество страниц, для заданного числа записей
        /// </summary>
        /// <returns></returns>
        private int GetTotalPages()
        {
            if (TotalItems <= 0 || PerPage <= 0) return 0;

            return (int)Math.Ceiling((decimal)TotalItems / PerPage);
        }
    }
}
