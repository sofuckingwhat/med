﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.CQRS.Commands.TimePeriods;
using Domain.Entities.Dictionaries;
using Domain.Entities.Doctors;
using Domain.ViewModels.TimePeriod;

namespace Domain.Entities.Common
{
    /// <summary>
    /// Интервал времени
    /// </summary>
    public class TimePeriod : EntityBase
    {
        /// <summary>
        /// Период на дату
        /// </summary>
        public DateTime OnDate { get; set; }
        /// <summary>
        /// Начало интервала
        /// </summary>
        public TimeSpan Start { get; set; }

        /// <summary>
        /// Конец интервала
        /// </summary>
        public TimeSpan End { get; set; }

        /// <summary>
        /// Управляющее свойство.
        /// Указатель на доктора.
        /// </summary>
        public Guid DoctorId { get; set; }

        /// <summary>
        /// Длительность
        /// </summary>
        public TimeSpan Duration => End - Start;

        /// <summary>
        /// Навигационное свойство типа интервала
        /// </summary>
        public Guid PeriodTypeId { get; set; }

        /// <summary>
        /// Тип интервала
        /// </summary>
        [ForeignKey("PeriodTypeId")]
        public DictionaryItem PeriodType { get; set; }

        /// <summary>
        /// Доктор
        /// </summary>
        [ForeignKey("DoctorId")]
        public Doctor Doctor { get; set; }

        #region Constructors

        public TimePeriod() {}

        public TimePeriod(TimePeriodDto p, AddPeriodCommand common)
        {
            DoctorId = common.Id;
            OnDate = common.Date;
            Start = p.Start;
            End = p.End;
            PeriodTypeId = p.PeriodType;
        }

        #endregion
    }
}