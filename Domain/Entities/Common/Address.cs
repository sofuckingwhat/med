﻿using System;
using Domain.Entities.Dictionaries;
using Domain.Entities.Patients;
using Domain.ViewModels.Common;

namespace Domain.Entities.Common
{
    /// <summary>
    /// Информация о адресе
    /// </summary>
    public class Address : EntityBase
    {
        #region Properties

        /// <summary>
        /// Почтовый индекс
        /// </summary>
        public string Index { get; set; }

        /// <summary>
        /// Страна
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Регион / край / область
        /// </summary>
        public string Region { get; set; }

        /// <summary>
        /// Город / населенный пункт
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Улица / проспект
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// Дом / строение
        /// </summary>
        public string Building { get; set; }

        /// <summary>
        /// Квартира
        /// </summary>
        public string Room { get; set; }

        /// <summary>
        /// Координата широты
        /// </summary>
        public string Latitude { get; set; }

        /// <summary>
        /// Координата долготы
        /// </summary>
        public string Longitude { get; set; }

        /// <summary>
        /// Навигационное свойство типа адреса
        /// </summary>
        public Guid AddressTypeId { get; set; }

        /// <summary>
        /// Тип адреса (прописка/домашний/рабочий)
        /// Значения берутся из справочника.
        /// </summary>
        public DictionaryItem AddressType { get; set; }


        /// <summary>
        /// Ссылка на человека
        /// </summary>
        public Person Person { get; set; }

        public Guid PersonId { get; set; }

        #endregion

        #region Constructors

        public Address() { }

        public Address(AddressDto a, Guid personId)
        {
            Country = "Russia";
            Region = a.Region;
            City = a.City;
            Street = a.Street;
            Building = a.Building;
            Room = a.Room;

            AddressTypeId = Guid.Parse(a.AddressTypeId);
            PersonId = personId;
        }

        #endregion

        #region Public properties

        /// <summary>
        /// Обновить данные о адресе пациента
        /// </summary>
        /// <param name="a"></param>
        public void Update(AddressDto a)
        {
            Country = "Russia";
            Region = a.Region;
            City = a.City;
            Street = a.Street;
            Building = a.Building;
            Room = a.Room;

            AddressTypeId = Guid.Parse(a.AddressTypeId);
            Updated = DateTime.Now;
        }

        #endregion
    }
}
