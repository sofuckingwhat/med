﻿using System;
using Domain.Entities.Patients;

namespace Domain.Entities.Common
{
    /// <summary>
    /// Запись о комментарии
    /// </summary>
    public class NamedComment : EntityBase
    {
        /// <summary>
        /// Дата сообщения
        /// </summary>
        public DateTime CommentDate { get; set; }

        /// <summary>
        /// Автор сообщения
        /// </summary>
        public Person Author { get; set; }

        /// <summary>
        /// Текст сообщения
        /// </summary>
        public string Text { get; set; }
    }
}