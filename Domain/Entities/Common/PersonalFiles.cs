﻿namespace Domain.Entities.Common
{
    public class PersonalFiles : EntityBase
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public int FileSize { get; set; }
    }
}