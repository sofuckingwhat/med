﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.CQRS.Commands.Dictionaries;
using Domain.Helpers.StringHelpers;

namespace Domain.Entities.Dictionaries
{
    public class Dictionary : EntityBase
    {
        #region Properties

        /// <summary>
        /// Наименование справочника
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Код по приказу:
        /// Приказ Минздрава РФ от 13.10.2017 N 804Н
        /// </summary>
        public string Code804H { get; set; }
        /// <summary>
        /// Уникальное наименование справочника для поиска
        /// </summary>
        [Required(ErrorMessage = "Поле 'name' не может быть пустым")]
        public string Name { get; set; }
        /// <summary>
        /// Описание справочника
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Содержимое справочника
        /// </summary>
        public List<DictionaryItem> Items { get; set; }

        #endregion

        #region Constructors

        public Dictionary() { }

        public Dictionary(CreateDictionaryCommand d)
        {
            Title = d.Title;
            Description = d.Description;
            Code804H = d.Title;
            Name = d.Title.TranslitToEnglish();
        }
        #endregion

        #region Public methods

        /// <summary>
        /// Обновляем данные словаря
        /// </summary>
        /// <param name="c"></param>
        public void UpdateDictionaryValues(UpdateDictionaryCommand c)
        {
            Title = c.Name;
            Description = c.Description;
        }

        #endregion

        #region Private methods



        #endregion
    }
}