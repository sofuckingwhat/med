﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.CQRS.Commands.Dictionaries;

namespace Domain.Entities.Dictionaries
{
    /// <summary>
    /// Модель записи справочника
    /// </summary>
    public class DictionaryItem : EntityBase
    {
        /// <summary>
        /// Наименование справочника
        /// </summary>
        [ForeignKey("DictionaryId")]
        public Dictionary DictionaryName { get; set; }

        /// <summary>
        /// Навигационное свойство словаря
        /// </summary>
        public Guid DictionaryId { get; set; }

        /// <summary>
        /// Полное наименование значения справочника
        /// </summary>
        public string DictionaryFullValue { get; set; }

        /// <summary>
        /// Краткое наименование значения справочника
        /// </summary>
        public string DictionaryShortValue { get; set; }

        /// <summary>
        /// Приказ Минздрава РФ от 13.10.2017 N 804Н
        /// Код услуги
        /// </summary>
        public string Code804 { get; set; }

        #region Constructors

        public DictionaryItem() { }

        public DictionaryItem(CreateDictionaryItemCommand di)
        {
            DictionaryShortValue = di.ShortName;
            DictionaryFullValue = di.FullName;
            Code804 = di.Code;
        }

        #endregion

        public void UpdateEntity(UpdateDictionaryItemCommand c)
        {
            DictionaryFullValue = c.FullName;
            DictionaryShortValue = c.ShortName;
            Code804 = c.Code;
        }
    }
}
