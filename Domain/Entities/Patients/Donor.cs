﻿using Domain.Entities.Common;

namespace Domain.Entities.Patients
{
    /// <inheritdoc />
    /// <summary>
    /// Пациент "Донор"
    /// </summary>
    public sealed class Donor : Person
    {
        /// <summary>
        /// Анонимный донор
        /// </summary>
        public bool IsConfidential { get; set; }

        /// <summary>
        /// Код донора по каталогу
        /// </summary>
        public string DonorCode { get; set; }

        /// <summary>
        /// Фоточки донора, для показа реципиентам
        /// </summary>
        public PersonalFiles Photos { get; set; }
    }
}