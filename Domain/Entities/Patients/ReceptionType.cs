﻿using System.ComponentModel.DataAnnotations;
using Domain.Entities.Dictionaries;

namespace Domain.Entities.Patients
{
    /// <summary>
    /// Тип приема
    /// </summary>
    public class ReceptionType : EntityBase
    {
        /// <summary>
        /// Наименование приема
        /// </summary>
        public DictionaryItem ReceptionTypeName { get; set; }

        /// <summary>
        /// Не прямая ссылка на пациента.
        /// Не является ForeignKey на пациента.
        /// </summary>
        [Required(ErrorMessage = "Не указан пациент.")]
        public string Patient { get; set; }
    }
}