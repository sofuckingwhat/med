﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Domain.CQRS.Commands.Patients;
using Domain.Entities.Common;
using Domain.Entities.Dictionaries;
using Domain.Helpers.Address;
using Domain.ViewModels.Common;

namespace Domain.Entities.Patients
{
    /// <inheritdoc />
    /// <summary>
    /// Данные о пациенте
    /// </summary>
    public sealed class Patient : Person
    {
        #region Properties

        /// <summary>
        /// Номер для картотеки клиники.
        /// Должен быть уникальным.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public string ClinicUID { get; set; }

        /// <summary>
        /// Список телефонов в формате строки,
        /// где разделителем телефонов является ';'.
        /// 79008887766;5554433;221100 и так далее. 
        /// </summary>
        public string Phones { get; set; }

        /// <summary>
        /// Адрес электронной почты
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Комментарии к пациенту
        /// </summary>
        public List<NamedComment> NamedComments { get; set; }

        /// <summary>
        /// Партнер пациента
        /// </summary>
        public Person Partner { get; set; }

        /// <summary>
        /// Заметки, комментарии, описание.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Откуда вы узнали о клинике?
        /// </summary>
        [ForeignKey("SourceOfInformationGuid")]
        public DictionaryItem SourceOfInformation { get; set; }

        /// <summary>
        /// Ссылка на ключ справочника
        /// </summary>
        public Guid SourceOfInformationGuid { get; set; }

        #endregion

        #region Constructors

        public Patient() { }

        /// <summary>
        /// Первичное создание записи о пациенте до амбулаторной карты
        /// </summary>
        /// <param name="c">Команда на создание пациента</param>
        public Patient(CreatePatientCommand c)
        {
            SplitFio(c.Fio);

            Phones = c.Phones;
            Description = c.Comment;
            BirthDay = c.Birthday;
            Gender = c.Gender;
            Passport = new Passport();

            // Если уж все валидаторы пропустили это, то бд выдаст ошибку!
            if (c.SourceOfInformation != null)
            {
                SourceOfInformationGuid = c.SourceOfInformation.Value;
            }

        }

        #endregion

        #region Public methods

        /// <summary>
        /// Обновить данные о пациенте
        /// </summary>
        /// <param name="c"></param>
        public void UpdatePatient(UpdatePatientCommand c)
        {
            SplitFio(c.Fio);

            Phones = c.Phones;
            Description = c.Comment;
            BirthDay = c.Birthday;
            Gender = c.Gender;

            Email = c.Email;
            Inn = c.Inn;
            Snils = c.Snils;
            ClinicUID = c.AmbulanceCard;
            Passport.UpdatePassport(c.Passport, c.Id);

            UpdatePolisesList(c.Polises);
            UpdateAddresses(c.Addresses);

            Updated = DateTime.Now;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Заполняем ФИО пациента
        /// </summary>
        /// <param name="fio"></param>
        private void SplitFio(string fio)
        {
            var result = fio.Split(' ');

            if (result.Length < 3)
            {
                throw new Exception("Нужно обязательно указывать ФИО полностью");
            }

            Fio = fio;

            Surname = result[0] ?? "";
            FirstName = result[1] ?? "";
            MiddleName = result[2] ?? "";
        }

        /// <summary>
        /// Обновить данные полисов пациента
        /// </summary>
        /// <param name="polises">Входящие полиса</param>
        private void UpdatePolisesList(IEnumerable<PolisDto> polises)
        {
            if (Polises == null)
            {
                Polises = new List<Polis>();
            }

            foreach (var dto in polises)
            {
                if (string.IsNullOrEmpty(dto.Id))
                {
                    Polises.Add(new Polis(dto, Id));
                    continue;
                }

                var polis = Polises.FirstOrDefault(p => p.Id == Guid.Parse(dto.Id));

                if (polis != null)
                {
                    polis.Update(dto, Id);
                }
                else
                {
                    Polises.Add(new Polis(dto, Id));
                }
            }
        }

        /// <summary>
        /// Обновить данные адреса пациента
        /// </summary>
        /// <param name="addressList"></param>
        private void UpdateAddresses(IEnumerable<AddressDto> addressList)
        {
            if (Addresses == null)
            {
                Addresses = new List<Address>();
            }

            foreach (var dto in addressList.Where(a => !a.AddressIsNullOrEmpty()))
            {
                if (string.IsNullOrEmpty(dto.Id))
                {
                    Addresses.Add(new Address(dto, Id));
                    continue;
                }

                var adr = Addresses.FirstOrDefault(a => a.Id == Guid.Parse(dto.Id));

                if (adr != null)
                {
                    adr.Update(dto);
                }
                else
                {
                    Addresses.Add(new Address(dto, Id));
                }
            }
        }

        #endregion
    }
}