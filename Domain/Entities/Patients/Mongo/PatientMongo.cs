﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace Domain.Entities.Patients.Mongo
{
    /// <summary>
    /// Раскрытая модель пациента
    /// </summary>
    public class PatientMongo
    {
        [BsonId]
        public string Id { get; set; }
        /// <summary>
        /// Дата регистрации
        /// </summary>
        public DateTime Register { get; set; }
        /// <summary>
        /// ФИО пациента
        /// </summary>
        public string Fio { get; set; }
        /// <summary>
        /// Дата рождения
        /// </summary>
        public string Birthday { get; set; }
        /// <summary>
        /// Ссылка на сущность пациента
        /// </summary>
        public string PatientId { get; set; }
    }
}
