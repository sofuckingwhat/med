﻿using System;
using Domain.ViewModels.Common;

namespace Domain.Entities.Patients
{

    /// <summary>
    /// Данные паспорта
    /// </summary>
    public class Passport : EntityBase
    {

        #region Properties
        /// <summary>
        /// Серия
        /// </summary>
        public string Series { get; set; }

        /// <summary>
        /// Номер
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Дата выдачи
        /// </summary>
        public DateTime IssuedDate { get; set; }

        /// <summary>
        /// Код подразделения
        /// </summary>
        public string DepartmentCode { get; set; }

        /// <summary>
        /// Кем выдан
        /// </summary>
        public string DepartmentName { get; set; }

        /// <summary>
        /// Ссылка на человека
        /// </summary>
        public Person Person { get; set; }

        /// <summary>
        /// ID человека
        /// </summary>
        public Guid PersonId { get; set; }

        #endregion

        #region Constructors

        public Passport() { }

        #endregion

        #region Public methods

        public void UpdatePassport(PassportDto p, Guid personId)
        {
            Series = p.Series;
            Number = p.Number;

            if (!string.IsNullOrEmpty(p.IssuedDate))
            {
                IssuedDate = DateTime.Parse(p.IssuedDate);
            }

            DepartmentName = p.IssuerDepartment;
            DepartmentCode = p.DepartmentCode;
            PersonId = personId;
        }

        #endregion
    }
}