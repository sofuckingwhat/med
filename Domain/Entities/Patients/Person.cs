﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Entities.Common;
using Domain.Enums;

namespace Domain.Entities.Patients
{
    /// <summary>
    /// Модель описания персональных данных о человеке
    /// </summary>
    public class Person : EntityBase
    {
        #region Properties

        public string Fio { get; set; }
        /// <summary>
        /// Фамилия
        /// </summary>
        public string Surname { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime BirthDay { get; set; }

        /// <summary>
        /// Половая принадлежность
        /// </summary>
        public Gender Gender { get; set; }

        /// <summary>
        /// Ссылка на паспортные данные
        /// </summary>
        public Guid? PassportId { get; set; }

        /// <summary>
        /// Инн
        /// </summary>
        public string Inn { get; set; }

        /// <summary>
        /// СНИЛС
        /// </summary>
        public string Snils { get; set; }

        /// <summary>
        /// Паспортные данные
        /// </summary>
        [ForeignKey("PassportId")]
        public Passport Passport { get; set; }

        /// <summary>
        /// Полисы медицинского страхования
        /// </summary>
        public ICollection<Polis> Polises { get; set; }

        /// <summary>
        /// Адреса
        /// </summary>
        public ICollection<Address> Addresses { get; set; }

        #endregion

        #region Constructors

        public Person()
        {
            Addresses = new HashSet<Address>();
        }

        #endregion
    }
}
