﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Entities.Dictionaries;
using Domain.ViewModels.Common;

namespace Domain.Entities.Patients
{
    public class Polis : EntityBase
    {

        #region Properties

        /// <summary>
        /// Номер полиса
        /// </summary>
        public string Number { get; set; }
        /// <summary>
        /// Дата выдачи
        /// </summary>
        public DateTime IssuedDate { get; set; }
        /// <summary>
        /// Страховая компания
        /// </summary>
        [ForeignKey("IssuerCompanyId")]
        public DictionaryItem IssuedCompany { get; set; }

        public Guid IssuerCompanyId { get; set; }
        /// <summary>
        /// Ссылка на владельца полиса
        /// </summary>
        public Person Person { get; set; }

        [ForeignKey("Person")]
        public Guid PersonGuid { get; set; }

        #endregion

        #region Constructors

        public Polis() { }

        public Polis(PolisDto p, Guid personId)
        {
            Number = p.Number;
            IssuedDate = DateTime.Parse(p.IssuedDate);
            IssuerCompanyId = Guid.Parse(p.IssuerId);
            PersonGuid = personId;
        }

        #endregion

        #region Public methods

        public void Update(PolisDto d, Guid personId)
        {
            Number = d.Number;
            IssuedDate = DateTime.Parse(d.IssuedDate);
            IssuerCompanyId = Guid.Parse(d.IssuerId);
            PersonGuid = personId;

            Updated = DateTime.Now;
        }

        #endregion
    }
}