﻿using System;

namespace Domain.Entities
{
    public class EntityBase
    {
        /// <summary>
        /// Идентификатор сущности
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime Created { get; set; } = DateTime.Now;

        /// <summary>
        /// Дата изменения
        /// </summary>
        public DateTime Updated { get; set; } = DateTime.Now;

        /// <summary>
        /// Пометка на удаление
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Идентификатор автора записи
        /// </summary>
        public Guid AuthorId { get; set; }

        /// <summary>
        /// Фио автора записи
        /// </summary>
        public string AuthorName { get; set; }

        /// <summary>
        /// Идентификатор пользователя, кто правил запись
        /// </summary>
        public Guid UpdatedBuId { get; set; }

        /// <summary>
        /// Фио пользователя кто правил запись
        /// </summary>
        public string UpdatedByName { get; set; }

        /// <summary>
        /// Индекс сортировки
        /// </summary>
        public int SortOrder { get; set; }

    }
}