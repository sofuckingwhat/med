﻿using System;
using System.Collections.Generic;
using Domain.CQRS.Commands.Doctors;
using Domain.Entities.Common;
using Domain.Entities.Patients;

namespace Domain.Entities.Doctors
{
    public class Doctor : Person
    {

        #region Properties

        public ICollection<TimePeriod> WorkTimes { get; set; }

        #endregion

        #region Constructors

        public Doctor()
        {
            WorkTimes = new HashSet<TimePeriod>();
        }

        /// <summary>
        /// Создание доктора из команды
        /// </summary>
        /// <param name="c">Модель создания доктора</param>
        public Doctor(CreateDoctorCommand c)
        {
            Fio = c.Fio;

            Created = DateTime.Now;
        }

        #endregion

    }
}
