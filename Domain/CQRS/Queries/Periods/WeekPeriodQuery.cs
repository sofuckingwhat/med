﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Domain.ViewModels.TimePeriod;
using MediatR;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Domain.CQRS.Queries.Periods
{
    public class WeekPeriodQuery: IRequest<IEnumerable<WorkGraphicDto>>
    {
        /// <summary>
        /// Идентификатор объекта
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Начало периода
        /// </summary>
        [JsonConverter(typeof(IsoDateTimeConverter))]
        public DateTime From { get; set; }
        /// <summary>
        /// Конец периода
        /// </summary>
        [JsonConverter(typeof(IsoDateTimeConverter))]
        public DateTime To { get; set; }
    }
}
