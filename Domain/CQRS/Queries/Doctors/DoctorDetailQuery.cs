﻿using System;
using Domain.ViewModels.Doctors;
using MediatR;

namespace Domain.CQRS.Queries.Doctors
{
    public class DoctorDetailQuery : IRequest<DoctorDetailDto>
    {
        public Guid Id { get; set; }
    }
}