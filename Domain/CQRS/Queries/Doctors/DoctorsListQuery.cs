﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Abstract;
using Domain.ViewModels.Doctors;
using MediatR;

namespace Domain.CQRS.Queries.Doctors
{
    /// <summary>
    /// Запрос списка докторов
    /// </summary>
    public class DoctorsListQuery : IRequest<IResponseList<DoctorListItemDto>>
    {
        public int Page { get; set; } = 0;
        public int PerPage { get; set; } = 20;
    }
}
