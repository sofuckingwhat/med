﻿using Domain.Abstract;
using Domain.ViewModels.Patients;
using MediatR;

namespace Domain.CQRS.Queries.Patients
{
    /// <summary>
    /// Запрос списка пациентов
    /// </summary>
    public class PatientsListQuery : IRequest<IResponseList<PatientItemViewModel>>
    {
        public int Page { get; set; } = 0;
        public int PerPage { get; set; } = 20;
    }
}