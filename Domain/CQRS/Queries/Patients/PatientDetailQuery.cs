﻿using System;
using Domain.ViewModels.Patients;
using MediatR;

namespace Domain.CQRS.Queries.Patients
{
    public class PatientDetailQuery : IRequest<PatientDetailViewModel>
    {
        public Guid Id { get; set; }
    }
}