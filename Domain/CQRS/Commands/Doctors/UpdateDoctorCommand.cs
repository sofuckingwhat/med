﻿using System;
using Domain.ViewModels.Doctors;
using MediatR;

namespace Domain.CQRS.Commands.Doctors
{
    public class UpdateDoctorCommand : CreateDoctorCommand
    {
        public Guid Id { get; set; }
    }
}