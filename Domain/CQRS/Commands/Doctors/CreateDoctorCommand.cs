﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.ViewModels.Doctors;
using MediatR;

namespace Domain.CQRS.Commands.Doctors
{
    /// <summary>
    /// Команда на создание сущности доктора
    /// </summary>
    public class CreateDoctorCommand : IRequest<DoctorDetailDto>
    {
        /// <summary>
        /// Фамилия Имя Отчество доктора
        /// </summary>
        public string Fio { get; set; }
    }
}
