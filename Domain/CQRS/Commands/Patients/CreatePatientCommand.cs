﻿using System;
using Domain.Enums;
using Domain.ViewModels.Patients;
using MediatR;

namespace Domain.CQRS.Commands.Patients
{
    /// <summary>
    /// Создание нового пациента
    /// </summary>
    public class CreatePatientCommand : IRequest<PatientItemViewModel>
    {
        #region Properties

        /// <summary>
        /// ФИО
        /// </summary>
        public string Fio { get; set; }
        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime Birthday { get; set; }
        /// <summary>
        /// Пол пациента
        /// </summary>
        public Gender Gender { get; set; }
        /// <summary>
        /// Телефоны для связи
        /// </summary>
        public string Phones { get; set; }
        /// <summary>
        /// Источник информации о клинике
        /// </summary>
        public Guid? SourceOfInformation { get; set; }
        /// <summary>
        /// Комментарий
        /// </summary>
        public string Comment { get; set; }

        #endregion
    }
}
