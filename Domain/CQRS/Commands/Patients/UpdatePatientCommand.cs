﻿using System;
using System.Collections.Generic;
using Domain.ViewModels.Common;
using Domain.ViewModels.Patients;
using MediatR;

namespace Domain.CQRS.Commands.Patients
{
    public class UpdatePatientCommand : CreatePatientCommand, IRequest<PatientDetailViewModel>
    {
        public Guid Id { get; set; }
        public string AmbulanceCard { get; set; }
        public string Email { get; set; }
        public string Inn { get; set; }
        public string Snils { get; set; }
        public PassportDto Passport { get; set; }
        public List<AddressDto> Addresses { get; set; }
        public List<PolisDto> Polises { get; set; }

    }
}