﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.ViewModels.TimePeriod;
using MediatR;

namespace Domain.CQRS.Commands.TimePeriods
{
    public class AddPeriodCommand : IRequest<WorkGraphicDto>
    {
        /// <summary>
        /// Идентификатор объекта периодов
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Дата периодов
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Периоды времени
        /// </summary>
        public List<TimePeriodDto> Periods { get; set; }
    }
}
