﻿using System;
using Domain.ViewModels.Dictionaries;
using MediatR;

namespace Domain.CQRS.Commands.Dictionaries
{
    public class DeleteDictionaryItemCommand : IRequest<DictionaryItemViewModel>
    {
        public Guid Id { get; set; }
    }
}