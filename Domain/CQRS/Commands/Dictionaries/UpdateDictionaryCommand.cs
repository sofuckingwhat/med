﻿using System;
using Domain.ViewModels.Dictionaries;
using MediatR;

namespace Domain.CQRS.Commands.Dictionaries
{
    public class UpdateDictionaryCommand : IRequest<DictionaryViewModel>
    {
        /// <summary>
        /// Если нужно обновить данные словаря, то указываем
        /// идентификатор справочника
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название справочника
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание справочника
        /// </summary>
        public string Description { get; set; }

    }
}