﻿using Domain.ViewModels.Dictionaries;
using MediatR;

namespace Domain.CQRS.Commands.Dictionaries
{
    public class CreateDictionaryCommand : IRequest<DictionaryViewModel>
    {
        /// <summary>
        /// Название справочника
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Описание справочника
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Приказ Минздрава РФ от 13.10.2017 N 804Н
        /// Код классификатора.
        /// </summary>
        public string Code { get; set; }
    }
}