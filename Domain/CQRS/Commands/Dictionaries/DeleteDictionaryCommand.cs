﻿using System;
using Domain.ViewModels.Dictionaries;
using MediatR;

namespace Domain.CQRS.Commands.Dictionaries
{
    public class DeleteDictionaryCommand : IRequest<DictionaryViewModel>
    {
        public Guid Id { get; set; }
    }
}