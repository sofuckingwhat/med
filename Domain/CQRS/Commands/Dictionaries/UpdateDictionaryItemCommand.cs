﻿using System;
using Domain.ViewModels.Dictionaries;
using MediatR;

namespace Domain.CQRS.Commands.Dictionaries
{
    public class UpdateDictionaryItemCommand : IRequest<DictionaryItemViewModel>
    {
        public Guid Id { get; set; }
        public string ShortName { get; set; }
        public string FullName { get; set; }
        public string Code { get; set; }
    }
}