﻿using System;
using Domain.ViewModels.Dictionaries;
using MediatR;

namespace Domain.CQRS.Commands.Dictionaries
{
    public class CreateDictionaryItemCommand : IRequest<DictionaryItemViewModel>
    {
        public Guid DictionaryId { get; set; }
        public string ShortName { get; set; }
        public string FullName { get; set; }
        public string Code { get; set; }
    }
}