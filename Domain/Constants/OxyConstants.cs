﻿namespace Domain.Constants
{
    /// <summary>
    /// Основные константы приложения
    /// </summary>
    public class OxyConstants
    {
        /// <summary>
        /// Константы кросс доменных политик
        /// </summary>
        public static readonly OxyPoliciesNames Policies = new OxyPoliciesNames();
    }

    /// <summary>
    /// Константы кросс доменных политик
    /// </summary>
    public class OxyPoliciesNames
    {
        /// <summary>
        /// string local
        /// </summary>
        public const string Local = "local";

        /// <summary>
        /// production
        /// </summary>
        public const string Production = "production";

        /// <summary>
        /// get_sts_config
        /// </summary>
        public const string GetStsConfig = "get_sts_config";
    }
}