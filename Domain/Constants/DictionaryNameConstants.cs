﻿namespace Domain.Constants
{
    /// <summary>
    /// Наименования справочников
    /// </summary>
    public static class DictionaryNameConstants
    {
        /// <summary>
        /// Справочник типа адреса
        /// </summary>
        public const string AddressDictionaryName = "Типы почтовых адресов";

        /// <summary>
        /// Справочник видов приема пациентов
        /// </summary>
        public const string ReceptionTypeName = "Типы приема пациентов";

        /// <summary>
        /// Источник информации о клинике
        /// </summary>
        public const string SourceOfInformation = "Источники информации";

        /// <summary>
        /// Справочник страховых компаний
        /// </summary>
        public const string InsuranceCompanies = "Страховые компании";

        /// <summary>
        /// Типы рабочего времени
        /// </summary>
        public const string TimePeriodType = "Тип рабочего времени";

    }
}