﻿namespace Domain.Configurations
{
    public class MongoSettings
    {
        /// <summary>
        /// Строка подключения к базе данных
        /// </summary>
        public string ConnectionString { get; set; }
        /// <summary>
        /// База данных пациентов
        /// </summary>
        public string PatientsDb { get; set; }

        /// <summary>
        /// Данные по рабочим графикам и времени в целом
        /// </summary>
        public string WorkTimeDb { get; set; }

        /// <summary>
        /// Доноры
        /// </summary>
        public string Donors { get; set; }
    }
}
