﻿using System;
using System.IO;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using Data_Access;
using Data_Access.Identity;
using Domain.Constants;
using identity_infrastructure.Models;
using identity_infrastructure.Services;
using identity_infrastructure.Validators;
using IdentityServer4.Services;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;

namespace identity_server_4
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            Env = env;
        }

        public IConfiguration Configuration { get; }
        private IHostingEnvironment Env { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            var migrationAssembly = typeof(OxyDataContext).GetTypeInfo().Assembly.GetName().Name;
            var connection = Configuration["ConnectionStrings:IdentityDataBase"];
            var certPath = Path.Combine(Env.ContentRootPath, "oxyCert.pfx");

            Log.Logger.Information($"Путь к сертификату : {certPath}");
            Log.Logger.Information($"Переменная окружения: {Env.EnvironmentName}");
            Log.Logger.Information($"Строка подключения к БД: {connection}");

            #region Certificate

            X509Certificate2 cert = null;

            using (var store = new X509Store(StoreName.My, StoreLocation.CurrentUser))
            {
                store.Open(OpenFlags.ReadOnly);

                var cerColl = store.Certificates.Find(X509FindType.FindBySerialNumber, "‎00 8f a5 1c af 2e ed 99 4a", false);

                cert = cerColl.Any() ? cerColl[0] : new X509Certificate2(certPath, "***");
            }

            #endregion

            #region Databases

            services.AddDbContext<OxyIdentityDbContext>(builder =>
                // dotnet ef migrations add InitialIdentityServerMigration - c OxyIdentityDbContext -v
                builder.UseNpgsql(connection, optionsBuilder => optionsBuilder.MigrationsAssembly(migrationAssembly))
            );

            services.AddIdentity<OxyUser, IdentityRole>()
                .AddEntityFrameworkStores<OxyIdentityDbContext>();


            #endregion

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 8;
                options.Password.RequiredUniqueChars = 3;
            });

            services.AddIdentityServer(options =>
                {
                    options.IssuerUri = "https://localhost:8086";
                    options.Events.RaiseErrorEvents = true;
                    options.Events.RaiseFailureEvents = true;
                    options.Events.RaiseInformationEvents = true;
                    options.Events.RaiseSuccessEvents = true;

                })
                .AddOperationalStore(options =>
                {
                    // dotnet ef migrations add InitialIdentityServerMigration -c PersistedGrantDbContext -v
                    options.ConfigureDbContext = builder =>
                        builder.UseNpgsql(connection, optionsBuilder => optionsBuilder.MigrationsAssembly(migrationAssembly));
                })
                .AddConfigurationStore(options =>
                {
                    // dotnet ef migrations add InitialIdentityServerMigration -c ConfigurationDbContext
                    options.ConfigureDbContext = builder =>
                        builder.UseNpgsql(connection, optionsBuilder => optionsBuilder.MigrationsAssembly(migrationAssembly));
                })
                .AddAspNetIdentity<OxyUser>()
                .AddProfileService<OxyProfileService>()
                .AddSigningCredential(cert);

            services.AddMvc();

            #region Policies

            services.AddCors(options =>
            {
                options.AddPolicy(OxyPoliciesNames.GetStsConfig,
                    builder =>
                    {
                        builder.WithOrigins("http://localhost:5001", "http://10.20.10.26:8080");
                        builder.AllowAnyHeader();
                        builder.AllowAnyMethod();
                    });
            });

            #endregion

            // Подключаем логгер
            services.AddSingleton(x => Log.Logger);

            services.AddTransient<AccountService>();
            services.AddTransient<IResourceOwnerPasswordValidator, ResourceOwnerPasswordValidator>();
            services.AddTransient<IProfileService, OxyProfileService>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCors(OxyPoliciesNames.GetStsConfig);
            app.UseIdentityServer();
            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();
            InitIdentity(app);
        }

        /// <summary>
        /// Инициализация базы данных.
        /// </summary>
        /// <param name="app"></param>
        private void InitIdentity(IApplicationBuilder app)
        {
            using (var scope = app.ApplicationServices.CreateScope())
            {
                try
                {
                    OxyIdentityServerInitializer.Initialize(scope, Configuration).Wait();
                }
                catch (Exception e)
                {
                    var logger = scope.ServiceProvider.GetRequiredService<ILogger<Startup>>();
                    logger.LogError(e, "При инициализации сервера авторизации возникло исключение");
                }
            }
        }
    }
}
