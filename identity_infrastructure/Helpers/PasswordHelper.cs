﻿using System;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace identity_infrastructure.Helpers
{
    public static class PasswordHelper
    {
        /// <summary>
        /// Хэширует строку, также как это делает identity в случае с паролем.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string HashPbkdf2(this string str)
        {
            var salt = new byte[128 / 8];

            using (var rnd = RandomNumberGenerator.Create())
            {
                rnd.GetBytes(salt);
            }
            return Convert.ToBase64String(KeyDerivation.Pbkdf2(str, salt, KeyDerivationPrf.HMACSHA1, 10000, 256 / 8));
        }
    }
}
