﻿using Microsoft.AspNetCore.Identity;

namespace identity_infrastructure.Models
{
    public class OxyUser : IdentityUser
    {
        public string Fio { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsOwner { get; set; }

    }
}