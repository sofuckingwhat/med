﻿using System.Collections.Generic;
using identity_infrastructure.Resources;
using IdentityServer4;
using IdentityServer4.Models;
using Microsoft.Extensions.Configuration;

namespace identity_infrastructure.InMemory
{
    public class ClientsFactory
    {
        public static IEnumerable<Client> Get(IConfiguration config)
        {
            return new List<Client>()
            {
                new Client
                {
                    ClientId = OxyClients.MisUi,
                    ClientName = "Медицинская информационная система OXY",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowedCorsOrigins = { config["Redirect:Mis:Origin"] },
                    RedirectUris = { config["Redirect:Mis:Callback"], config["Redirect:Mis:SilentCallback"], config["Redirect:Mis:Origin"] },
                    PostLogoutRedirectUris = { config["Redirect:Mis:Origin"], config["Redirect:Mis:Logout"] },
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email,
                        OxyConstantsNames.Company,
                        OxyConstantsNames.Fio,
                        OxyConstantsNames.Position,
                        OxyConstantsNames.MisApi,
                        OxyConstantsNames.Role,
                        OxyConstantsNames.Permission
                    },
                    AllowAccessTokensViaBrowser = true,
                    AllowOfflineAccess = true,
                    AlwaysSendClientClaims = true,
                    AlwaysIncludeUserClaimsInIdToken = true,
                    AccessTokenType = AccessTokenType.Reference,
                    AccessTokenLifetime = 15*60*1000, // 15минут
                    RequireConsent = true
                },
                // Локальный клиент UI
                new Client
                {
                    ClientId = OxyClients.MisUiLocal,
                    ClientName = "Медицинская информационная система OXY",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowedCorsOrigins = { config["Redirect:MisLocal:Origin"] },
                    RedirectUris = { config["Redirect:MisLocal:Callback"], config["Redirect:MisLocal:SilentCallback"], config["Redirect:MisLocal:Origin"] },
                    PostLogoutRedirectUris = { config["Redirect:MisLocal:Origin"], config["Redirect:MisLocal:Logout"] },
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email,
                        OxyConstantsNames.Company,
                        OxyConstantsNames.Fio,
                        OxyConstantsNames.Position,
                        OxyConstantsNames.MisApi,
                        OxyConstantsNames.Role,
                        OxyConstantsNames.Permission
                    },
                    AllowAccessTokensViaBrowser = true,
                    AlwaysIncludeUserClaimsInIdToken = true,
                    AllowOfflineAccess = true,
                    AlwaysSendClientClaims = true,
                    AccessTokenType = AccessTokenType.Reference,
                    AccessTokenLifetime = 30*60*1000,
                    RequireConsent = false
                },
                new Client
                {
                    ClientId = "openIdTestMvc",
                    ClientName = "Приложение для тестирования сервера идентификации пользователя.",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email,
                        "role",
                    },
                    RedirectUris = new List<string> { "http://10.20.10.26:8089/signin-oidc" },
                    PostLogoutRedirectUris = new List<string> {
                        "http://10.20.10.26:8089/",
                        "http://10.20.10.26:8089/signout-callback-oidc"
                    }
                }
            };
        }
    }
}
