﻿using System.Collections.Generic;
using System.Security.Claims;
using IdentityModel;
using IdentityServer4.Test;

namespace identity_infrastructure.InMemory
{
    public class UsersFactory
    {
        public static IEnumerable<TestUser> Get()
        {
            return new List<TestUser>()
            {
                new TestUser()
                {
                    SubjectId = "72358B30-CEDC-4426-9A3B-A6244B7107E5",
                    Username = "Root",
                    Password = "userPassword",
                    Claims = new List<Claim>()
                    {
                        new Claim(JwtClaimTypes.Email, "jfk@oxy.ru"),
                        new Claim(JwtClaimTypes.Role, "admin")
                    }
                }
            };
        }
    }
}