﻿using System.Collections.Generic;
using identity_infrastructure.Resources;
using IdentityModel;
using IdentityServer4.Models;

namespace identity_infrastructure.InMemory
{
    public class ResourcesFactory
    {
        public static IEnumerable<IdentityResource> Get()
        {
            return new List<IdentityResource>()
            {
                new IdentityResource {
                    Name = "openid",
                    DisplayName = "Персональный идентификатор",
                    Required = true,
                    Enabled = true
                },
                new IdentityResource {
                    Name = "profile",
                    DisplayName = "Данные пользователя",
                    Required = true,
                    Enabled = true
                },
                new IdentityResource{
                    Name = "email",
                    DisplayName = "Адрес эл. почты",
                    Enabled = true,
                    UserClaims =
                    {
                        JwtClaimTypes.Email
                    }
                },
                new IdentityResource()
                {
                    Name = "role",
                    DisplayName = "Должность",
                    UserClaims = new List<string>(){OxyConstantsNames.Role}
                },
                new IdentityResource()
                {
                    Name = "company",
                    DisplayName = "Информация о компании",
                    UserClaims = new List<string>()
                    {
                        OxyConstantsNames.Role,
                        OxyConstantsNames.Company
                    }
                },
                new IdentityResource()
                {
                    Name = "permission",
                    DisplayName = "Привилегии",
                    UserClaims = new List<string>()
                    {
                        OxyConstantsNames.Permission
                    }
                },
                new IdentityResource()
                {
                    Name = "fio",
                    DisplayName = "Фамилия Имя Отчество",
                    UserClaims = new List<string>()
                    {
                        OxyConstantsNames.Fio
                    }
                },
                new IdentityResource()
                {
                    Name = "position",
                    DisplayName = "Должность",
                    UserClaims = new List<string>()
                    {
                        OxyConstantsNames.Position
                    }
                },
                new IdentityResource()
                {
                    Name = OxyConstantsNames.MisFrontend,
                    DisplayName = "Доступ к МИС",
                    UserClaims = new List<string>()
                    {
                        OxyConstantsNames.Role,
                        OxyConstantsNames.Admin,
                        OxyConstantsNames.User,
                        OxyConstantsNames.Company,
                        OxyConstantsNames.MisApi,
                        OxyConstantsNames.MisApiRead,
                        OxyConstantsNames.MisApiWrite,
                        OxyConstantsNames.Permission,
                        OxyConstantsNames.Fio,
                        OxyConstantsNames.Position,
                        JwtClaimTypes.Name,
                        JwtClaimTypes.Email
                    }
                }
            };
        }
    }
}