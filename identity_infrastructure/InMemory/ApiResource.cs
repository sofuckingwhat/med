﻿using System.Collections.Generic;
using identity_infrastructure.Resources;
using IdentityModel;
using IdentityServer4.Models;

namespace identity_infrastructure.InMemory
{
    public class ApiResourceFactory
    {
        public static IEnumerable<ApiResource> Get()
        {
            return new List<ApiResource>()
            {
                new ApiResource
                {
                    Name = OxyScopes.MisApiScopeName,
                    DisplayName = "API - Медицинская информационная система",
                    Description = "Mis Api",
                    UserClaims = new List<string>
                    {
                        JwtClaimTypes.Name,
                        JwtClaimTypes.Email,
                        OxyConstantsNames.Role,
                        OxyConstantsNames.Admin,
                        OxyConstantsNames.User,
                        OxyConstantsNames.MisApi,
                        OxyConstantsNames.MisApiRead,
                        OxyConstantsNames.MisApiWrite,
                        OxyConstantsNames.Permission,
                        OxyConstantsNames.Fio,
                        OxyConstantsNames.Position,

                    },
                    ApiSecrets = new List<Secret> { new Secret(OxySecrets.ApiSecret.ToSha256())},
                    Scopes = new List<Scope>()
                    {
                        OxyScopes.MisApiScope
                    },
                    Enabled = true
                }
            };
        }
    }
}