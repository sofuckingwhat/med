﻿namespace identity_infrastructure.Resources
{
    /// <summary>
    /// Клиентские приложения клиника Oxy
    /// </summary>
    public static class OxyClients
    {
        /// <summary>
        /// Клиентская программа реализующая API клиники Oxy
        /// </summary>
        public static string MisApi => "mis.api.client";

        /// <summary>
        /// Интерфейс пользователей (Angular client)
        /// </summary>
        public static string MisUi => "mis.ui.client";

        /// <summary>
        /// Интерфейс пользователей (Angular client)
        /// </summary>
        public static string MisUiLocal => "mis.ui.client.local";
    }
}