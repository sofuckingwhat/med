﻿namespace identity_infrastructure.Resources
{
    /// <summary>
    /// Наименования основных констант приложения
    /// </summary>
    public static class OxyConstantsNames
    {
        public const string Company = "company";
        public const string Role = "role";
        public const string Permission = "permission";
        public const string Admin = "admin";
        public const string User = "user";
        public const string MisApi = "mis.api";
        public const string MisApiRead = "mis.api.read";
        public const string MisApiWrite = "mis.api.write";
        public const string MisFrontend = "mis.frontend.ui";
        public const string Fio = "fio";
        public const string Position = "position";
    }
}