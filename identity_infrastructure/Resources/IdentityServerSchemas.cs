﻿namespace identity_infrastructure.Resources
{
    /// <summary>
    /// Схемы авторизации
    /// </summary>
    public static class IdentityServerSchemas
    {
        public static string Bearer => "Bearer";
    }
}