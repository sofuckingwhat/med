﻿using IdentityServer4.Models;

namespace identity_infrastructure.Resources
{
    public static class OxyScopes
    {
        /// <summary>
        /// Read scope для приложения Api
        /// </summary>
        public static string MisApiRead => "MisApi.read";

        /// <summary>
        /// Read/write scope для приложения Api
        /// </summary>
        public static string MisApiWrite => "MisApi.write";

        /// <summary>
        /// Полный доступ к Api
        /// </summary>
        public static string MisApiRoot => "MisApi.root";

        /// <summary>
        /// Поддержка "Медицинской информационной системы"
        /// </summary>
        public static string MisUiService => "MisUi.service";

        /// <summary>
        /// Доступ к "Медицинской информационной системе"
        /// </summary>
        public static string MisUiAccess => "MisUi.access";

        /// <summary>
        /// Scope для МИС апи
        /// </summary>
        public static Scope MisApiScope => new Scope
        {
            Name = "mis.api",
            DisplayName = "МИС API"
        };

        /// <summary>
        /// Scope для доступа апи
        /// </summary>
        public static Scope MisPermissionScope => new Scope
        {
            Name = "permission",
            DisplayName = "Привилегии доступа"
        };

        /// <summary>
        /// Наименование для МИС апи Scope
        /// </summary>
        public static string MisApiScopeName => MisApiScope.Name;
    }
}