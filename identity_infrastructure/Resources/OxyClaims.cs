﻿using System.Security.Claims;
using IdentityModel;

namespace identity_infrastructure.Resources
{
    public static class OxyClaims
    {
        public static Claim Root => new Claim(JwtClaimTypes.Role, "root");
        public static Claim Owner => new Claim(JwtClaimTypes.Role, "owner");
        public static Claim Admin => new Claim(JwtClaimTypes.Role, "admin");
        public static Claim User => new Claim(JwtClaimTypes.Role, "user");
        public static Claim Company => new Claim("company", "oxy");
        public static Claim PermissionsAllAllow => new Claim("permissions", "oxy.all");

    }
}