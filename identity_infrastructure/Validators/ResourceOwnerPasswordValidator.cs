﻿using System;
using System.Security.Authentication;
using System.Threading.Tasks;
using identity_infrastructure.Helpers;
using identity_infrastructure.Models;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;

namespace identity_infrastructure.Validators
{
    public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        #region Properties

        private readonly UserManager<OxyUser> _userManager;
        private readonly ILogger<ResourceOwnerPasswordValidator> _logger;

        #endregion

        public ResourceOwnerPasswordValidator(
                UserManager<OxyUser> user,
                ILogger<ResourceOwnerPasswordValidator> logger
            )
        {
            _logger = logger;
            _userManager = user;
        }

        public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            try
            {
                var user = await _userManager.FindByNameAsync(context.UserName);
                if (user == null)
                {
                    throw new NullReferenceException($"Пользователь с логином {context.UserName} не найден.");
                }

                if (user.PasswordHash != context.Password.HashPbkdf2())
                {
                    throw new AuthenticationException($"Пользователь {context.UserName} указал не верный пароль.");
                }

                context.Result = new GrantValidationResult(user.Id, "custom", await _userManager.GetClaimsAsync(user));
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Ошибка проверки пароля: {context.UserName}");
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, e.Message);
            }
        }
    }
}
