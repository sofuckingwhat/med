﻿using System;
using System.Linq;
using System.Threading.Tasks;
using identity_infrastructure.Models;
using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;

namespace identity_infrastructure.Services
{
    public class OxyProfileService : IProfileService
    {
        #region Properties

        private readonly UserManager<OxyUser> _userManager;
        private readonly ILogger<OxyProfileService> _logger;

        #endregion

        public OxyProfileService(UserManager<OxyUser> userManager, ILogger<OxyProfileService> logger)
        {
            _userManager = userManager;
            _logger = logger;
        }

        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var userName = context.Subject.Claims.FirstOrDefault(c => c.Type == JwtClaimTypes.PreferredUserName)?.Value;
            if (!string.IsNullOrEmpty(userName))
            {
                var user = await _userManager.FindByNameAsync(userName);
                if (user != null)
                {
                    var claims = await _userManager.GetClaimsAsync(user);
                    context.IssuedClaims = claims.Where(x => context.RequestedClaimTypes.Contains(x.Type)).ToList();
                }
            }
            else
            {
                var userSub = context.Subject.Claims.FirstOrDefault(x => x.Type == "sub");
                if (!string.IsNullOrEmpty(userSub?.Value) && Guid.TryParse(userSub.Value, out var userId))
                {
                    var user = await _userManager.FindByIdAsync(userId.ToString());
                    if (user != null)
                    {
                        var claims = await _userManager.GetClaimsAsync(user);
                        context.IssuedClaims =
                            claims.Where(c => context.RequestedClaimTypes.Contains(c.Type)).ToList();
                    }
                }
            }

        }

        public async Task IsActiveAsync(IsActiveContext context)
        {
            var userId = context.Subject.Claims.FirstOrDefault(x => x.Type == "sub");
            if (!string.IsNullOrEmpty(userId?.Value))
            {
                var user = await _userManager.FindByIdAsync(userId.Value);
                context.IsActive = user != null && user.EmailConfirmed;
            }
        }
    }
}