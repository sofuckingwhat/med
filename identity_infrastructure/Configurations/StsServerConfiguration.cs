﻿using Newtonsoft.Json;

namespace identity_infrastructure.Configurations
{
    /// <summary>
    /// Конфигурация для Identity Server 4
    /// </summary>
    public class StsServerConfiguration
    {
        [JsonProperty("stsServer")]
        public string StsServer { get; set; }

        [JsonProperty("redirect_url")]
        public string RedirectUrl { get; set; }

        [JsonProperty("client_id")]
        public string ClientId { get; set; }

        [JsonProperty("response_type")]
        public string ResponseType { get; set; }

        [JsonProperty("scope")]
        public string Scope { get; set; }

        [JsonProperty("post_logout_redirect_uri")]
        public string PostLogoutRedirectUri { get; set; }

        [JsonProperty("start_checksession")]
        public bool StartChecksession { get; set; }

        [JsonProperty("silent_renew")]
        public bool SilentRenew { get; set; }

        [JsonProperty("silent_renew_url")]
        public string SilentRenewUrl { get; set; }

        [JsonProperty("startup_route")]
        public string StartupRoute { get; set; }

        [JsonProperty("forbidden_route")]
        public string ForbiddenRoute { get; set; }

        [JsonProperty("unauthorized_route")]
        public string UnauthorizedRoute { get; set; }

        [JsonProperty("log_console_warning_active")]
        public bool LogConsoleWarningActive { get; set; }

        [JsonProperty("log_console_debug_active")]
        public bool LogConsoleDebugActive { get; set; }

        [JsonProperty("max_id_token_iat_offset_allowed_in_seconds")]
        public int MaxIdTokenIatOffsetAllowedInSeconds { get; set; }

        /// <summary>
        /// Сервис API
        /// </summary>
        [JsonProperty("apiServer")]
        public string ApiServer { get; set; }

        /// <summary>
        /// Файловое хранилище
        /// </summary>
        [JsonProperty("apiFileServer")]
        public string ApiFileServer { get; set; }
    }
}
