using identity_infrastructure.Models;

namespace identity_infrastructure.Viewmodels
{
    public class LogoutViewModel : LogoutInputModel
    {
        public bool ShowLogoutPrompt { get; set; } = true;
    }
}
