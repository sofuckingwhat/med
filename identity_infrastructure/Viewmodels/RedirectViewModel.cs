namespace identity_infrastructure.Viewmodels
{
    public class RedirectViewModel
    {
        public string RedirectUrl { get; set; }
    }
}