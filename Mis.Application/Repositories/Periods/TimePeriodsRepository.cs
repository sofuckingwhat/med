﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Data_Access;
using Domain.Constants;
using Domain.CQRS.Commands.TimePeriods;
using Domain.Entities.Common;
using Domain.Entities.Doctors;
using Domain.Helpers.StringHelpers;
using Domain.ViewModels.TimePeriod;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Mis.Application.Exceptions;
using Mis.Application.Interfaces.Periods;

namespace Mis.Application.Repositories.Periods
{
    public class TimePeriodsRepository: IPeriodsRepository
    {
        private readonly OxyDataContext _context;

        public TimePeriodsRepository(OxyDataContext context, IMediator mediator)
        {
            _context = context;
        }

        public async Task<Doctor> SaveTime(AddPeriodCommand command, CancellationToken token)
        {
            var doctor = await _context.Doctors
                .Include(w=>w.WorkTimes)
                .FirstOrDefaultAsync(d => d.Id == command.Id, token);

            if (doctor == null)
            {
                throw new NotFoundException(nameof(doctor), command.Id);
            }

            var periods = doctor.WorkTimes.Where(p => p.OnDate == command.Date).ToList();

            _context.TimePeriods.RemoveRange(periods);

            var newPeriods = command.Periods.Select(p => new TimePeriod(p, command)).ToList();

            newPeriods.ForEach(p =>
            {
                doctor.WorkTimes.Add(p);
            });

            await _context.SaveChangesAsync(token);

            return  doctor;
        }
    }
}
