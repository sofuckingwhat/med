﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Data_Access;
using Domain.CQRS.Commands.Doctors;
using Domain.Entities.Doctors;
using Domain.Helpers.Doctors;
using Domain.ViewModels.Doctors;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Mis.Application.Interfaces.Doctors;

namespace Mis.Application.Repositories.Doctors
{
    public class DoctorsRepository : IDoctorsRepository
    {
        private readonly OxyDataContext _context;
        private readonly IMediator _mediator;

        public DoctorsRepository(OxyDataContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        /// <summary>
        /// Создает новую запись о докторе и кладет готовый результат в Монгу
        /// </summary>
        /// <param name="request">Данные о докторе</param>
        /// <param name="cancellationToken">Токен отмены таска</param>
        /// <returns></returns>
        public async Task<DoctorDetailDto> CreateDoctor(CreateDoctorCommand request, CancellationToken cancellationToken)
        {
            var newDoctor = new Doctor(request);

            _context.Doctors.Add(newDoctor);

            await _context.SaveChangesAsync(cancellationToken);

            var response = new DoctorDetailDto(newDoctor);

            await _mediator.Publish(response, cancellationToken);

            return response;
        }

        /// <summary>
        /// Обновляет личные данные доктора 
        /// </summary>
        /// <param name="request">Данные</param>
        /// <param name="cancellationToken">Отмена задачи</param>
        /// <returns></returns>
        public async Task<DoctorDetailDto> UpdateDoctor(UpdateDoctorCommand request, CancellationToken cancellationToken)
        {
            var dbDoctor = await _context.Doctors.FirstOrDefaultAsync(d => d.Id == request.Id, cancellationToken);

            dbDoctor.UpdateDoctorData(request);

            await _context.SaveChangesAsync(cancellationToken);

            var response = new DoctorDetailDto(dbDoctor);

            await _mediator.Publish(response, cancellationToken);

            return response;
        }
    }
}
