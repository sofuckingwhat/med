﻿using System.Threading;
using System.Threading.Tasks;
using Domain.CQRS.Commands.Doctors;
using Domain.ViewModels.Doctors;
using MediatR;
using Mis.Application.Interfaces.Doctors;

namespace Mis.Application.CQRS.Doctors.Commands.Update
{
    public class UpdateDoctorCommandHandler : IRequestHandler<UpdateDoctorCommand, DoctorDetailDto>
    {
        private readonly IDoctorsRepository _repository;

        public UpdateDoctorCommandHandler(IDoctorsRepository repository)
        {
            _repository = repository;
        }

        public async Task<DoctorDetailDto> Handle(UpdateDoctorCommand request, CancellationToken cancellationToken)
        {
            return await _repository.UpdateDoctor(request, cancellationToken);
        }
    }
}