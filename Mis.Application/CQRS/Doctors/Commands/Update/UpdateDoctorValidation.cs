﻿using Domain.CQRS.Commands.Doctors;
using FluentValidation;

namespace Mis.Application.CQRS.Doctors.Commands.Update
{
    public class UpdateDoctorValidation : AbstractValidator<UpdateDoctorCommand>
    {
        public UpdateDoctorValidation()
        {
            RuleFor(x => x.Id)
                .NotNull()
                .WithMessage("Идентификатор доктора не должен быть пустым");
        }
    }
}