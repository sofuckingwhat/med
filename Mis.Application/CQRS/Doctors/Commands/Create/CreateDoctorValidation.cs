﻿using Domain.CQRS.Commands.Doctors;
using FluentValidation;

namespace Mis.Application.CQRS.Doctors.Commands.Create
{
    public class CreateDoctorValidation : AbstractValidator<CreateDoctorCommand>
    {
        public CreateDoctorValidation()
        {
            RuleFor(x => x.Fio)
                .NotNull()
                .WithMessage("ФИО null")
                .NotEmpty()
                .WithMessage("ФИО не должно быть пустым")
                .Must(FioWithThreeWords)
                .WithMessage("ФИО нужно указать полностью");
        }

        /// <summary>
        /// Проверяем, что бы ФИО было минимум из 3х слов
        /// </summary>
        /// <param name="fio"></param>
        /// <returns></returns>
        private static bool FioWithThreeWords(string fio)
        {
            return !string.IsNullOrEmpty(fio) && fio.Split(" ").Length >= 3;
        }
    }
}