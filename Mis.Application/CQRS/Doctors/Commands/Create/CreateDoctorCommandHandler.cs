﻿using System.Threading;
using System.Threading.Tasks;
using Domain.CQRS.Commands.Doctors;
using Domain.ViewModels.Doctors;
using MediatR;
using Mis.Application.Interfaces.Doctors;

namespace Mis.Application.CQRS.Doctors.Commands.Create
{
    public class CreateDoctorCommandHandler : IRequestHandler<CreateDoctorCommand, DoctorDetailDto>
    {
        private readonly IDoctorsRepository _repository;

        public CreateDoctorCommandHandler(IDoctorsRepository repo)
        {
            _repository = repo;
        }

        public Task<DoctorDetailDto> Handle(CreateDoctorCommand request, CancellationToken cancellationToken)
        {
            return _repository.CreateDoctor(request, cancellationToken);
        }
    }
}
