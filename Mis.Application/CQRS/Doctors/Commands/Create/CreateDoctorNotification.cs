﻿using System.Threading;
using System.Threading.Tasks;
using Data_Access.MongoDb;
using Domain.ViewModels.Doctors;
using MediatR;
using MongoDB.Driver;

namespace Mis.Application.CQRS.Doctors.Commands.Create
{
    public class CreateDoctorNotification : INotificationHandler<DoctorDetailDto>
    {
        private readonly IMongoCollection<DoctorDetailDto> _collection;

        public CreateDoctorNotification(MongoContextBase context)
        {
            _collection = context.Doctors;
        }

        public async Task Handle(DoctorDetailDto notification, CancellationToken cancellationToken)
        {
            var filter = Builders<DoctorDetailDto>.Filter.Eq(model => model.Id, notification.Id);
            await _collection.ReplaceOneAsync(filter, notification, new UpdateOptions() { IsUpsert = true }, cancellationToken);

        }
    }
}