﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Data_Access.MongoDb;
using Domain.Abstract;
using Domain.CQRS.Queries.Doctors;
using Domain.Implementations;
using Domain.ViewModels.Doctors;
using MediatR;
using MongoDB.Driver;

namespace Mis.Application.CQRS.Doctors.Queries
{
    /// <summary>
    /// Запрос возвращает список докторов
    /// с сортировкой по ФИО и пагинацией
    /// </summary>
    public class DoctorsListQueryHandler : IRequestHandler<DoctorsListQuery, IResponseList<DoctorListItemDto>>
    {
        private readonly MongoContextBase _mongo;

        public DoctorsListQueryHandler(MongoContextBase mongo)
        {
            _mongo = mongo;
        }

        public async Task<IResponseList<DoctorListItemDto>> Handle(DoctorsListQuery request, CancellationToken cancellationToken)
        {
            var requestOptions = new FindOptions<DoctorDetailDto>()
            {
                Skip = (request.Page - 1) * request.PerPage,
                Limit = request.PerPage
            };

            var requestToDb = await _mongo.Doctors
                .FindAsync(FilterDefinition<DoctorDetailDto>.Empty, requestOptions, cancellationToken);

            var doctors = await requestToDb.ToListAsync(cancellationToken);

            var response = new ResponseList<DoctorListItemDto>
            {
                Items = doctors.AsReadOnly().OrderBy(d => d.Fio),
                PerPage = request.PerPage,
                Page = request.Page,
                TotalItems = (int)_mongo.Doctors.CountDocuments(FilterDefinition<DoctorDetailDto>.Empty)
            };

            return response;
        }
    }
}
