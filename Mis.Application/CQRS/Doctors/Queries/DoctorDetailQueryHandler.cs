﻿using System.Threading;
using System.Threading.Tasks;
using Data_Access.MongoDb;
using Domain.CQRS.Queries.Doctors;
using Domain.ViewModels.Doctors;
using MediatR;
using MongoDB.Driver;

namespace Mis.Application.CQRS.Doctors.Queries
{
    public class DoctorDetailQueryHandler : IRequestHandler<DoctorDetailQuery, DoctorDetailDto>
    {
        private readonly MongoContextBase _mongo;

        public DoctorDetailQueryHandler(MongoContextBase mongo)
        {
            _mongo = mongo;
        }

        public async Task<DoctorDetailDto> Handle(DoctorDetailQuery request, CancellationToken cancellationToken)
        {
            var filter = Builders<DoctorDetailDto>.Filter.Eq(model => model.Id, request.Id);

            var dto = await _mongo.Doctors.Find(filter).FirstOrDefaultAsync(cancellationToken);

            return dto;
        }
    }
}