﻿using System;
using Domain.CQRS.Commands.Patients;
using FluentValidation;

namespace Mis.Application.CQRS.Patients.Commands.CreatePatient
{
    public class FirstVisitCommandValidation : AbstractValidator<CreatePatientCommand>
    {
        public FirstVisitCommandValidation()
        {
            RuleFor(x => x.Fio)
                .Must(x => x.Trim().Split(' ').Length == 3)
                .WithMessage("Фамилия Имя Отчество должно быть указано полностью");

            RuleFor(x => x.Birthday)
                .GreaterThan(DateTime.Today.AddYears(-50))
                .WithMessage(
                    $"Пациент должен быть младше 50 лет. Дата рождения не может превышать {DateTime.Today.AddYears(-50).ToShortDateString()}")
                .LessThan(DateTime.Today.AddYears(-18))
                .WithMessage($"Пациент должен быть старше 18 лет. Год рождения должен быть больше {DateTime.Today.AddYears(-18).Year}");

            RuleFor(x => x.SourceOfInformation)
                .NotNull()
                .WithMessage("Не указан источник информации о клинике");

            RuleFor(x => x.Phones)
                .NotNull()
                .MinimumLength(6)
                .WithMessage("Телефон обязателен. Минимум городской с кодом города.");

        }
    }
}