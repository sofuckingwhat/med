﻿using System.Threading;
using System.Threading.Tasks;
using Data_Access;
using Domain.CQRS.Commands.Patients;
using Domain.Entities.Patients;
using Domain.ViewModels.Patients;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Mis.Application.CQRS.Patients.Commands.CreatePatient
{
    /// <summary>
    /// Запись нового пациента 
    /// </summary>
    public class FirstVisitCommandHandler : IRequestHandler<CreatePatientCommand, PatientItemViewModel>
    {
        private readonly OxyDataContext _context;

        public FirstVisitCommandHandler(OxyDataContext context)
        {
            _context = context;
        }

        public async Task<PatientItemViewModel> Handle(CreatePatientCommand request, CancellationToken cancellationToken)
        {
            var patientEntity = new Patient(request);

            _context.Patients.Add(patientEntity);

            await _context.SaveChangesAsync(cancellationToken);

            patientEntity.SourceOfInformation =
                await _context.DictionaryItems.FirstOrDefaultAsync(x => x.Id == request.SourceOfInformation,
                    cancellationToken);
            return new PatientItemViewModel(patientEntity);
        }
    }
}