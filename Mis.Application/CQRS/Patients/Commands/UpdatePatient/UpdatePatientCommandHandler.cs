﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Data_Access;
using Domain.CQRS.Commands.Patients;
using Domain.ViewModels.Patients;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Mis.Application.Exceptions;

namespace Mis.Application.CQRS.Patients.Commands.UpdatePatient
{
    public class UpdatePatientCommandHandler : IRequestHandler<UpdatePatientCommand, PatientDetailViewModel>
    {
        private readonly OxyDataContext _context;
        private readonly IMediator _mediator;

        public UpdatePatientCommandHandler(OxyDataContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<PatientDetailViewModel> Handle(UpdatePatientCommand request, CancellationToken cancellationToken)
        {
            var dbPatient = _context.Patients
                .Include(x => x.Passport)
                .Include(x => x.Addresses)
                .Include(x => x.Polises)
                .Include(x => x.SourceOfInformation)
                .FirstOrDefault(x => x.Id == request.Id);

            if (dbPatient == null)
            {
                throw new NotFoundException(nameof(dbPatient), request.Id);
            }

            dbPatient.UpdatePatient(request);

            await _context.SaveChangesAsync(cancellationToken);

            var response = new PatientDetailViewModel(dbPatient);

            await _mediator.Publish(response, cancellationToken);

            return response;

        }
    }
}