﻿using System.Threading;
using System.Threading.Tasks;
using Data_Access.MongoDb;
using Domain.ViewModels.Patients;
using MediatR;
using MongoDB.Driver;

namespace Mis.Application.CQRS.Patients.Commands.UpdatePatient
{
    public class UpdatePatientNotification : INotificationHandler<PatientDetailViewModel>
    {
        private readonly IMongoCollection<PatientDetailViewModel> _mongo;

        public UpdatePatientNotification(MongoContextBase mongo)
        {
            _mongo = mongo.Patients;
        }

        public async Task Handle(PatientDetailViewModel notification, CancellationToken cancellationToken)
        {
            var filter = Builders<PatientDetailViewModel>.Filter.Eq(model => model.Id, notification.Id);
            await _mongo.ReplaceOneAsync(filter, notification, new UpdateOptions() { IsUpsert = true }, cancellationToken);
        }
    }
}