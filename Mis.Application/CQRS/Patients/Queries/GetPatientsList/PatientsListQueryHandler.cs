﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Data_Access;
using Domain.Abstract;
using Domain.CQRS.Queries.Patients;
using Domain.Implementations;
using Domain.ViewModels.Patients;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Mis.Application.CQRS.Patients.Queries.GetPatientsList
{
    public class PatientsListQueryHandler : IRequestHandler<PatientsListQuery, IResponseList<PatientItemViewModel>>
    {
        private readonly OxyDataContext _context;

        public PatientsListQueryHandler(OxyDataContext context)
        {
            _context = context;
        }

        public async Task<IResponseList<PatientItemViewModel>> Handle(PatientsListQuery request, CancellationToken cancellationToken)
        {
            var page = request.Page > 0 ? request.Page - 1 : request.Page;

            var totalItems = await _context.Patients.CountAsync(cancellationToken);

            var patients = await _context.Patients
                .Include(x => x.SourceOfInformation)
                .OrderByDescending(x => x.Created)
                .Skip(page * request.PerPage)
                .Take(request.PerPage)
                .Select(x => new PatientItemViewModel(x))
                .ToListAsync(cancellationToken);

            var result = new ResponseList<PatientItemViewModel>
            {
                Items = patients,
                Page = page,
                PerPage = request.PerPage,
                TotalItems = totalItems
            };
            return result;
        }
    }
}