﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Data_Access;
using Domain.CQRS.Queries.Patients;
using Domain.ViewModels.Patients;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Mis.Application.Exceptions;
using MongoDB.Driver;

namespace Mis.Application.CQRS.Patients.Queries.GetPatientDetails
{
    public class PatientDetailQueryHandler : IRequestHandler<PatientDetailQuery, PatientDetailViewModel>
    {
        private readonly OxyDataContext _context;
        private readonly IMongoCollection<PatientDetailViewModel> _mongo;

        public PatientDetailQueryHandler(OxyDataContext context, IMongoCollection<PatientDetailViewModel> mongo)
        {
            _context = context;
            _mongo = mongo;
        }

        public async Task<PatientDetailViewModel> Handle(PatientDetailQuery request, CancellationToken cancellationToken)
        {
            var filter = Builders<PatientDetailViewModel>.Filter.Eq(model => model.Id, request.Id);

            var mongoPatient = await _mongo.Find(filter).FirstOrDefaultAsync(cancellationToken);

            if (mongoPatient != null)
            {
                return mongoPatient;
            }

            var dbPerson = await _context.Patients
                .Include(x => x.Addresses)
                .Include(x => x.Passport)
                .Include(x => x.Polises)
                .Include(x => x.SourceOfInformation)
                .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

            if (dbPerson == null)
            {
                throw new NotFoundException(nameof(dbPerson), request.Id);
            }

            if (!string.IsNullOrEmpty(dbPerson.ClinicUID)) return new PatientDetailViewModel(dbPerson);

            var personNumber = _context.Patients
                .Count(p => !string.IsNullOrEmpty(p.ClinicUID));

            if (string.IsNullOrEmpty(dbPerson.ClinicUID))
            {
                dbPerson.ClinicUID = GenerateAmbulanceNumber(personNumber);
            }

            return new PatientDetailViewModel(dbPerson);
        }

        private static string GenerateAmbulanceNumber(int count)
        {
            count++;
            var number = count.ToString().PadLeft(7, '0');

            return $"AK-{number}";
        }
    }
}