﻿using Domain.CQRS.Commands.Dictionaries;
using FluentValidation;

namespace Mis.Application.CQRS.Dictionaries.Commands.CreateDictionary
{
    public class CreateDictionaryCommandValidator : AbstractValidator<CreateDictionaryCommand>
    {
        public CreateDictionaryCommandValidator()
        {
            RuleFor(x => x.Title)
                .MinimumLength(5)
                .NotEmpty()
                .WithMessage("Наименование справочника должно быть минимум 5 символов.");
        }
    }
}
