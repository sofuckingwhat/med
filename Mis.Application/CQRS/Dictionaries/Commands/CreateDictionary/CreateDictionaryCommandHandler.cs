﻿using System.Threading;
using System.Threading.Tasks;
using Data_Access;
using Domain.CQRS.Commands.Dictionaries;
using Domain.Entities.Dictionaries;
using Domain.ViewModels.Dictionaries;
using MediatR;

namespace Mis.Application.CQRS.Dictionaries.Commands.CreateDictionary
{
    public class CreateDictionaryCommandHandler : IRequestHandler<CreateDictionaryCommand, DictionaryViewModel>
    {
        private readonly OxyDataContext _data;

        public CreateDictionaryCommandHandler(OxyDataContext data)
        {
            _data = data;
        }

        public async Task<DictionaryViewModel> Handle(CreateDictionaryCommand request, CancellationToken cancellationToken)
        {
            var newDictionary = new Dictionary(request);

            _data.Dictionaries.Add(newDictionary);

            await _data.SaveChangesAsync(cancellationToken);

            return new DictionaryViewModel(newDictionary);
        }
    }
}