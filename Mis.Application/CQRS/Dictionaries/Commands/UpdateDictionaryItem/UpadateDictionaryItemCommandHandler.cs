﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Data_Access;
using Domain.CQRS.Commands.Dictionaries;
using Domain.ViewModels.Dictionaries;
using MediatR;
using Mis.Application.Exceptions;

namespace Mis.Application.CQRS.Dictionaries.Commands.UpdateDictionaryItem
{
    public class UpdateDictionaryItemCommandHandler : IRequestHandler<UpdateDictionaryItemCommand, DictionaryItemViewModel>
    {
        private readonly OxyDataContext _data;

        public UpdateDictionaryItemCommandHandler(OxyDataContext data)
        {
            _data = data;
        }

        public async Task<DictionaryItemViewModel> Handle(UpdateDictionaryItemCommand request, CancellationToken cancellationToken)
        {
            var dbItem = _data.DictionaryItems
                .Where(i => !i.IsDeleted)
                .FirstOrDefault(i => i.Id == request.Id);

            if (dbItem == null)
            {
                throw new NotFoundException(nameof(dbItem), request.Id);
            }

            dbItem.UpdateEntity(request);

            await _data.SaveChangesAsync(cancellationToken);

            return new DictionaryItemViewModel(dbItem);
        }
    }
}
