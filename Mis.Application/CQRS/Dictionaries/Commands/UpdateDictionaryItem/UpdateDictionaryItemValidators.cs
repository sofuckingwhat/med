﻿using System;
using Domain.CQRS.Commands.Dictionaries;
using FluentValidation;

namespace Mis.Application.CQRS.Dictionaries.Commands.UpdateDictionaryItem
{
    public class UpdateDictionaryItemValidators : AbstractValidator<UpdateDictionaryItemCommand>
    {
        public UpdateDictionaryItemValidators()
        {
            RuleFor(i => i.Id).NotNull().NotEqual(command => Guid.Empty);
            RuleFor(i => i.ShortName).NotNull().NotEmpty();
            RuleFor(i => i.FullName).NotNull().NotEmpty();
        }
    }
}