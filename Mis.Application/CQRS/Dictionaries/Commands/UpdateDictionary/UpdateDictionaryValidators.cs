﻿using Domain.CQRS.Commands.Dictionaries;
using FluentValidation;

namespace Mis.Application.CQRS.Dictionaries.Commands.UpdateDictionary
{
    public class UpdateDictionaryValidators : AbstractValidator<UpdateDictionaryCommand>
    {
        public UpdateDictionaryValidators()
        {
            RuleFor(c => c.Id).NotNull().NotEmpty();
            RuleFor(c => c.Name).MinimumLength(5).NotNull();
        }

    }
}