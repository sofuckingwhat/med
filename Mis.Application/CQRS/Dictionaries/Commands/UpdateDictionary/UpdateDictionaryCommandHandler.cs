﻿using System.Threading;
using System.Threading.Tasks;
using Data_Access;
using Domain.CQRS.Commands.Dictionaries;
using Domain.ViewModels.Dictionaries;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Mis.Application.Exceptions;

namespace Mis.Application.CQRS.Dictionaries.Commands.UpdateDictionary
{
    public class UpdateDictionaryCommandHandler : IRequestHandler<UpdateDictionaryCommand, DictionaryViewModel>
    {
        private readonly OxyDataContext _data;

        public UpdateDictionaryCommandHandler(OxyDataContext data)
        {
            _data = data;
        }

        public async Task<DictionaryViewModel> Handle(UpdateDictionaryCommand request, CancellationToken cancellationToken)
        {
            var dbDictionary = await _data.Dictionaries
                .FirstOrDefaultAsync(d => d.Id == request.Id, cancellationToken);

            if (dbDictionary == null)
            {
                throw new NotFoundException(nameof(dbDictionary), request.Id);
            }

            dbDictionary.UpdateDictionaryValues(request);

            await _data.SaveChangesAsync(cancellationToken);

            return new DictionaryViewModel(dbDictionary);
        }
    }
}