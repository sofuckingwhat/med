﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Data_Access;
using Domain.CQRS.Commands.Dictionaries;
using Domain.Entities.Dictionaries;
using Domain.ViewModels.Dictionaries;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Mis.Application.Exceptions;

namespace Mis.Application.CQRS.Dictionaries.Commands.CreateDictionaryItem
{
    public class CreateDictionaryItemHandler : IRequestHandler<CreateDictionaryItemCommand, DictionaryItemViewModel>
    {
        private readonly OxyDataContext _data;

        public CreateDictionaryItemHandler(OxyDataContext data)
        {
            _data = data;
        }

        public async Task<DictionaryItemViewModel> Handle(CreateDictionaryItemCommand request, CancellationToken cancellationToken)
        {
            var dbDictionary = await _data.Dictionaries
                .Include(d => d.Items)
                .FirstOrDefaultAsync(d => d.Id == request.DictionaryId, cancellationToken);

            if (dbDictionary == null)
            {
                throw new NotFoundException(nameof(dbDictionary), request.DictionaryId);
            }

            if (dbDictionary.Items == null)
            {
                dbDictionary.Items = new List<DictionaryItem>();
            }

            var newItem = new DictionaryItem(request);

            dbDictionary.Items.Add(newItem);

            await _data.SaveChangesAsync(cancellationToken);

            return new DictionaryItemViewModel(newItem);
        }
    }
}
