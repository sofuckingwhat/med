﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Data_Access;
using Domain.CQRS.Commands.Dictionaries;
using Domain.ViewModels.Dictionaries;
using MediatR;
using Mis.Application.Exceptions;

namespace Mis.Application.CQRS.Dictionaries.Commands.DeleteDictionaryItem
{
    public class DeleteDictionaryItemCommandHandler : IRequestHandler<DeleteDictionaryItemCommand, DictionaryItemViewModel>
    {
        private readonly OxyDataContext _data;

        public DeleteDictionaryItemCommandHandler(OxyDataContext data)
        {
            _data = data;
        }

        public async Task<DictionaryItemViewModel> Handle(DeleteDictionaryItemCommand request, CancellationToken cancellationToken)
        {
            var dbItem = _data.DictionaryItems.FirstOrDefault(i => i.Id == request.Id);

            if (dbItem == null)
            {
                throw new NotFoundException(nameof(dbItem), request.Id);
            }

            dbItem.IsDeleted = true;

            await _data.SaveChangesAsync(cancellationToken);

            return new DictionaryItemViewModel(dbItem);
        }
    }
}
