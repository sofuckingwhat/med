﻿using Domain.ViewModels.Dictionaries;
using MediatR;

namespace Mis.Application.CQRS.Dictionaries.Queries
{
    /// <summary>
    /// Получить данные справочника по его наименованию
    /// </summary>
    public class DictionaryByNameQuery : IRequest<DictionaryDetailViewModel>
    {
        public string Name { get; set; }
    }
}