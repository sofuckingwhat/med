﻿using System.Collections.Generic;
using Domain.ViewModels.Dictionaries;
using MediatR;

namespace Mis.Application.CQRS.Dictionaries.Queries
{
    /// <summary>
    /// Запрос списка доступных справочников
    /// </summary>
    public class DictionariesListQuery : IRequest<IEnumerable<DictionaryViewModel>>
    {
    }
}