﻿using System;
using Domain.ViewModels.Dictionaries;
using MediatR;

namespace Mis.Application.CQRS.Dictionaries.Queries
{
    public class DictionaryDetailQuery : IRequest<DictionaryDetailViewModel>
    {
        public Guid Id { get; set; }
    }
}