﻿using System.Threading;
using System.Threading.Tasks;
using Data_Access;
using Domain.ViewModels.Dictionaries;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Mis.Application.Exceptions;

namespace Mis.Application.CQRS.Dictionaries.Queries.Handlers
{
    public class DictionaryByNameHandler : IRequestHandler<DictionaryByNameQuery, DictionaryDetailViewModel>
    {
        private readonly OxyDataContext _context;

        public DictionaryByNameHandler(OxyDataContext context)
        {
            _context = context;
        }

        public async Task<DictionaryDetailViewModel> Handle(DictionaryByNameQuery request, CancellationToken cancellationToken)
        {
            var dbDictionary = await _context.Dictionaries
                .Include(d => d.Items)
                .FirstOrDefaultAsync(d => d.Name == request.Name, cancellationToken);

            if (dbDictionary == null)
            {
                throw new NotFoundException(nameof(dbDictionary), request.Name);
            }

            return new DictionaryDetailViewModel(dbDictionary);
        }
    }
}