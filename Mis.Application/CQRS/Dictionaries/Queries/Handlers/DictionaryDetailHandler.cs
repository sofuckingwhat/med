﻿using System.Threading;
using System.Threading.Tasks;
using Data_Access;
using Domain.ViewModels.Dictionaries;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Mis.Application.Exceptions;

namespace Mis.Application.CQRS.Dictionaries.Queries.Handlers
{
    public class DictionaryDetailHandler : IRequestHandler<DictionaryDetailQuery, DictionaryDetailViewModel>
    {
        private readonly OxyDataContext _context;

        public DictionaryDetailHandler(OxyDataContext context)
        {
            _context = context;
        }

        public async Task<DictionaryDetailViewModel> Handle(DictionaryDetailQuery request, CancellationToken cancellationToken)
        {
            var dbDictionary = await _context.Dictionaries
                .Include(d => d.Items)
                .FirstOrDefaultAsync(dictionary => dictionary.Id == request.Id, cancellationToken);

            if (dbDictionary == null)
            {
                throw new NotFoundException(nameof(dbDictionary), request.Id);
            }

            return new DictionaryDetailViewModel(dbDictionary);
        }
    }
}