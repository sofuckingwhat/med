﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Data_Access;
using Domain.ViewModels.Dictionaries;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Mis.Application.CQRS.Dictionaries.Queries.Handlers
{
    public class QueryDictionaryListHandler : IRequestHandler<DictionariesListQuery, IEnumerable<DictionaryViewModel>>
    {
        private readonly OxyDataContext _context;

        public QueryDictionaryListHandler(OxyDataContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<DictionaryViewModel>> Handle(DictionariesListQuery request, CancellationToken cancellationToken)
        {
            var result = await _context.Dictionaries
                .Where(d => !d.IsDeleted)
                .Select(d => new DictionaryViewModel(d))
                .ToListAsync(cancellationToken);

            return result;
        }
    }
}