﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Domain.CQRS.Commands.TimePeriods;
using Domain.ViewModels.Doctors;
using Domain.ViewModels.TimePeriod;
using MediatR;
using Mis.Application.Interfaces.Periods;

namespace Mis.Application.CQRS.TimePeriod.Commands
{
    public class AddPeriodCommandHandler : IRequestHandler<AddPeriodCommand, WorkGraphicDto>
    {
        private readonly IPeriodsRepository _periods;
        private readonly IMediator _mediator;

        public AddPeriodCommandHandler(IPeriodsRepository repository, IMediator mediator)
        {
            _periods = repository;
            _mediator = mediator;
        }

        public async Task<WorkGraphicDto> Handle(AddPeriodCommand request, CancellationToken cancellationToken)
        {
            var doctor = await _periods.SaveTime(request, cancellationToken);

            var doctorDto = new DoctorDetailDto(doctor);

            await _mediator.Publish(doctorDto, cancellationToken);

            var response = doctorDto.WorkGraphic.FirstOrDefault(w => w.PeriodDate == request.Date);

            return response;
        }
    }
}
