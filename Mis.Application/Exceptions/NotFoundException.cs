﻿using System;

namespace Mis.Application.Exceptions
{
    public class NotFoundException : Exception
    {
        public NotFoundException(string name, object key)
            : base($"Entity \"{name}\" ({key}) не найден в базе данных")
        {
        }
    }
}