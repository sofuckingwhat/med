﻿using System.Threading;
using System.Threading.Tasks;
using Domain.CQRS.Commands.Doctors;
using Domain.ViewModels.Doctors;

namespace Mis.Application.Interfaces.Doctors
{
    public interface IDoctorsRepository
    {
        Task<DoctorDetailDto> CreateDoctor(CreateDoctorCommand request, CancellationToken cancellationToken);
        Task<DoctorDetailDto> UpdateDoctor(UpdateDoctorCommand request, CancellationToken cancellationToken);
    }
}
