﻿using System.Threading;
using System.Threading.Tasks;
using Domain.CQRS.Commands.TimePeriods;
using Domain.Entities.Doctors;

namespace Mis.Application.Interfaces.Periods
{
    public interface IPeriodsRepository
    {
        Task<Doctor> SaveTime(AddPeriodCommand command, CancellationToken token);
    }
}
