import { IAppConfig } from 'src/app/core/interfaces';

export const environment: IAppConfig = {
  production: true,
  host: 'http://10.20.10.26:8080/api',
  preloaderTime: 5000,
  useAuthentication: true
};
