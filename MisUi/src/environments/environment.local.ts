import { IAppConfig } from 'src/app/core/interfaces';

export const environment: IAppConfig = {
  production: true,
  host: 'http://localhost:5029/api',
  preloaderTime: 100,
  useAuthentication: true
};
