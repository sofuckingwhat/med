import { environment } from 'src/environments/environment';

// TODO!: перенести в отдельный класс
export class Urls {

  /** Конфигурация сервера авторизации */
  public static stsConfig = `${environment.host}/config`;

  /** Корневой урл для работы со справочниками */
  public static dictionaries = `${environment.host}/dictionary`;

  /** Запросить данные справочника по его имени */
  public static dictionaryByName = `${environment.host}/dictionary/by-name`;

  /** Корневой элемент работы с записями справочника */
  public static dictionariesItems = `${environment.host}/dictionary/item`;

}
