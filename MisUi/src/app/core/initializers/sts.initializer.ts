import { OidcConfigService } from 'angular-auth-oidc-client';
import { Urls } from '../constants';
import { APP_INITIALIZER } from '@angular/core';

export function loadConfig(appConfig: OidcConfigService) {
  // tslint:disable-next-line:arrow-return-shorthand
  return () => { return appConfig.load(Urls.stsConfig); };
}

export const StsInitializer = {
  provide: APP_INITIALIZER,
  useFactory: loadConfig,
  deps: [OidcConfigService],
  multi: true
};
