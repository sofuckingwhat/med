import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Injector, Injectable } from '@angular/core';

// Rxjs
import { Observable } from 'rxjs';

// Services
import { AuthService } from '../services/authentication';

/** Подписывает все запросы токеном пользователя */
@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  private authService: AuthService;

  constructor(private readonly injector: Injector) {}

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let requestToForward = req;

    if (this.authService === undefined) {
      this.authService = this.injector.get(AuthService);
    }

    if (this.authService !== undefined) {
      const token = this.authService.token;

      if (token !== null) {
        const tokenHeaderValue = `Bearer ${token}`;
        requestToForward = req.clone({setHeaders: {Authorization: tokenHeaderValue}});
      }

    }

    return next.handle(requestToForward);
  }
}
