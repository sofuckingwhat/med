export interface IAppConfig {
    /** Переменная среды */
    production: boolean;

    /** Ссылка на хост, где находится Api */
    host: string;

    /** Preloader time interval */
    preloaderTime: number;

    /** Использовать авторизацию в приложении */
    useAuthentication: boolean;
}
