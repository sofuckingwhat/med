/** Модель данных конфигурации StsServer */
export interface IStsConfigInterface {
  stsServer: string;
  redirect_url: string;
  client_id: string;
  response_type: string;
  scope: string;
  post_logout_redirect_uri: string;
  start_checksession: boolean;
  silent_renew: boolean;
  startup_route: string;
  forbidden_route: string;
  unauthorized_route: string;
  log_console_warning_active: boolean;
  log_console_debug_active: boolean;
  max_id_token_iat_offset_allowed_in_seconds: number;
  apiServer: string;
  apiFileServer: string;
}
