import { Dictionary } from '../../models/generics';

export interface IBackValidationError {
  errors: Dictionary<string, string[]>;
  title: string;
  status: number;
  traceId: string;
}
