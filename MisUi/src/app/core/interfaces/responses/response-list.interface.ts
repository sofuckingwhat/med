/** Ответ сервера со списком и с пагинацией */
export class IResponseList<T> {
  page: number;
  perPage: number;
  totalItems: number;
  totalPages: number;
  items: T[];
}
