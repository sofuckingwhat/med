// Enums
import { ModalEventsEnum } from '../enums';

/** Интерфейс параметров модального окна */
export interface IModalWindowParams {
  popupEvent: ModalEventsEnum;
  response?: any;
  component?: any;
  options?: object;
}
