export { PatientsUrls } from './patients/patients.urls';
export { DoctorsUrls } from './doctors/doctors.urls';
export { PeriodsUrls } from './periods/time-periods.urls';
