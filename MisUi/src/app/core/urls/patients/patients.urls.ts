import { environment } from 'src/environments/environment';

export class PatientsUrls {

  /** Базовый урл для работы с пациентами */
  public static baseUrl = `${environment.host}/patient`;
}
