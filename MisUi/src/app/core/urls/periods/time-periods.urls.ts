import { environment } from 'src/environments/environment';

/** Коллекция адресов для интервалов времени */
export class PeriodsUrls {

  /** Базовый адрес апи для интервалов времени */
  public static baseUrl = `${environment.host}/timeperiod`;

}
