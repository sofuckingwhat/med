import { environment } from 'src/environments/environment';

/** Коллекция адресов для докторов */
export class DoctorsUrls {

  /** Базовый адрес апи для работы с докторами */
  public static baseUrl = `${environment.host}/doctors`;

}
