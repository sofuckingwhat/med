/** Модель справочника */
export class DictionaryModel {
  id: string;
  title: string;
  description: string;
}
