import { DictionaryItemModel } from './dictionary-item.model';

/** Модель записи справочника для отправки на сервер */
export class DictionaryItemPostModel extends DictionaryItemModel {

  /** Идентификатор справочника */
  public dictionaryId: string;
}
