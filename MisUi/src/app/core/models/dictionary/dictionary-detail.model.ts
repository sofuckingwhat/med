import { DictionaryItemModel } from './dictionary-item.model';
import { DictionaryModel } from './dictionary.model';

/** Развернутая модель справочника */
export class DictionaryDetailModel extends DictionaryModel {
  items: DictionaryItemModel[];
}
