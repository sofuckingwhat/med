export { DictionaryModel } from './dictionary.model';
export { DictionaryDetailModel } from './dictionary-detail.model';
export { DictionaryItemModel } from './dictionary-item.model';
export { DictionaryItemPostModel } from './dictionary-item-post.model';
