/** Модель записи справочника */
export class DictionaryItemModel {
  id: string;
  fullName: string;
  shortName: string;
  code: string;
}
