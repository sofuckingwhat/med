/** Модель интервала времени */
export class TimePeriod {

  /** Идентификатор периода */
  public id: string;

  /** Тип интервала */
  public periodType: string;

  /** Начало интервала */
  public start: string;

  /** Конец интервала */
  public end: string;

  /** Длительность интервала в минутах */
  public duration: number;
}
