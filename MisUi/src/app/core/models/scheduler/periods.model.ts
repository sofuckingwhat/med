import { TimePeriod } from './time-period.model';

/** Модель периодов времени работы */
export class PeriodModel {

  /** Идентификатор доктора */
  public objectId: string;

  /** Наименование объекта интервала */
  public title: string;
  /** День */
  public periodDate: Date;

  /** Рабочие часы */
  public workTime: TimePeriod[] = [];

}
