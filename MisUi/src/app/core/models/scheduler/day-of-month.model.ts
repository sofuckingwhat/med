import { WeekDayNames } from '../../enums';
import { DrawDaySchedule } from './draw-day-schedule.model';
import { PeriodModel } from './periods.model';

export class DayOfMonthModel {

  /** День месяца 1-28/29/30/31 */
  public day: number;

  /** Номер месяца 1-12 */
  public month: number;

  /** Номер года */
  public year: number;

  /** Наименование дня недели на русском */
  public title: string;

  /** Всего рабочих часов в неделе */
  public totalWorkHours = 0;

  /** Дана (полная) */
  public dayDate: Date;

  public workingHours: PeriodModel;

  /** Неделя, в которую входит день */
  private _week: DayOfMonthModel[] = [];

  /** Визуализатор рабочих часов */
  public painter: DrawDaySchedule;

  constructor(
    date: Date
  ) {
    this.year = date.getFullYear();
    this.month = date.getMonth() + 1;
    this.day = date.getDate();
    this.dayDate = date;
    this.title = this.getDayName(date);
  }

  /** Формируем неделю если ее нет,
   * или возвращаем готовую.
   */
  public get week(): DayOfMonthModel[] {
    if (this._week && this._week.length === 7) {

      return this._week;
    }
    this._week = [];
    this._week.push(this);

    const weekDay = this.dayDate.getDay();

    if (weekDay !== 1) {
      this.addDaysAtStart(this.dayDate);
    }

    if (weekDay !== 0) {
      this.addDaysAtEnd(this.dayDate);
    }

    return this._week;
  }

  /**
   * Добавить дни до конца последней недели,
   * если последний день месяца не воскресенье
   * @param lastDay Дата последнего дня месяца
   */
  private addDaysAtEnd(lastDay: Date): void {
    const dayNumber = lastDay.getDay();
    let plusDay = 1;

    let moveDaysForward = dayNumber === 6 ? 0 : dayNumber + 1;

    while (moveDaysForward !== 1) {
      this._week.push(new DayOfMonthModel(
        new Date(this.year, this.month - 1, this.day + plusDay)
      ));


      if (moveDaysForward === 7) {
        moveDaysForward = 0;
      }
      moveDaysForward++;

      plusDay++;
    }
  }

  /**
   * Добавить дни до понедельника, если неделя начинается не с начала
   * @param firstDay Дата первого дня в месяце
   */
  private addDaysAtStart(firstDay: Date): void {
    const dayNumber = firstDay.getDay();
    let moveDaysBack = dayNumber === 0 ? 6 : dayNumber - 1;

    let minusDays = 0;
    // const daysInPrevMonth = this.getDaysInMonth(this.month - 1);

    while (moveDaysBack !== 0) {
      minusDays++;
      this._week.unshift(new DayOfMonthModel(
        new Date(this.year, this.month - 1, this.day - minusDays)
        ));
      moveDaysBack--;
    }
  }

  /**
   * Возвращает название дня недели
   * @param date Дата дня недели
   */
  private getDayName(date: Date): string {

    return Object.keys(WeekDayNames)
    .map((e: string, index: number) => WeekDayNames[e])
    .find((name: string, i: number) => i === date.getDay());
  }

  /**
   * Проверяет, принадлежит ли текущий день этому месяцу
   * @param month Текущий месяц
   */
  public outOfThisMonth(month: number): boolean {
    return month + 1  !== this.month;
  }

  /**
   * Нарисовать график работы
   * @param date Дата
   * @param canvas Ссылка на текущий холст
   */
  public drawGraph(canvas: HTMLCanvasElement): void {
    if (!canvas) {
      return;
    }

    this.painter = new DrawDaySchedule(canvas);
    window.requestAnimationFrame(() => this.painter.draw());
  }

}
