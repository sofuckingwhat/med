import { MonthNames } from '../../enums';
import { DayOfMonthModel } from './day-of-month.model';

/** Модель месяца для календаря */
export class MonthModel {

  public year: number;

  public title: string;

  public totalWorkHours = 0;

  public monthNumber: number;

  public days: DayOfMonthModel[] = [];

  public selectedDay: DayOfMonthModel = null;

  constructor(
    nm: MonthNames,
    num: number,
    yar: number
  ) {
    this.title = nm;
    this.monthNumber = num;
    this.year = yar;
    this.initDays();
  }

  private initDays(): void {
    this.days = [];
    const daysInThisMonth = this.getDaysInMonth();
    for (let index = 1; index <= daysInThisMonth; index++) {
      this.days.push(new DayOfMonthModel(new Date(this.year, this.monthNumber, index)));
    }

    const dayFirst = this.days[0];

    // Если первый день месяца не понедельник,
    // нужно докинуть дней до понедельника
    if (dayFirst.dayDate.getDay() !== 1) {
      this.addDaysAtStart(dayFirst.dayDate);
    }

    const dayLast = this.days[this.days.length - 1];

    // Если последний день месяца не воскресенье,
    // то нужно докинуть дней до воскресенья
    if (dayLast.dayDate.getDay() !== 0) {
      this.addDaysAtEnd(dayLast.dayDate);
    }
  }

  /**
   * Добавить дни до конца последней недели,
   * если последний день месяца не воскресенье
   * @param lastDay Дата последнего дня месяца
   */
  private addDaysAtEnd(lastDay: Date): void {
    let dayNumber = lastDay.getDay();
    let plusDay = 1;

    while (dayNumber !== 1) {
      this.days.push(new DayOfMonthModel(
        new Date(this.year, this.monthNumber + 1, plusDay)
      ));

      dayNumber++;
      if (dayNumber >= 6) {
        dayNumber = 0;
      }

      plusDay++;
    }
  }

  /**
   * Добавить дни до понедельника, если неделя начинается не с начала
   * @param firstDay Дата первого дня в месяце
   */
  private addDaysAtStart(firstDay: Date): void {
    const dayNumber = firstDay.getDay();
    let moveDaysBack = dayNumber === 0 ? 6 : dayNumber - 1;

    let minusDays = 0;
    const daysInPrevMonth = this.getDaysInMonth(this.monthNumber - 1);

    while (moveDaysBack !== 0) {
      this.days.unshift(new DayOfMonthModel(
        new Date(this.year, this.monthNumber - 1, daysInPrevMonth - minusDays)
        ));
      minusDays++;
      moveDaysBack--;
    }
  }

  /**
   * Количество дней в месяце
   * @param month Месяц, для которого нужно получить количество дней.
   * По умолчанию ставится текущий месяц.
   */
  private getDaysInMonth(month?: number): number {
    const onMonth = month !== undefined ? month : this.monthNumber;
    return new Date(this.year, onMonth + 1, 0).getDate();
  }

  public get isActive(): boolean {

    return this.monthNumber === new Date().getMonth();
  }

  public get isPast(): boolean {

    return this.monthNumber < new Date().getMonth();
  }
}
