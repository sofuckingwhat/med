import { SelectedRegion } from './drawer';
import { Subject } from 'rxjs';
import { TimePeriod } from './time-period.model';

/** Вспомогательный модуль для рисования расписания на день */
export class DrawDaySchedule {

  /** Событие клика на графике времени с указанием часа, где был клик */
  public setTime = new Subject<number>();

  /** Отступ с верх графика */
  private topOffset = 12;

  /** Курсор над графиком */
  private mouseOver: SelectedRegion = null;

  private timePeriods: TimePeriod[] = [];

  constructor(
    private readonly canvas: HTMLCanvasElement
  ) {
    this.mouseEvents();
  }

  /** Подписка на события мыши */
  private mouseEvents(): void {
    /** Отслеживаем курсор над графиком */
    this.canvas.onmousemove = (ev: MouseEvent) => this.onMouseMove(ev);

    /** Курсор вошел в область графика */
    this.canvas.onmouseover = () => {
      this.mouseOver = new SelectedRegion(35, this.topOffset);
    };

    /** Курсор покинул область графика */
    this.canvas.onmouseleave = () => this.mouseOver = null;

    this.canvas.onmouseup = (ev: MouseEvent) => this.onMouseClick(ev);
  }

  /** Контекст холста */
  private get ctx(): CanvasRenderingContext2D {

    return this.canvas.getContext('2d');
  }

  /** Цена деления одного часа на холсте */
  private get hourHeightInPixels(): number {

    return Math.floor(this.canvas.height / 24); // цена деления
  }

  public setPeriods(per: TimePeriod[]): void {

    this.timePeriods = per;
  }

  /** Рисуем график */
  public draw(): void {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.drawHoursGrid();
    this.drawWorkHours();
    this.drawSelectedRegion();

    window.requestAnimationFrame(() => this.draw());
  }

  /** Рисуем сетку 24 часов */
  private drawHoursGrid(): void {
    const hourHeight = Math.floor(this.canvas.height / 24);

    let cordY = this.topOffset;

    this.ctx.font = '14px sans-serif';

    this.ctx.beginPath();

    for (let index = 0; index < 25; index++) {

      this.ctx.strokeStyle = 'blue';
      this.ctx.lineWidth = 1;
      this.ctx.moveTo(3, cordY + 0.5);
      this.ctx.lineTo(this.canvas.width, cordY + 0.5);
      this.ctx.fillStyle = 'black';
      this.ctx.fillText(`${index.toString().padStart(2, '0')}:00`, 5, cordY - 1, 25);

      cordY += hourHeight;
    }

    this.ctx.stroke();
  }

  private drawWorkHours(): void {
    if (this.timePeriods.length <= 0) {
      return;
    }

    const tps = this.timePeriods;

    const hourHeight = this.hourHeightInPixels;

    tps.forEach((tp: TimePeriod) => {
      const tm = tp.start.split(':').map((s: string) => +s);
      let tlc = hourHeight * (tm[0] - 1); // отступ сверху по часам
      const minutesOffset = Math.ceil((60 - tm[1]) * (hourHeight / 60));
      tlc = tlc + minutesOffset;

    });

  }

  /** Рисуем маркер под курсором мыши на выбранном часе */
  private drawSelectedRegion(): void {
    if (!this.mouseOver) {
      return;
    }

    const hourHeight = this.hourHeightInPixels;

    if (this.mouseOver.mouseY < hourHeight) {
      return;
    }

    const cnt = Math.floor((this.mouseOver.mouseY - this.mouseOver.offsetY) / hourHeight); // кол-во делений

    const tlc = hourHeight * cnt + this.mouseOver.offsetY + 0.5; // нулевая точка прямоугольника

    this.ctx.beginPath();
    this.ctx.fillStyle = 'rgba(101,160,38,0.7)';

    this.ctx.strokeStyle = 'rgba(101,160,38)';
    this.ctx.lineWidth = 3;
    this.ctx.lineJoin = 'round';
    const rW = this.canvas.width - this.mouseOver.offsetX - 1;

    this.ctx.rect(this.mouseOver.offsetX, tlc, rW, hourHeight);
    this.ctx.fill();
    this.ctx.stroke();
  }

  /**
   * Передаем координаты мыши в конвертер часов
   * @param ev Событие мыши
   */
  private onMouseMove(ev: MouseEvent): void {
    if (this.mouseOver) {

      this.mouseOver.setMousePosition(ev.offsetX, ev.offsetY);
    }
  }

  /**
   * На каком часу был клик
   * @param ev Событие мыши
   */
  private onMouseClick(ev: MouseEvent): void {

    if (!this.mouseOver) {
      return;
    }

    const hourHeight = this.hourHeightInPixels;

    const hourClicked = Math.ceil((this.mouseOver.mouseY - this.mouseOver.offsetY) / hourHeight);

    this.setTime.next(hourClicked);
  }
}
