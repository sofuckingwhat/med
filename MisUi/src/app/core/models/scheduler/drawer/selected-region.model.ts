export class SelectedRegion {
  public mouseX: number;
  public mouseY: number;

  constructor(

    public offsetX: number,
    public offsetY: number
  ) {}

  public setMousePosition(x: number, y: number): void {

    this.mouseX = x;
    this.mouseY = y;
  }
}
