import { MonthModel } from './month.model';
import { MonthNames } from '../../enums';

/** Модель года */
export class YearModel {

  public year: number;

  public months: MonthModel[] = [];

  public selectedMonth: MonthModel;

  constructor(
    private readonly thisYear: number
  ) {
    this.year = thisYear;
    this.initMonths();
  }

  /** Является ли год текущим */
  private get isCurrent(): boolean {

    return this.year === new Date().getFullYear();
  }

  private initMonths(): void {

    this.months = Object.keys(MonthNames)
      .map((n: string, index: number) => new MonthModel(MonthNames[n], index, this.year));
  }
}
