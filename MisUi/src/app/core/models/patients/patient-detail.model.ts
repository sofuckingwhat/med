import { PatientListModel } from './patient-list.model';
import { PassportDto, AddressDto, PolisDto } from '../common';

/** Детальная модель пациента */
export class PatientDetailDto extends PatientListModel {

  public phones: string;
  public email: string;
  public inn: string;
  public snils: string;

  public passport: PassportDto;
  public address: AddressDto[];
  public polises: PolisDto[];
}
