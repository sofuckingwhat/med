/** Модель пациента для списков */
export class PatientListModel {

  /** Идентификатор пациента */
  public id: string;
  /** ФИО пациента */
  public fio: string;

  /** Возраст / полных лет */
  public age: number;

  /** Пол */
  public gender: string;

  /** Дата рождения */
  public birthday: string;

  /** Номер амбулаторной карты */
  public ambulanceCard: string;

  /** Дата внесения записи в бд */
  public registered: string;

  /** Источник информации */
  public informSource: string;

  /** Комментарий регистратуры */
  public comments: string;
}
