/** Модель доктора для списка */
export class DoctorListItemDto {
  /** Идентификатор доктора */
  public id: string;
  /** ФИО доктора */
  public fio: string;
  /** Дата внесения записи */
  public created: string;
}
