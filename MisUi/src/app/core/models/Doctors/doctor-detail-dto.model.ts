import { PeriodModel } from '../scheduler';

/** Детализированная модель доктора */
export class DoctorDetailDto {
  /** Идентификатор доктора */
  public id: string;

  /** Фамилия Имя Отчество доктора */
  public fio: string;

  /** График работы */
  public workTime: PeriodModel[];
}
