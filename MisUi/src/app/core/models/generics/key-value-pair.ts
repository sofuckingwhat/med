/** Модель ключ / значение */
export class KeyValuePair<K, V> {

  public key: K;

  public value: V;

  /**
   *
   * @param key Ключ
   * @param val Значение
   */
  constructor(key: K, val: V) {
    this.key = key;
    this.value = val;
  }
}
