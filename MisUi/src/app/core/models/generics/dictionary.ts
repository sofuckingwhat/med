import { KeyValuePair } from './key-value-pair';

/** Модель коллекции типа словарю. С набором ключ / значение */
export class Dictionary<K, V> {

  private items: KeyValuePair<K, V>[] = [];

  /**
   * Добавить элемент в коллекцию
   * @param pair Элемент для добавления в коллекцию
   */
  public setItem(pair: KeyValuePair<K, V>): void {

    const item = this.items.find((itm: KeyValuePair<K, V>) => itm.key === pair.key);

    if (!item) {
      this.items.push(pair);
      return;
    }

    throw new Error(`Запись с ключом "${pair.key}" уже есть в списке.`);
  }

  /** Получить коллекцию */
  public getList(): KeyValuePair<K, V>[] {

    return this.items;
  }

  /**
   * Получить элемент по его ключу
   * @param key Ключ элемента
   */
  public getItem(key: K): KeyValuePair<K, V> {

    return this.items.find((itm: KeyValuePair<K, V>) => itm.key === key);
  }

  /**
   * Удалить элемент по ключу
   * @param key Ключ элемента
   */
  public removeItem(key: K): void {

    const index = this.items.map((itm: KeyValuePair<K, V>) => itm.key).indexOf(key);

    this.items.splice(index, 0);

  }
}
