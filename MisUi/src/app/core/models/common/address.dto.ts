/** Информация о адресе */
export class AddressDto {

  public id = '';
  public address: string;
  public region: string;
  public city: string;
  public street: string;
  public building: string;
  public room: number;
  public addressType: string;
  public addressTypeId: string;
}
