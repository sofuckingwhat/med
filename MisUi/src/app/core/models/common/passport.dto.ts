/** Модель паспорта */
export class PassportDto {

  public id: string;
  public series: string;
  public number: string;
  public issuedDate: string;
  public issuerDepartment: string;
  public departmentCode: string;

}
