
/** Модель полиса медицинского страхования */
export class PolisDto {
  public id = '';
 /** Номер полиса */
  public number: string;
  /** Дата выдачи */
  public issuedDate: string;
  /** Организация, выдавшая полис */
  public issuer: string;
  /** Идентификатор организации */
  public issuerId: string;
}
