/** Модель запроса списка записей с пагинацией */
export class RequestListFilters{

  /** Страница */
  page = 1;

  /** Количество страниц */
  perPage = 20;

  /** Поисковый запрос */
  queryString = '';
}
