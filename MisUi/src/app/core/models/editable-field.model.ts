export class EditableField<T> {

  /** В режиме редактирования */
  editMode: boolean;

  /** Модель */
  item: T;
}
