import { Injectable } from '@angular/core';

// Services
import { OidcConfigService, OidcSecurityService, AuthWellKnownEndpoints, OpenIDImplicitFlowConfiguration } from 'angular-auth-oidc-client';

// RxJs
import { Subscription, Observable, of } from 'rxjs';
import { filter, take, flatMap } from 'rxjs/operators';
import { IStsConfigInterface } from '../../interfaces';

/** Сервис авторизации пользователей */
@Injectable()
export class AuthService {

  private _initResult: boolean;

  private _subs: Subscription[];

  constructor(
    private readonly configService: OidcConfigService,
    private readonly securityService: OidcSecurityService
  ) {
    this.initSubscriptions();
  }

  /** Авторизация возможна? */
  public get canAuthorize(): boolean {

    return this._initResult;
  }

  /** Возвращает токен пользователя */
  public get token(): string {

    return this.securityService.getToken();
  }

  /** Возвращает данные текущего пользователя */
  public get currentUser$(): Observable<any> {

    return this.securityService.getUserData();
  }

  /** Пользователь авторизован */
  public get isAuthorized$(): Observable<boolean> {

    return this.securityService.getIsAuthorized();
  }

  /** Вход пользователя */
  public login(): void {
    this.securityService.authorize();
  }

  /** Выход из системы */
  public logout(): void {
    this.securityService.logoff();
  }

  /** Инициализирует подписки */
  private initSubscriptions(): void {
    this._subs = [
      this.configService.onConfigurationLoaded
        .subscribe((result: boolean) => {
          if (result) {
            this._initResult = result;
            const openIdImplicitFlowConfiguration = new OpenIDImplicitFlowConfiguration();
            const stsConfig = this.configService.clientConfiguration;
            Object.assign(openIdImplicitFlowConfiguration, stsConfig);
            const authWellKnownEndpoints = new AuthWellKnownEndpoints();
            authWellKnownEndpoints.setWellKnownEndpoints(this.configService.wellKnownEndpoints);
            this.securityService.setupModule(openIdImplicitFlowConfiguration, authWellKnownEndpoints);
          }
        }),
      this.securityService.getIsModuleSetup()
        .pipe(
          filter((isModuleSetup: boolean) => isModuleSetup),
          take(1)
        )
        .subscribe((isModuleSetup: boolean) => {
          if (isModuleSetup) {
            this.doCallBackLogicIfRequired();
          }
        }),
      this.securityService.getIsAuthorized()
        .pipe(
          flatMap((result: boolean) => {
            if (result) {
              return this.securityService.getUserinfo();
            }

            return of(null);
          })
        ).subscribe(),
        this.securityService.onCheckSessionChanged.subscribe(
          (checksession: boolean) => {
              if (checksession) {
                this.securityService.authorize();
              }
          })
    ];

  }

  private getTokenHash(): string {
    if (typeof localStorage !== 'undefined' && window.location.hash) {
      const indexHash = window.location.hash.indexOf('id_token');

      return indexHash > -1 && window.location.hash.substr(indexHash);
    }
  }

  private doCallBackLogicIfRequired(): void {
    const hash = this.getTokenHash();

    if (hash) {
      this.securityService.authorizedImplicitFlowCallback(hash);
    }
  }

  /** Инициализация сервиса авторизации */
  public initialize(): void {
    this.initSubscriptions();
  }
}
