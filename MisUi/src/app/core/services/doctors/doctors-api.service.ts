import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import {
  DoctorDetailDto,
  DoctorListItemDto,
  RequestListFilters
} from '../../models';

import { DoctorsUrls } from '../../urls';

import { ApiHelper } from '../api-helper.service';
import { IResponseList } from '../../interfaces';

/** Сервис для работы с разделом докторов */
@Injectable({
  providedIn: 'root'
})
export class DoctorsApiService {

  constructor(
    private readonly http: HttpClient,
    private readonly helper: ApiHelper
  ) {}

  /**
   * Запрос списка докторов
   * @param filters Параметры запроса
   */
  public getList(filters: RequestListFilters): Observable<IResponseList<DoctorListItemDto>> {

    const queryParams  = this.helper.encodeParamsToUrl(DoctorsUrls.baseUrl, filters);

    return this.http.get<IResponseList<DoctorListItemDto>>(queryParams);
  }

  public getDoctor(id: string): Observable<DoctorDetailDto> {

    return this.http.get<DoctorDetailDto>(`${DoctorsUrls.baseUrl}/${id}`);
  }
  /**
   * Создание новой записи о докторе
   * @param model Модель данных о докторе
   */
  public createDoctor(model: DoctorDetailDto): Observable<DoctorDetailDto> {

    return this.http.post<DoctorDetailDto>(DoctorsUrls.baseUrl, model);
  }

  /**
   * Изменить данные о докторе
   * @param model Данные о докторе
   */
  public updateDoctor(model: DoctorDetailDto): Observable<DoctorDetailDto> {

    return this.http.put<DoctorDetailDto>(DoctorsUrls.baseUrl, model);
  }
}
