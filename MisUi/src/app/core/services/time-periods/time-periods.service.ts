import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PeriodModel } from '../../models';
import { DoctorsUrls } from '../../urls';
import { PeriodsUrls } from '../../urls/periods/time-periods.urls';

/** Сервис для работы с расписанием */
@Injectable({
  providedIn: 'root'
})
export class TimePeriodsService {

  constructor(
    private readonly http: HttpClient
  ) {}


  public saveWorkTime(model: PeriodModel): Observable<PeriodModel> {

    return this.http.post<PeriodModel>(PeriodsUrls.baseUrl, model);
  }
}
