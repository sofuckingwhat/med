import { Injectable } from '@angular/core';

/** Сервис управления лоадерами */
@Injectable({
  providedIn: 'root'
})
export class LoadersService {

  private preLoaderScreen: HTMLElement;

  constructor () {
    this.preLoaderScreen = document.getElementById('pre-loader');
  }

  /** Скрыть главный экран загрузки приложения */
  public hidePreLoader(): void {
    if (this.preLoaderScreen === null) {
      return;
    }

    this.preLoaderScreen.classList.add('hidden');
  }

  /** Показать главный экран загрузки приложения */
  public showPreLoader(): void {
    if (this.preLoaderScreen === null) {
      return;
    }

    this.preLoaderScreen.classList.remove('hidden');
  }

}
