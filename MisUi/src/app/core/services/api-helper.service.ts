import { Injectable } from '@angular/core';

/** Вспомогательные методы для апи */
@Injectable({
  providedIn: 'root'
})
export class ApiHelper {

  /**
   * Добавляем параметры к запросу.
   * @param originalUrl Исходный адрес
   * @param params Объект, который конвертируем в строку запроса
   */
  public encodeParamsToUrl(originalUrl: string, params: object): string {

    const queryParams = Object.keys(params)
    .map((key: string) => {
      if (params[key] && params[key] !== 'object') {
        return `${key}=${params[key]}`;
      }
    })
    .filter((param: string) => param && param.length > 0)
    .join('&');

    let result = originalUrl;

    if (queryParams) {
      result += `?${queryParams}`;
    }

    return result;
  }

}
