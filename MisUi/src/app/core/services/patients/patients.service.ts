import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PatientsUrls } from '../../urls';
import { FirstVisitPatientModel, PatientListModel, PatientDetailDto } from '../../models';
import { IResponseList } from '../../interfaces';

@Injectable({
  providedIn: 'root'
})
export class PatientsService {

  constructor(
    private readonly http: HttpClient
  ) {}

  /** Получить список пациентов клиники */
  public getPatients(): Observable<IResponseList<PatientListModel>> {

    return this.http.get<IResponseList<PatientListModel>>(PatientsUrls.baseUrl);
  }

  /**
   * Получить данные пациента
   * @param id Идентификатор пациента
   */
  public getPatientDetail(id: string): Observable<PatientDetailDto> {

    return this.http.get<PatientDetailDto>(`${PatientsUrls.baseUrl}/${id}`);
  }

  /**
   * Создать пациента
   * @param item Первичные данные пациента
   */
  public createPatient(item: FirstVisitPatientModel): Observable<PatientListModel> {

    return this.http.post<PatientListModel>(PatientsUrls.baseUrl, item);
  }

  /**
   * Обновить данные пациента
   * @param item Данные пациента
   */
  public updatePatient(item: PatientDetailDto): Observable<PatientDetailDto> {

    return this.http.put<PatientDetailDto>(PatientsUrls.baseUrl, item);
  }
}
