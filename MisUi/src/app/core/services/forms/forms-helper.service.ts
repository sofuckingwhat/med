import { HttpErrorResponse } from '@angular/common/http';
import { IBackValidationError } from '../../interfaces/errors';
import { Dictionary, KeyValuePair } from '../../models/generics';
import { Injectable } from '@angular/core';

/** Вспомогательные методы для валидации форм */
@Injectable({
  providedIn: 'root'
})
export class FormsHelperService {

  /**
   * Извлекает ошибки при валидации форм на серверной стороне
   * @param response Bad request 400
   */
  public backSideValidationErrors(response: HttpErrorResponse): IBackValidationError {

    const backErrors = {
      status: response.error.status,
      title: response.error.title,
      traceId: response.error.traceId,
      errors: new Dictionary<string, string[]>()
    };

    const er = Object.keys(response.error.errors);

    if (er && er.length > 0) {
      er.forEach((e: string) => {
        backErrors.errors.setItem(new KeyValuePair<string, string[]>(e, response.error.errors[e]));
      });
    }

    return backErrors;
  }
}
