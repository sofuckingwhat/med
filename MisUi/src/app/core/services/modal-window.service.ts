import { Injectable } from '@angular/core';

import { Subject, Observable, Observer } from 'rxjs';

import { IModalWindowParams } from '../interfaces';

import { ModalEventsEnum } from '../enums';

@Injectable({
  providedIn: 'root'
})
export class ModalWindowService {

  private readonly popupDialog = new Subject<IModalWindowParams>();

  public popupDialogEvents = this.popupDialog.asObservable();

  /**
   * Открыть модальное окно
   * @param component Компонент модального окна
   * @param options Доп. параметры
   */
  public open(component: any, options?: any): Observable<any> {
    const modalParams: IModalWindowParams = {
      popupEvent: ModalEventsEnum.Open,
      component,
      options
    };

    this.popupDialog.next(modalParams);

    return new Observable((observer: Observer<any>): any => {
      this.popupDialog.subscribe((event: IModalWindowParams) => {
        if (event.popupEvent === ModalEventsEnum.Close) {
          observer.next(event);
          observer.complete();
        }
      });
    });
  }

  /** Закрыть модальное окно */
  public close(action: any): void {
    this.popupDialog.next({ popupEvent: ModalEventsEnum.Close, response: action });
  }
}
