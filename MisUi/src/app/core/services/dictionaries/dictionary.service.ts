import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// Rxjs
import { Observable } from 'rxjs';

// Urls
import { Urls } from 'src/app/core/constants';
import { DictionaryModel, DictionaryDetailModel, DictionaryItemModel } from '../../models/dictionary';

// Models

/** Сервис работы со словарями */
@Injectable({
  providedIn: 'root'
})
export class DictionaryService {

  constructor(
    private readonly http: HttpClient
  ) {}

  /** Получить список всех справочников */
  public getDictionaries(): Observable<DictionaryModel[]> {

    return this.http.get<DictionaryModel[]>(Urls.dictionaries);
  }

  /**
   * Получить справочник в развернутом виде
   * @param id Идентификатор справочника
   */
  public getDictionary(id: string): Observable<DictionaryDetailModel> {

    return this.http.get<DictionaryDetailModel>(`${Urls.dictionaries}/${id}`);
  }

  /**
   * Получить данные справочника по его наименованию
   * @param name Наименование справочника
   */
  public getDictionaryByName(name: string): Observable<DictionaryDetailModel> {

    return this.http.get<DictionaryDetailModel>(`${Urls.dictionaryByName}/${name}`);
  }

  /**
   * Создание / редактирование справочника
   * @param item Модель справочника
   */
  public updateDictionary(item: DictionaryModel): Observable<DictionaryModel> {

    const request = item.id
      // Обновить справочник
      ? this.http.put<DictionaryModel>(Urls.dictionaries, item)
      // Создать справочник
      : this.http.post<DictionaryModel>(Urls.dictionaries, item);

      return request;
  }

  /**
   * Создает запись в справочнике
   * @param item Модель записи в справочнике
   */
  public createDictionaryItem(item: DictionaryItemModel): Observable<DictionaryItemModel> {

    return this.http.post<DictionaryItemModel>(Urls.dictionariesItems, item);
  }

  /**
   * Обновляет запись в справочнике
   * @param item Модель записи в справочнике
   */
  public updateDictionaryItem(item: DictionaryItemModel): Observable<DictionaryItemModel> {

    return this.http.put<DictionaryItemModel>(Urls.dictionariesItems, item);
  }
}
