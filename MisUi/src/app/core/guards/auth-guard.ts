import { CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { CanLoad, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from '../services/authentication';
import { Observable } from 'rxjs';
import { tap, take } from 'rxjs/operators';
import { LoadersService } from '../services';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad {

  constructor(
    private readonly router: Router,
    private readonly securityService: AuthService,
    private readonly loader: LoadersService
  ) {}

  public canActivate(): Observable<boolean> | boolean {
    // return true;
    return this.checkUser();
  }

  public canLoad(): Observable<boolean> | boolean {
    return true;

    // return this.checkUser();
  }

  private checkUser(): Observable<boolean> | boolean {

    if (!environment.useAuthentication) {
      this.loader.hidePreLoader();

      return true;
    }

    return this.securityService.isAuthorized$
    .pipe(
        tap((isAuthorized: boolean) => {

          if (!isAuthorized) {
            this.router.navigate(['/unauthorized']);
            return false;
          }
            this.loader.hidePreLoader();
            return true;
        }),
        take(1)
    );
  }
}
