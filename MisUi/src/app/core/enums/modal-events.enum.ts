/** Перечисление событий модального окна */
export enum ModalEventsEnum {
  Open = 'open',
  Close = 'close'
}
