import { LOCALE_ID,  NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// i18n
import { registerLocaleData } from '@angular/common';
import localeRu from '@angular/common/locales/ru';
registerLocaleData(localeRu);
// Icons
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faCalendarPlus
} from '@fortawesome/free-solid-svg-icons';

// Modules
import { AppRoutingModule } from './app-routing.module';
import {
  AuthModule,
  OidcSecurityService,
  OidcConfigService
} from 'angular-auth-oidc-client';

// Components
import { AppComponent } from './app.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ForbiddenComponent } from './components/forbidden/forbidden.component';
import { UnauthorizedComponent } from './components/unauthorized/unauthorized.component';

// Initializers
import * as inits from './core/initializers';

// Services
import { AuthService } from './core/services/authentication';
import { AuthInterceptor } from './core/interceptors';
import { AuthGuard } from './core/guards';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    ForbiddenComponent,
    UnauthorizedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AuthModule.forRoot(),
    FontAwesomeModule,
    ReactiveFormsModule
  ],
  providers: [
    OidcConfigService,
    OidcSecurityService,
    AuthService,
    // Localization
    {
      provide: LOCALE_ID, useValue: 'ru'
    },
    // Initializers
    inits.StsInitializer,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },

    // Guards
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
    library.add(faCalendarPlus);
  }
}
