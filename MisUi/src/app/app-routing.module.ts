import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { NotFoundComponent, ForbiddenComponent } from './components';
import { UnauthorizedComponent } from './components/unauthorized/unauthorized.component';
import { AuthGuard } from './core/guards';

/** Основные маршруты */
const routes: Routes = [
  // Основной модуль
  {
    path: '',
    loadChildren: './modules/main-module/main.module#MainModule',
    canActivate: [AuthGuard],
    canLoad: [AuthGuard]
  },
  // Страница 403 доступ ограничен
  {
    path: 'forbidden',
    component: ForbiddenComponent
  },
  {
    path: 'unauthorized',
    component: UnauthorizedComponent
  },
  // Страница 404
  {
    path: '**',
    component: NotFoundComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
