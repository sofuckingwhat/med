import { Component, OnInit } from '@angular/core';

// Services
import { LoadersService } from 'src/app/core';

// Environments
import { environment } from 'src/environments/environment';
import { AuthService } from 'src/app/core/services/authentication';

@Component({
  selector: 'mis-unauthorized',
  templateUrl: './unauthorized.component.html',
  styleUrls: ['./unauthorized.component.scss']
})
export class UnauthorizedComponent implements OnInit {

  constructor (
    private readonly loaders: LoadersService,
    private readonly auth: AuthService
  ) {}

  public login() {
    this.auth.login();
  }

  public ngOnInit(): void {

    setTimeout(() => {
      this.loaders.hidePreLoader();
    }, environment.preloaderTime);
  }

}
