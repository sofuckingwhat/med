import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { DayOfMonthModel, DrawDaySchedule, ModalWindowService, PeriodModel, TimePeriod, IModalWindowParams } from 'src/app/core';
import { flatMap } from 'rxjs/operators';

import { AddPeriodModalComponent } from './modals';

@Component({
  selector: 'mis-workday-canvas',
  templateUrl: './workday-canvas.component.html',
  styleUrls: ['./workday-canvas.component.scss']
})
export class WorkdayCanvasComponent implements OnInit, AfterViewInit {

  @Input() public objectId: string;

  @Input() public objectTitle: string;

  @Input() public weekDay: DayOfMonthModel;

  @Input() public parentElementSelector: string;

  constructor(
    private readonly ms: ModalWindowService
  ) { }

  /** Рисуем графики рабочего времени, если в этом есть необходимость */
  private initWorkGraphics(): void {
    // Контейнер, где мы будем размещать холст
    const canvasContainer = document.getElementById(this.parentElementSelector);

    // Блок с холстом
    const canvasBlock = canvasContainer.children.item(0).children.item(0);

    // Высота холста (может быть вычисляемым полем, но оно должно вычисляться до прорисовки холста)
    const cH = 445;

    // Ширина холста
    const cW = canvasBlock.clientWidth;

    // Холст
    const canvas = document.createElement('canvas');
    canvas.width = cW;
    canvas.height = cH;
    canvas.classList.add('canvas');

    // Устанавливаем холст в его блок
    canvasBlock.appendChild(canvas);

    // Рисуем расписание
    this.weekDay.drawGraph(canvas);

    this.initCanvasSubscriptions(this.weekDay.painter);
  }

  private initCanvasSubscriptions(drawer: DrawDaySchedule): void {
    if (!drawer || !drawer.setTime) {
      return;
    }

    drawer.setTime
      .pipe(
        flatMap((hour: number) => {

          const tp = new TimePeriod();
          tp.start = `${hour.toString().padStart(2, '0')}:00`;
          tp.end = `${(hour + 1).toString().padStart(2, '0')}:00`;

          this.weekDay.workingHours.workTime.push(tp);

          return this.ms.open(AddPeriodModalComponent, this.weekDay.workingHours);
        })
      )
      .subscribe((data: IModalWindowParams) => {
        console.log(data);
        if (data.response) {
          this.weekDay.workingHours = data.response;

          const a = this.weekDay;
          this.weekDay.painter.setPeriods(this.weekDay.workingHours.workTime);
        }
      });
  }

  public ngOnInit(): void {
    // TODO!: Запросить актуальные данные о рабочем времени с сервера
    const period = new PeriodModel();
    period.periodDate = this.weekDay.dayDate;
    period.objectId = this.objectId;
    period.title = this.objectTitle;

    this.weekDay.workingHours = period;
  }

  public ngAfterViewInit(): void {
    this.initWorkGraphics();
  }
}
