import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';


import {
  FormsHelperService,
  IModalComponent,
  ModalWindowService,
  PeriodModel,
  DictionaryService,
  TimePeriodsService,
  TimePeriod,
  Dictionary,
  DictionaryDetailModel,
  DictionaryItemModel,
  KeyValuePair
} from 'src/app/core';

import { Observable, Observer, of } from 'rxjs';

import { FormBaseComponent } from '../../../forms-base.component';
import { flatMap, delay } from 'rxjs/operators';

@Component({
  selector: 'mis-add-period-modal',
  templateUrl: './add-period-modal.component.html',
  styleUrls: ['./add-period-modal.component.scss']
})
export class AddPeriodModalComponent extends FormBaseComponent implements IModalComponent, OnInit {

  /** Данные модального окна */
  @Input() public data: PeriodModel;

  public form: FormGroup;

  public periodsTypes = new Dictionary<string, string>();

  constructor(
    private readonly fh: FormsHelperService,
    private readonly fb: FormBuilder,
    private readonly ms: ModalWindowService,
    private readonly ds: DictionaryService,
    private readonly tp: TimePeriodsService
  ) {
    super(fh, fb);
  }

  public get inProgress(): boolean {
    return !this.formReady;
  }

  public onSubmit(): void {
    if (this.form.invalid) {
      this.showErrors = true;
      return;
    }
    console.log(JSON.stringify(this.form.value, null, 1));

    this.tp.saveWorkTime(this.form.value)
      .subscribe((period: PeriodModel) => {
        this.ms.close(period);
      });
  }

  public get timePeriods(): FormArray {

    return this.form.get('periods') as FormArray;
  }

  public close(): void {
    this.ms.close(false);
  }

  private buildForm(): Observable<FormGroup> {

    return new Observable<FormGroup>((observer: Observer<FormGroup>) => {

      try {

        const form = this.fb.group({
          id: [this.data.objectId],
          date: [this.data.periodDate],
          periods: this.fb.array(
            this.data.workTime.map((tm: TimePeriod) => this.setPeriod(tm))
          )
        });

        observer.next(form);
        observer.complete();

      } catch (error) {
        observer.error(error);
      }
    });
  }

  private setPeriod(time: TimePeriod): FormGroup {

    return this.fb.group({
      id: [time.id],
      start: [time.start, Validators.required],
      end: [time.end, Validators.required],
      periodType: [time.periodType, Validators.required]
    });
  }

  public addPeriod(): void {
    const prevPeriod = this.timePeriods.at(this.timePeriods.length-1);
    if (prevPeriod) {
      const tp = new TimePeriod;
      tp.start = prevPeriod.get('end').value;
      tp.end = prevPeriod.get('end').value;
      this.timePeriods.push(this.setPeriod(tp));
      return;
    }
    this.timePeriods.push(this.setPeriod(new TimePeriod()));
  }

  public removePeriod(index: number): void {
    this.timePeriods.removeAt(index);
  }
  public ngOnInit(): void {
    this.formReady = false;
    this.ds.getDictionaryByName('tip-rabochego-vremeni')
      .pipe(
        flatMap((types: DictionaryDetailModel) => {

          if(types.items) {
            types.items
            .map(
              (item: DictionaryItemModel) => this.periodsTypes.setItem(new KeyValuePair<string, string>(item.id, item.shortName))
              );
            }
            return this.buildForm();
          }),
          delay(1000)
      ).subscribe((form: FormGroup) => {
        this.form = form;
        this.formReady = true;
      });
  }

}
