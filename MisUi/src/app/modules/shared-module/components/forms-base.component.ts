import { FormsHelperService } from 'src/app/core';
import { FormBuilder } from '@angular/forms';

export abstract class FormBaseComponent {

  /** Показать ошибки */
  public showErrors: boolean;

  /** Форма готова */
  public formReady = false;

  constructor(
    protected readonly formHelper: FormsHelperService,
    protected readonly formBuilder: FormBuilder
  ) { }

  /** Контент в стадии загрузки */
  public abstract get inProgress(): boolean;

  /** Сохранить данные формы */
  public abstract onSubmit(): void;
}
