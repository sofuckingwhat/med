import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import * as components from './components';
import { ReactiveFormsModule } from '@angular/forms';

const sharedComponents = [
  components.LoaderComponent,
  components.WorkdayCanvasComponent,
  components.AddPeriodModalComponent
];

@NgModule({
  declarations: sharedComponents,
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: sharedComponents
})
export class SharedModule { }
