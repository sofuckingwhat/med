import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import * as components from './main.component';

const routes: Routes = [
  {
    // host/registry
    path: '',
    component: components.MainComponent,
    children: [
      {path: '', redirectTo: 'registry', pathMatch: 'full'},
      {
        path: 'registry',
        loadChildren: '../registry-module/registry.module#RegistryModule'
      },
      {
        path: 'patients',
        loadChildren: '../patients/patients.module#PatientsModule'
      },
      {
        path: 'settings',
        loadChildren: '../settings/settings.module#SettingsModule'
      },
      {
        path: 'doctors',
        loadChildren: '../doctors/doctors.module#DoctorsModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
