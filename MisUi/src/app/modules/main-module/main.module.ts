import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

// Components
import { NewPatientComponent } from '../patients';

// Modules
import { MainRoutingModule } from './main.routing';
import { MainComponent } from './main.component';
import { UiModule } from '../ui-module';

import { AddPeriodModalComponent } from '../shared-module';

@NgModule({
  declarations: [MainComponent],
  imports: [
    CommonModule,
    MainRoutingModule,
    UiModule
  ],
  entryComponents: [
    NewPatientComponent,
    AddPeriodModalComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MainModule { }
