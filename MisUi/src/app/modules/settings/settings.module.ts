import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { SettingsRoutingModule } from './settings.routing';
import { DictionariesComponent } from './components';
import { MainSettingsComponent } from './main.component';

@NgModule({
  declarations: [
    DictionariesComponent,
    MainSettingsComponent
  ],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ]
})
export class SettingsModule { }
