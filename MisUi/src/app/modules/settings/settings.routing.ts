import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DictionariesComponent } from './components';
import { MainSettingsComponent } from './main.component';

const routes: Routes = [
  {
    path: '',
    component: MainSettingsComponent,
    children: [
      { path: 'dictionaries', component: DictionariesComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
