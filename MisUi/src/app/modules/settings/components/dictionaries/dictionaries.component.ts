import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// Services
import { DictionaryService } from 'src/app/core/services/dictionaries';

// RxJs
import { delay } from 'rxjs/operators';

// Models
import { DictionaryModel, DictionaryDetailModel, DictionaryItemModel, DictionaryItemPostModel } from 'src/app/core/models/dictionary';
import { Subscription } from 'rxjs';

@Component({
  selector: 'mis-dictionaries',
  templateUrl: './dictionaries.component.html',
  styleUrls: ['./dictionaries.component.scss']
})
export class DictionariesComponent implements OnInit, OnDestroy {

  private subs: Subscription[] = [];
  /** Список справочников */
  private dictionaries: DictionaryModel[] = [];

  /** Отфильтрованный список справочников */
  public filteredDictionaries: DictionaryModel[] = [];

  /** Справочник с детализацией */
  public selectedDictionary: DictionaryDetailModel;

  /** Индикатор загрузки данных */
  public onProgress: boolean;

  /** Строка поиска по справочникам */
  public searchForm: FormGroup;

  /** Добавление / редактирование справочника */
  public editDictionary: FormGroup;

  /** Добавление / редактирование записи справочника */
  public editDictionaryItem: FormGroup;

  constructor(
    private readonly api: DictionaryService,
    private readonly fb: FormBuilder
  ) { }

  /**
   * Создание / редактирование справочника.
   * @param item Модель справочника для редактирования
   */
  public createEditDictionary(item?: DictionaryModel): void {
    const model = item !== null ? item : new DictionaryModel();

    this.editDictionary = this.fb.group({
      id: [model.id],
      title: [model.title, Validators.required],
      description: [model.description, Validators.required]
    });
  }

  /** Сохранить данные справочника */
  public submitDictionary(): void {
    this.api.updateDictionary(this.editDictionary.value)
      .subscribe((response: DictionaryModel) => {
        const dictionary = this.dictionaries.find((d: DictionaryModel) => d.id === response.id);

        if (dictionary) {
          dictionary.title = response.title;
          dictionary.description = response.description;
        } else {
          this.dictionaries.push(response);
        }

        this.editDictionary = null;
      });
  }

  /** Отмена редактирования / создания справочника */
  public cancelDictionary(): void {
    this.editDictionary = null;
  }

  /** Форма создания записи в справочнике */
  public createEditDictionaryItem(item?: DictionaryItemModel): void {

    const model = item ? item : new DictionaryItemModel();

    this.editDictionaryItem = this.fb.group({
      dictionaryId: [this.selectedDictionary.id, Validators.required],
      id: [model.id],
      shortName: [model.shortName, Validators.required],
      fullName: [model.fullName, Validators.required],
      code: [model.code]
    });
  }

  /** Отмена редактирования записи справочника */
  public cancelDictionaryItemEdit(): void {
    this.editDictionaryItem = null;
  }

  /** Создает / сохраняет запись в справочнике */
  public submitDictionaryItem(): void {
    if (this.editDictionaryItem.valid) {
      const item: DictionaryItemPostModel = this.editDictionaryItem.value;

      if (item.id) {
        this.updateDictionaryItem(item);
      } else {
        this.createDictionaryItem(item);
      }
    }

  }

  /** Создать запись в справочнике */
  private createDictionaryItem(item: DictionaryItemPostModel): void {

    this.api.createDictionaryItem(item)
      .subscribe((response: DictionaryItemModel) => {
        this.selectedDictionary.items.unshift(item);
        this.editDictionaryItem = null;
    });
  }

  /** Редактировать запись в справочнике */
  private updateDictionaryItem(item: DictionaryItemPostModel): void {
    this.api.updateDictionaryItem(item)
      .subscribe((response: DictionaryItemModel) => {
        const itm = this.selectedDictionary.items.find((i: DictionaryItemModel) => i.id === response.id);
        if (itm) {
          itm.fullName = response.fullName;
          itm.shortName = response.shortName;
          this.editDictionaryItem = null;
        }
      });
  }

  /** Выбор текущего справочника */
  public selectDictionary(id: string): void {
    if (id !== null) {
      this.api.getDictionary(id)
        .subscribe((model: DictionaryDetailModel) => {
          this.selectedDictionary = model;
        });
    }
  }

  /** Строит форму для строки поиска */
  private buildSearchForm(): void {

    this.searchForm = this.fb.group({
      searchString: []
    });
  }

  private initSubs(): void {
    this.subs = [
      this.searchForm.get('searchString').valueChanges
        .subscribe((val: string) => {
          if (!val || val.length === 0) {
            this.filteredDictionaries = this.dictionaries;
            return;
          }
          this.filteredDictionaries = this.dictionaries.filter((dictionary: DictionaryModel) => {
            return dictionary.title.toLowerCase().startsWith(val.toLowerCase());
          });
        })
    ];
  }
  public ngOnInit(): void {
    this.onProgress = true;
    this.buildSearchForm();
    this.api.getDictionaries()
      .pipe(
        delay(1000)
      )
      .subscribe((items: DictionaryModel[]) => {
        this.dictionaries = items;
        this.filteredDictionaries = items;
        this.initSubs();
        this.onProgress = false;
      });
  }

  public ngOnDestroy(): void {
    this.subs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
}
