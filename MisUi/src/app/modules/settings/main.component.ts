import { Component, OnInit } from '@angular/core';

/** Основной компонент раздела настроек */
@Component({
  selector: 'mis-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainSettingsComponent implements OnInit {

  constructor() { }

  public ngOnInit(): void {
  }

}
