import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import * as components from './components';
import { MainRegistryComponent } from './main.component';

const routes: Routes = [
  {
    path: '',
    component: MainRegistryComponent,
    children: [
      {path: '', redirectTo: 'index', pathMatch: 'full'},
      {path: 'index', component: components.RegistryIndexComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegistryRoutingModule { }
