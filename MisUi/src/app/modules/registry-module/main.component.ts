import { Component, OnInit } from '@angular/core';

/** Компонент маршрутизатор "Регистратуры" */
@Component({
  selector: 'mis-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainRegistryComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
