import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegistryRoutingModule } from './registry.routing';
import { MainRegistryComponent } from './main.component';
import { RegistryIndexComponent } from './components/registry-index/registry-index.component';

@NgModule({
  declarations: [
    MainRegistryComponent,
    RegistryIndexComponent,
  ],
  imports: [
    CommonModule,
    RegistryRoutingModule,
  ]
})
export class RegistryModule { }
