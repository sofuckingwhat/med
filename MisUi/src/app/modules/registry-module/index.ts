export * from './components';

export { MainRegistryComponent } from './main.component';
export { RegistryModule } from './registry.module';
export { RegistryRoutingModule } from './registry.routing';
