import {
  Component,
  AfterViewInit,
  OnDestroy,
  ViewChild,
  ViewContainerRef,
  ComponentFactoryResolver,
  ElementRef
} from '@angular/core';

import { Subscription } from 'rxjs';
import { ModalWindowService, IModalWindowParams, IModalComponent } from 'src/app/core';
import { ModalEventsEnum } from 'src/app/core/enums';

@Component({
  selector: 'mis-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements AfterViewInit, OnDestroy {
  private subscription: Subscription;

  @ViewChild('dynamicModalComponent', { read: ViewContainerRef })
  public modalHost;

  constructor(
    private readonly modalWindowService: ModalWindowService,
    private readonly componentFactoryResolver: ComponentFactoryResolver,
    private readonly elementRef: ElementRef,
    private viewContainerRef: ViewContainerRef
  ) {}

  // Открыть модальное окно
  private open(data: IModalWindowParams): void {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(data.component);

    this.viewContainerRef = this.modalHost;

    // Удаление ранее открытых модальных окон
    this.viewContainerRef.clear();

    const componentRef = this.viewContainerRef.createComponent(componentFactory);
    const componentInstance = componentRef.instance as IModalComponent;

    // Наполнение компонента данными
    componentInstance.data = data.options;
  }

  // Закрыть модальное окно
  private close(): void {
    this.viewContainerRef.detach(0);
  }

  public ngAfterViewInit(): void {
    this.subscription = this.modalWindowService
      .popupDialogEvents.subscribe((data: IModalWindowParams) => {
        if (!!data && data.popupEvent === ModalEventsEnum.Open) {
          this.open(data);

          // Добавление класса для отображения модального окна
          this.elementRef.nativeElement.firstElementChild.classList.add('modal-template--show');

        } else if (data && data.popupEvent === ModalEventsEnum.Close) {

          this.close();

          // Удаление класса для отображения модального окна
          this.elementRef.nativeElement.firstElementChild.classList.remove('modal-template--show');
        }
      });
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
