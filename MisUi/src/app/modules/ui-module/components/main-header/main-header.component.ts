import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/authentication';

@Component({
  selector: 'mis-main-header',
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.scss']
})
export class MainHeaderComponent implements OnInit {

  public toggleUserMenu = false;

  public isAuthenticated = false;

  constructor(
    private readonly auth: AuthService
  ) { }

  public menu(): void {
    this.toggleUserMenu = !this.toggleUserMenu;
  }

  public logoff(): void {
    this.auth.logout();
  }
  public ngOnInit(): void {
    this.auth.isAuthorized$
      .subscribe((result: boolean) => {
        this.isAuthenticated = result;
      });
  }

}
