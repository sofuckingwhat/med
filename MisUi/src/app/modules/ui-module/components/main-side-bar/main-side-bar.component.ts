import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/authentication';

@Component({
  selector: 'mis-main-side-bar',
  templateUrl: './main-side-bar.component.html',
  styleUrls: ['./main-side-bar.component.scss']
})
export class MainSideBarComponent implements OnInit {

  constructor(private readonly auth: AuthService) { }

  public login(): void {

    this.auth.login();
  }
  ngOnInit() {
  }

}
