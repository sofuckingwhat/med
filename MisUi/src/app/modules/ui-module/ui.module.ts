import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// Icons
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faUsers,
  faUserMd,
  faCogs
} from '@fortawesome/free-solid-svg-icons';

import * as components from './components';

const uiComponents = [
  components.MainHeaderComponent,
  components.MainSideBarComponent,
  components.ModalComponent
];

@NgModule({
  declarations: uiComponents,
  imports: [
    CommonModule,
    FontAwesomeModule,
    RouterModule
  ],
  exports: uiComponents
})
export class UiModule {
  constructor() {
    library.add(faUsers, faUserMd, faCogs);
  }
}
