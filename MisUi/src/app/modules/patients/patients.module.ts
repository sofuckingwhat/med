import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

// Icons
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faCalendarPlus,
  faCommentDots,
  faPlusCircle,
  faTrashAlt
} from '@fortawesome/free-solid-svg-icons';

import { PatientsRoutingModule } from './patients.routing';
import { MainPatientsComponent } from './main-patients.component';

// Components
import {
  PatientsListComponent,
  PatientDetailsComponent
} from './components';

// Modal windows
import { NewPatientComponent } from './components/modals';

// Modules
import { SharedModule } from '../shared-module';

@NgModule({
  declarations: [
    MainPatientsComponent,
    PatientsListComponent,
    NewPatientComponent,
    PatientDetailsComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    PatientsRoutingModule,
    SharedModule
  ],
  entryComponents: [
    NewPatientComponent
  ]
})
export class PatientsModule {
  constructor() {
    library.add(
      faCalendarPlus,
      faCommentDots,
      faPlusCircle,
      faTrashAlt
      );
  }
}
