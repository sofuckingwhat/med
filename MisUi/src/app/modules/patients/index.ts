export * from './components';

export { MainPatientsComponent } from './main-patients.component';
export { PatientsModule } from './patients.module';
export { PatientsRoutingModule } from './patients.routing';
