import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainPatientsComponent } from './main-patients.component';

import * as components from './components';
const routes: Routes = [
  {
    path: '',
    component: MainPatientsComponent,
    children: [
      {path: '', redirectTo: 'index', pathMatch: 'full'},
      {path: 'index', component: components.PatientsListComponent},
      {path: 'detail/:id', component: components.PatientDetailsComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientsRoutingModule { }
