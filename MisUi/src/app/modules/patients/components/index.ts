export { PatientsListComponent } from './patients-list/patients-list.component';
export { PatientDetailsComponent } from './patient-details/patient-details.component';
export * from './modals';
