import { Component, OnInit } from '@angular/core';

// Interfaces
import {
  IModalWindowParams,
  IResponseList
} from 'src/app/core';

// Models
import {
  PatientListModel,
} from 'src/app/core';

// Services
import {
  ModalWindowService,
  PatientsService,
} from 'src/app/core';


import { NewPatientComponent } from '../modals';

@Component({
  selector: 'mis-patients-list',
  templateUrl: './patients-list.component.html',
  styleUrls: ['./patients-list.component.scss']
})
export class PatientsListComponent implements OnInit {

  public patients: PatientListModel[] = [];

  public loading: boolean;

  private page: number;

  public pages: number;

  constructor(
    private readonly modalService: ModalWindowService,
    private readonly api: PatientsService
  ) { }

  public createNewPatient(): void {
    this.modalService.open(NewPatientComponent)
      .subscribe((event: IModalWindowParams) => {
        if (event.response) {
          this.patients.unshift(event.response);
        }
      });
  }

  private getPatients(): void {
    this.api.getPatients()
      .subscribe((items: IResponseList<PatientListModel>) => {
        this.patients = items.items;
        this.page = items.page;
        this.pages = items.totalPages;
      });
  }

  public trackPatients(index: number, item: PatientListModel): string {

    return item.id;
  }

  public ngOnInit(): void {
    this.getPatients();
  }

}
