import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';

// Interfaces
import { IModalComponent, IBackValidationError } from 'src/app/core';

// Models
import {
  DictionaryItemModel,
  DictionaryDetailModel,
  PatientListModel
} from 'src/app/core';

// Services
import {
  DictionaryService,
  ModalWindowService,
  AuthService,
  PatientsService,
  FormsHelperService
} from 'src/app/core';

// Rxjs
import { flatMap } from 'rxjs/operators';
import { FormBaseComponent } from 'src/app/modules/shared-module/components/forms-base.component';

/** Модальное окно добавления пациента клиники */
@Component({
  selector: 'mis-new-patient',
  templateUrl: './new-patient.component.html',
  styleUrls: ['./new-patient.component.scss'],
  providers: [ReactiveFormsModule]
})
export class NewPatientComponent extends FormBaseComponent implements IModalComponent, OnInit {

  /** Источник информации о клинике */
  public sourceOfInformation: DictionaryItemModel[] = [];

  /** Форма пациента */
  public form: FormGroup;

  /** Ошибки заполнения формы */
  public backErrors: IBackValidationError;

  public user: any;

   /** Данные модального окна */
  @Input() public data: object | null = null;

  constructor(
    private readonly modalWindowService: ModalWindowService,
    private readonly dictionaryService: DictionaryService,
    private readonly fb: FormBuilder,
    private readonly auth: AuthService,
    private readonly api: PatientsService,
    private readonly formsHelper: FormsHelperService
  ) {
    super(formsHelper, fb);
  }

  // TODO!: Переделать все методы с использованием этого свойства
  public get inProgress(): boolean {
    return true;
  }

  public close(): void {
    this.modalWindowService.close(false);
  }

  public onSubmit(): void {
    if (this.form.valid) {
      this.api.createPatient(this.form.value)
      .subscribe((success: PatientListModel) => {

        this.modalWindowService.close(success);
      },
      (err: HttpErrorResponse) => {

        this.backErrors = this.formsHelper.backSideValidationErrors(err);

      });
    }
    this.showErrors = true;
  }

  public createForm(): void {

    this.form = this.fb.group({
      id: [],
      fio: ['', Validators.required],
      birthday: ['', Validators.required],
      gender: ['', Validators.required],
      phones: ['', Validators.required],
      sourceOfInformation: ['', Validators.required],
      comment: []
    });
  }

  public ngOnInit(): void {
    this.dictionaryService.getDictionaryByName('istochniki-informacii')
      .pipe(
        flatMap((item: DictionaryDetailModel) => {
          this.sourceOfInformation = item.items;

          this.createForm();
          return this.auth.currentUser$;
        })
      )
      .subscribe((user: any) => {
        this.formReady = true;
        this.user = user;
      });
  }
}
