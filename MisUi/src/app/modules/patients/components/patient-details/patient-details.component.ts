import { Component, OnInit } from '@angular/core';
import { FormBaseComponent } from 'src/app/modules/shared-module/components/forms-base.component';
import { DatePipe } from '@angular/common';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';

import {
  FormsHelperService,
  PatientsService,
  PatientDetailDto,
  PassportDto,
  AddressDto,
  PolisDto,
  DictionaryService,
  Dictionary,
  DictionaryDetailModel,
  DictionaryItemModel,
  KeyValuePair
} from 'src/app/core';

import { Observable, Observer } from 'rxjs';
import { flatMap, delay } from 'rxjs/operators';

@Component({
  selector: 'mis-patient-details',
  templateUrl: './patient-details.component.html',
  styleUrls: ['./patient-details.component.scss'],
  providers: [DatePipe]
})
export class PatientDetailsComponent extends FormBaseComponent implements OnInit {

  /** Данные пациента */
  public form: FormGroup;

  public patient: PatientDetailDto;

  public insuranceCompanies = new Dictionary<string, string>();

  public addressesTypes = new Dictionary<string, string>();

  constructor(
    private readonly api: PatientsService,
    private readonly dicApi: DictionaryService,
    private readonly fh: FormsHelperService,
    private readonly activatedRout: ActivatedRoute,
    private readonly fb: FormBuilder,
    private readonly dt: DatePipe
  ) {
    super(fh, fb);
  }

  public get inProgress(): boolean {
    return true;
  }

  public onSubmit(): void {

    if (this.form.invalid) {
      this.showErrors = true;
      return;
    }
    this.formReady = false;
    this.api.updatePatient(this.form.value)
      .pipe(
        flatMap((patient: PatientDetailDto) => {

          return this.createForm(patient);
        })
      )
      .subscribe((form: FormGroup) => {
        this.form = form;
        this.formReady = true;
      });
  }

  /** Массив адресов пациента */
  public get addressArray(): FormArray {

    return this.form.get('addresses') as FormArray;
  }

  /** Добавить адрес в список */
  public addAddress(): void {

    this.addressArray.push(this.createAddress(new AddressDto()));
  }

  /** Массив полисов пациента */
  public get polices(): FormArray {
    return this.form.get('polises') as FormArray;
  }

  /** Добавить полис в массив */
  public addPolis(): void {
    this.polices.push(this.createPolis(new PolisDto()));
  }

  /** Удалить полис */
  public removePolis(index: number): void {

    const polisLine = this.polices.get(index.toString()) as FormControl;

    if (polisLine.get('id').value) {
      return;
    }

    this.polices.removeAt(index);
  }

    /** Удалить полис */
  public removeAddress(index: number): void {

    const addressLine = this.addressArray.get(index.toString()) as FormControl;

    if (addressLine.get('id').value) {
      return;
    }

    this.addressArray.removeAt(index);
  }

  /** Трэкинг массива адресов */
  public trackAddress(index: number, item: AddressDto): string {
    return item.id;
  }

  /** Трэкинг массива полисов */
  public trackPolis(index: number, item: PolisDto): string {
    return item.id;
  }

  /**
   * Получить группу по номеру
   * @param i Номер группы полисов
   */
  public getPolisFormGroup(i: string): FormGroup {
    return this.polices.controls[i] as FormGroup;
  }

  /**
   * Получить адреса группу по номеру
   * @param i Номер группы адресов
   */
  public getAddressFormGroup(i: string): FormGroup {
    return this.addressArray.controls[i] as FormGroup;
  }

  /** Получить данные пациента */
  private getPatient(): Observable<PatientDetailDto> {

    return this.activatedRout.paramMap
      .pipe(
        flatMap((params: ParamMap) => {

          return this.api.getPatientDetail(params.get('id'));
        })
      );
  }

  /** Подготовить форму */
  private createForm(patient: PatientDetailDto): Observable<FormGroup> {

    return new Observable<FormGroup>((observer: Observer<FormGroup>): any => {
      const model = patient;
      if (!model.passport) {
        model.passport = new PassportDto();
      }
      try {
        const form =  this.fb.group({
          id: [model.id],
          fio: [model.fio],
          age: [{value: model.age, disabled: true}],
          gender: [model.gender],
          birthday: [model.birthday],
          ambulanceCard: [model.ambulanceCard],
          comment: [model.comments],
          phones: [model.phones],
          email: [model.email],
          inn: [model.inn],
          snils: [model.snils],
          passport: this.fb.group({
            id: [model.passport.id],
            series: [model.passport.series],
            number: [model.passport.number],
            issuedDate: [model.passport.issuedDate],
            issuerDepartment: [model.passport.issuerDepartment],
            departmentCode: [model.passport.departmentCode],
          }),
          addresses: this.fb.array(
            model.address.map((a: AddressDto) => this.createAddress(a))
          ),
          polises: this.fb.array(
            model.polises.map((a: PolisDto) => this.createPolis(a))
          )
        });

        observer.next(form);
        observer.complete();

      } catch (error) {
        observer.error(error);
        observer.complete();
      }
    });
  }

  /**
   * Конвертирует данные адреса в форму
   * @param address DTO адреса с сервера
   */
  private createAddress(address: AddressDto): FormGroup {

    const addressDto = address ? address : new AddressDto();

    return this.fb.group({
      id: [addressDto.id],
      address: [addressDto.address],
      region: [addressDto.region],
      city: [addressDto.city],
      street: [addressDto.street],
      building: [addressDto.building],
      room: [addressDto.room],
      addressTypeId: [addressDto.addressTypeId],
    });
  }

  /**
   * Конвертирует данные полиса в форму
   * @param address DTO полиса с сервера
   */
  private createPolis(polis: PolisDto): FormGroup {

    const model = polis ? polis : new PolisDto();

    return this.fb.group({
      id: [model.id],
      number: [model.number],
      issuedDate: [model.issuedDate],
      issuerId: [model.issuerId],
    });
  }

  public ngOnInit(): void {
    this.formReady = false;
    this.getPatient()
      .pipe(
        flatMap((patient: PatientDetailDto) => {
          this.patient = patient;

          return this.createForm(patient);
        }),
        flatMap((form: FormGroup) => {

          this.form = form;

          return this.dicApi.getDictionaryByName('straxovye-kompanii');
        }),
        flatMap((model: DictionaryDetailModel) => {

          model.items.forEach((i: DictionaryItemModel) => {
            this.insuranceCompanies.setItem(new KeyValuePair(i.id, i.shortName));
          });

          return this.dicApi.getDictionaryByName('tipy-pochtovyx-adresov');
        })
        ).subscribe(
          (model: DictionaryDetailModel) => {

            model.items.forEach((i: DictionaryItemModel) => {
              this.addressesTypes.setItem(new KeyValuePair(i.id, i.shortName));
            });

            this.formReady = true;
        },
        (err: any) => {
          console.log(err);
        });
  }

}
