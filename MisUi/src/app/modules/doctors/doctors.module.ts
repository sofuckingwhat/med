import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

// Icons
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faPlusCircle,
  faTrashAlt,
  faArrowLeft,
  faCopy
} from '@fortawesome/free-solid-svg-icons';

import { DoctorsRoutingModule } from './doctors.routing';

import { SharedModule, AddPeriodModalComponent } from '../shared-module';

import { DoctorsMainComponent } from './doctors-main.component';

import * as components from './components';

@NgModule({
  declarations: [
    DoctorsMainComponent,
    components.DoctorsListComponent,
    components.AddDoctorComponent,
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    DoctorsRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ],
  entryComponents: [
    AddPeriodModalComponent
  ]
})
export class DoctorsModule {
  constructor() {
    library.add(
      faPlusCircle,
      faTrashAlt,
      faArrowLeft,
      faCopy
      );
  }
}
