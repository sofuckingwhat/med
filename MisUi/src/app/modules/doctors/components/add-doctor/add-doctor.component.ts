import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';

// Rxjs
import { Subscription, Observable, Observer, of } from 'rxjs';
import { FormBaseComponent } from 'src/app/modules/shared-module/components/forms-base.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormsHelperService, YearModel, MonthModel, DayOfMonthModel } from 'src/app/core';
import { flatMap, delay } from 'rxjs/operators';
import { DoctorDetailDto } from 'src/app/core/models/Doctors';
import { DoctorsApiService } from 'src/app/core/services/doctors';

@Component({
  selector: 'mis-add-doctor',
  templateUrl: './add-doctor.component.html',
  styleUrls: ['./add-doctor.component.scss']
})
export class AddDoctorComponent extends FormBaseComponent implements OnInit {

  /** Модель доктора */
  public doctor: DoctorDetailDto = null;

  /** Календарь рабочих дней */
  public scheduler: YearModel;

  /** Форма доктора */
  public form: FormGroup;

  /** Подписки на сервисы */
  private subs: Subscription[] = [];

  constructor(
    private readonly ar: ActivatedRoute,
    private readonly fb: FormBuilder,
    private readonly fh: FormsHelperService,
    private readonly api: DoctorsApiService
  ) {
    super(fh, fb);
  }

  /** Весь необходимый контент загружен */
  public get inProgress(): boolean {

    return !this.formReady ||  this.doctor === null;
  }

  public get showCalendar(): boolean {

    return this.doctor.id !== null;
  }
  /** Показать год */
  public get showYear(): boolean {

    return !this.scheduler.selectedMonth;
  }

  /** Показывать сетку дней */
  public get showMonth(): boolean {

    return this.scheduler.selectedMonth && this.scheduler.selectedMonth.selectedDay === null;
  }

  /** Показать рабочую неделю */
  public get showWeek(): boolean {

    return this.scheduler.selectedMonth && this.scheduler.selectedMonth.selectedDay !== null;
  }

  /** Строит форму для редактирования рабочего времени докторов */
  private initForm(): Observable<FormGroup> {

    return new Observable((observer: Observer<FormGroup>) => {

      try {

        const form = this.fb.group({
          id: [this.doctor.id],
          fio: [this.doctor.fio, Validators.required]
        });

        observer.next(form);
        observer.complete();
      } catch (error) {
        observer.error(error);
      }

    });
  }

  /**
   * Установить месяц для просмотра
   * @param month Выбранный месяц года
   */
  public setMonth(month: MonthModel): void {
    this.scheduler.selectedMonth = month;
  }

  /**
   * Сделать день выбранным
   * @param day День месяца
   */
  public setDay(day: DayOfMonthModel): void {
    if (this.scheduler.selectedMonth) {
      this.scheduler.selectedMonth.selectedDay = day;
    }
  }

  /** Очистить выбранный день */
  public removeDay(): void {
    if (this.scheduler.selectedMonth) {
      this.scheduler.selectedMonth.selectedDay = null;
    }
  }

  /** Сбросить месяц и вернуться к просмотру года */
  public removeMonth(): void {
    if (this.scheduler.selectedMonth) {
      this.scheduler.selectedMonth.selectedDay = null;
    }
    this.scheduler.selectedMonth = null;
  }

  /** Сохранить данные формы */
  public onSubmit(): void {

    if (this.form.invalid) {
      return;
    }

    const request = this.doctor.id
      ? this.api.updateDoctor(this.form.value)
      : this.api.createDoctor(this.form.value);

    this.formReady = false;

    request
      .pipe(
        flatMap((model: DoctorDetailDto) => {
          this.doctor = model;

          return this.initForm();
        }),
        delay(1000)
      )
      .subscribe((form: FormGroup) => {
        this.form = form;
        this.formReady = true;
      });
  }

  public ngOnInit(): void {
    this.formReady = false;

    this.scheduler = new YearModel(2019);
    // Временно заполняем текущий месяц и день.
    this.scheduler.selectedMonth = this.scheduler.months.find((m: MonthModel) => m.monthNumber === new Date().getMonth());
    this.scheduler.selectedMonth.selectedDay =
      this.scheduler.selectedMonth.days.find((d: DayOfMonthModel) => d.day === new Date().getDate());
    // ----------------------------------------

    this.ar.paramMap
      .pipe(
        delay(1000),
        flatMap((par: ParamMap) => {
          const id = par.get('id');

          if (id) {
            return this.api.getDoctor(id);
          }
          const doctor = new DoctorDetailDto();
          doctor.id = id;

          return of(doctor);
        }),
        flatMap((doctor: DoctorDetailDto) => {
          this.doctor = doctor;

          return this.initForm();
        })
      ).subscribe((form: FormGroup) => {
        this.form = form;
        this.formReady = true;
      });
  }

}
