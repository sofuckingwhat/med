import { Component, OnInit } from '@angular/core';
import { DoctorsApiService, DoctorListItemDto, RequestListFilters, IResponseList } from 'src/app/core';

@Component({
  selector: 'mis-doctors-list',
  templateUrl: './doctors-list.component.html',
  styleUrls: ['./doctors-list.component.scss']
})
export class DoctorsListComponent implements OnInit {

  private _searchString: string;

  private page = 1;

  private perPage = 10;

  public doctorsList: DoctorListItemDto[] = [];

  constructor(
    private readonly api: DoctorsApiService
  ) { }

  public get search(): string {

    return this._searchString;
  }

  public set search(val: string) {

    this._searchString = val;
  }

  /**
   * Вспомогательный метод для таблицы
   * @param index Индекс
   * @param item Данные доктора
   */
  public doctorsTrack(index: number, item: DoctorListItemDto): string {

    return item.id;
  }

  public ngOnInit(): void {
    const filters = new RequestListFilters();
    filters.page = this.page;
    filters.perPage = this.perPage;
    filters.queryString = this._searchString;

    this.api.getList(filters)
      .subscribe((response: IResponseList<DoctorListItemDto>) => {

        this.doctorsList = response.items;
      });
  }

}
