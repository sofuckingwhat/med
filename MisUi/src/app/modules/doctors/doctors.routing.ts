import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DoctorsMainComponent } from './doctors-main.component';

import * as components from './components';

const routes: Routes = [
  {
    path: '',
    component: DoctorsMainComponent,
    children: [
      {path: '', redirectTo: 'index', pathMatch: 'full'},
      {path: 'index', component: components.DoctorsListComponent},
      {path: 'setup/:id', component: components.AddDoctorComponent},
      {path: 'setup', component: components.AddDoctorComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DoctorsRoutingModule { }
