export { DoctorsModule } from './doctors.module';
export { DoctorsRoutingModule } from './doctors.routing';
export { DoctorsMainComponent } from './doctors-main.component';

export * from './components';
