export { AuthModuleModule } from './auth-module/auth-module.module';
export { MainModule } from './main-module/main.module';
export { SharedModule } from './shared-module/shared-module.module';


export * from './patients';
export * from './registry-module';
export * from './ui-module';
