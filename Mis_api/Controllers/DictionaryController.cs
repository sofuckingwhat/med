﻿using System;
using System.Threading.Tasks;
using Domain.CQRS.Commands.Dictionaries;
using Domain.ViewModels.Dictionaries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Mis.Application.CQRS.Dictionaries.Queries;

namespace Mis_api.Controllers
{
    /// <summary>
    /// Контроллер работы со словарями
    /// </summary>
    [AllowAnonymous]
    public class DictionaryController : BaseController
    {
        #region Dictionary

        [HttpGet] // api/dictionary
        public async Task<ActionResult<DictionariesListViewModel>> Get()
        {
            return Ok(await Mediator.Send(new DictionariesListQuery()));
        }

        [HttpGet("{id}")] // api/dictionary/59bc26a1-c057-416b-98f2-4e37abe0a79e
        public async Task<ActionResult<DictionaryDetailViewModel>> Get(Guid id)
        {
            return Ok(await Mediator.Send(new DictionaryDetailQuery { Id = id }));
        }

        /// <summary>
        /// Получить справочника по его наименованию
        /// </summary>
        /// <param name="name">Наименование справочника</param>
        /// <returns></returns>
        [HttpGet("by-name/{name}")] // api/dictionary/by-name/name
        public async Task<ActionResult<DictionaryDetailViewModel>> GetByName(string name)
        {
            return Ok(await Mediator.Send(new DictionaryByNameQuery() { Name = name }));
        }

        [HttpPost] // api/dictionary
        public async Task<ActionResult<DictionaryViewModel>> Post([FromBody] CreateDictionaryCommand request)
        {
            return Ok(await Mediator.Send(request));
        }

        [HttpPut] // api/dictionary
        public async Task<ActionResult<DictionaryViewModel>> Put([FromBody] UpdateDictionaryCommand request)
        {
            return Ok(await Mediator.Send(request));
        }

        [HttpDelete("{id}")] // api/dictionary/59bc26a1-c057-416b-98f2-4e37abe0a79e
        public async Task<ActionResult<DictionaryViewModel>> Delete(Guid id)
        {
            return Ok(await Mediator.Send(new DeleteDictionaryCommand { Id = id }));
        }

        #endregion

        #region DictionatyItems

        [HttpPost("item")] // api/dictionary/item
        public async Task<ActionResult<DictionaryItemViewModel>> AddItem([FromBody] CreateDictionaryItemCommand request)
        {
            return Ok(await Mediator.Send(request));
        }

        [HttpPut("item")] // api/dictionary/item
        public async Task<ActionResult<DictionaryItemViewModel>> UpdateItem(
            [FromBody] UpdateDictionaryItemCommand request)
        {
            return Ok(await Mediator.Send(request));
        }

        [HttpDelete("item/{id}")] // api/dictionary/item/{guid}
        public async Task<ActionResult<DictionaryItemViewModel>> DeleteItem(Guid id)
        {
            return Ok(await Mediator.Send(new DeleteDictionaryItemCommand { Id = id }));
        }

        #endregion
    }
}