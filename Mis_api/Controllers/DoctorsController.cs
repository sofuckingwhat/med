﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.CQRS.Commands.Doctors;
using Domain.CQRS.Queries.Doctors;
using Domain.ViewModels.Doctors;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Mis_api.Controllers
{
    [AllowAnonymous]
    public class DoctorsController : BaseController
    {
        [HttpGet] // api/doctors
        [ProducesResponseType(typeof(IEnumerable<DoctorListItemDto>), 200)]
        public async Task<IActionResult> Get([FromQuery] DoctorsListQuery request)
        {

            return Ok(await Mediator.Send(request));
        }

        [HttpGet("{id}")] // api/doctors/{guid}
        [ProducesResponseType(typeof(DoctorListItemDto), 200)]
        public async Task<IActionResult> Get(Guid id)
        {

            return Ok(await Mediator.Send(new DoctorDetailQuery() { Id = id }));
        }

        [HttpPost] // api/doctors
        [ProducesResponseType(typeof(DoctorDetailDto), 200)]
        public async Task<IActionResult> Post(CreateDoctorCommand request)
        {
            return Ok(await Mediator.Send(request));
        }

        [HttpPut]
        [ProducesResponseType(typeof(DoctorDetailDto), 200)]
        public async Task<IActionResult> Put(UpdateDoctorCommand request)
        {

            return Ok(await Mediator.Send(request));
        }
    }
}