﻿using System;
using System.Threading.Tasks;
using Domain.CQRS.Commands.TimePeriods;
using Domain.CQRS.Queries.Periods;
using Domain.ViewModels.TimePeriod;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Mis_api.Controllers
{
    [AllowAnonymous]
    public class TimePeriodController : BaseController
    {
        [HttpGet("{id}/{from}/{to}")] // api/timeperiod/{guid}/{date}/{date}
        [ProducesResponseType(typeof(WorkGraphicDto), 200)]
        public async Task<IActionResult> Get([FromRoute] WeekPeriodQuery request)
        {
            return Ok();
        }

        [HttpPost] // api/timeperiod
        [ProducesResponseType(typeof(WorkGraphicDto), 200)]
        public async Task<IActionResult> Post([FromBody] AddPeriodCommand request)
        {
            return Ok(await Mediator.Send(request));
        }
    }
}