﻿using System;
using System.Threading.Tasks;
using Domain.Abstract;
using Domain.CQRS.Commands.Patients;
using Domain.CQRS.Queries.Patients;
using Domain.ViewModels.Patients;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Mis_api.Controllers
{
    [AllowAnonymous]
    public class PatientController : BaseController
    {
        #region Patients Core

        [HttpGet] // api/patient
        [ProducesResponseType(typeof(IResponseList<PatientItemViewModel>), 200)]
        public async Task<IActionResult> Get([FromQuery] PatientsListQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpGet("{id}")] // api/patient/{guid}
        [ProducesResponseType(typeof(PatientDetailViewModel), 200)]
        public async Task<IActionResult> Get(Guid id)
        {
            return Ok(await Mediator.Send(new PatientDetailQuery { Id = id }));
        }

        /// <summary>
        /// Создание пациента с минимальным набором данных
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost] // api/patient
        public async Task<ActionResult<PatientItemViewModel>> Post([FromBody] CreatePatientCommand request)
        {
            return Ok(await Mediator.Send(request));
        }

        /// <summary>
        /// Обновить данные пациента
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(typeof(PatientDetailViewModel), 200)]
        public async Task<IActionResult> Put([FromBody] UpdatePatientCommand request)
        {
            return Ok(await Mediator.Send<PatientDetailViewModel>(request));
        }

        #endregion

        #region Parients Fetures



        #endregion
    }
}