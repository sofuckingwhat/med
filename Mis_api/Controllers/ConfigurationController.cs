﻿using Domain.Constants;
using identity_infrastructure.Configurations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Mis_api.Controllers
{
    [AllowAnonymous]
    [Route("api/config")]
    [ApiController]
    public class ConfigurationController : ControllerBase
    {
        #region Properties

        private readonly StsServerConfiguration _config;

        #endregion

        #region Constructors

        public ConfigurationController(StsServerConfiguration config)
        {
            _config = config;
        }

        #endregion

        [HttpGet]
        [EnableCors(OxyPoliciesNames.GetStsConfig)]
        public IActionResult Get()
        {

            return Ok(_config);
        }

    }
}