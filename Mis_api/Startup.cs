﻿using System.IdentityModel.Tokens.Jwt;
using System.Reflection;
using Data_Access;
using Data_Access.Identity;
using Data_Access.MongoDb;
using Domain.Configurations;
using Domain.Constants;
using FluentValidation.AspNetCore;
using identity_infrastructure.Configurations;
using identity_infrastructure.Resources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Serilog;
using MediatR;
using MediatR.Pipeline;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Mis.Application.CQRS.Dictionaries.Queries.Handlers;
using Mis.Application.Exceptions;
using Mis.Application.Infrastructure;
using Mis.Application.Interfaces.Doctors;
using Mis.Application.Interfaces.Periods;
using Mis.Application.Repositories.Doctors;
using Mis.Application.Repositories.Periods;

namespace Mis_api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            #region Локальные переменные

            var migrationAssembly = typeof(OxyDataContext).GetTypeInfo().Assembly.GetName().Name;
            // База данных пользователей.
            var connection = Configuration["ConnectionStrings:IdentityDataBase"];

            var authEndPoint = Configuration["Endpoints:Authorize"];
            Log.Logger.Information(authEndPoint);
            #endregion

            #region Policies

            services.AddCors(options =>
            {
                options.AddPolicy(OxyPoliciesNames.Local, policy =>
                {
                    policy.WithOrigins("http://localhost:5001")
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
                options.AddPolicy(OxyPoliciesNames.Production, policy =>
                {
                    policy.WithOrigins("http://10.20.10.26:8080")
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
                options.AddPolicy(OxyPoliciesNames.GetStsConfig, builder => { builder.WithOrigins(new[] { "http://localhost:5001", "http://10.20.10.26:8080" }); });
            });

            var guestPolicy = new AuthorizationPolicyBuilder()
                .RequireAuthenticatedUser()
                .RequireClaim(OxyClaims.Company.Type, new[] { "oxy" })
                .Build();

            #endregion

            #region Блок баз данных

            // Подключаем контекст базы данных
            services.AddDbContext<OxyDataContext>(options =>
                {
                    options.UseNpgsql(Configuration["ConnectionStrings:OxyPostgresDatabase"], mig => mig.MigrationsAssembly("Data_Access"));
                });

            services.AddDbContext<OxyIdentityDbContext>(builder =>
                // dotnet ef migrations add InitialIdentityServerMigration - c OxyIdentityDbContext
                builder.UseNpgsql(connection, optionsBuilder => optionsBuilder.MigrationsAssembly(migrationAssembly))
            );

            #endregion

            #region Настройки IOptions и тд.
            // Настройки для MongoDb
            services.Configure<MongoSettings>(options =>
                {
                    options.ConnectionString = Configuration["MongoDbConfiguration:Connection"];
                    options.PatientsDb = Configuration["MongoDbConfiguration:DataBases:Persons"];
                    options.Donors = Configuration["MongoDbConfiguration:DataBases:Donors"];
                    options.WorkTimeDb = Configuration["MongoDbConfiguration:DataBases:WorkTimes"];
                });

            // Конфигурация OIDC для фронта
            services.AddSingleton(Configuration.GetSection("ClientAppSettings").Get<StsServerConfiguration>());

            #endregion

            #region MediatR

            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestPreProcessorBehavior<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestPerformanceBehavior<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehavior<,>));
            // Подключаем сборку для медиатора
            services.AddMediatR(typeof(ValidationException).GetTypeInfo().Assembly);

            #endregion

            #region DI

            // Подключаем логгер
            services.AddSingleton(x => Log.Logger);

            // Подключаем контекст базы данных MongoDb
            services.AddTransient<MongoContextBase>();

            // Репозиторий для работы с докторами
            services.AddTransient<IDoctorsRepository, DoctorsRepository>();

            // Репозиторий для работы с периодами времени
            services.AddTransient<IPeriodsRepository, TimePeriodsRepository>();

            #endregion

            #region Настройки приложения

            JwtSecurityTokenHandler.DefaultOutboundClaimTypeMap.Clear();
            services
                .AddMvc(options =>
                {
                    options.Filters.Add(new AuthorizeFilter(guestPolicy));
                })
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Local;
                    options.SerializerSettings.Formatting = Formatting.Indented;
                    options.SerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;

                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                // Подключаем все валидаторы, которые содержит сборка Application
                .AddFluentValidation(configuration => configuration.RegisterValidatorsFromAssemblyContaining<QueryDictionaryListHandler>());

            services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = authEndPoint;
                    options.ApiName = OxyConstantsNames.MisApi;
                    options.ApiSecret = OxySecrets.ApiSecret;
                    options.NameClaimType = "email";
                    options.RoleClaimType = OxyConstantsNames.Permission;
                });

            #endregion
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseCors(OxyPoliciesNames.Local);
            }
            else
            {
                app.UseForwardedHeaders(new ForwardedHeadersOptions
                {
                    ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
                });
                app.UseCors(OxyPoliciesNames.Production);

                app.UseHttpsRedirection();
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                // Форсируем использование HTTPS на продакшене
                // Использовать эту настройку, как только появится сертификат SSL
                app.UseHsts();
            }

            app.UseCors(OxyPoliciesNames.GetStsConfig);
            app.UseAuthentication();

            app.UseMvc();
        }
    }
}
