﻿namespace Mis_api.Helpers
{
    /// <summary>
    /// Вспомогательный класс, для работы с телефонными номерами
    /// </summary>
    public static class PhoneHelper
    {
        /// <summary>
        /// Извлекает из строки телефонные номера и форматирует их в список.
        /// Результат: ["+7(905)-444-55-66","205-25-25"]
        /// </summary>
        /// <param name="phonesString">Телефоны в виде строки 79054445566;2052525</param>
        /// <returns></returns>
        public static string[] GetPhonesToStrings(string phonesString)
        {

            return new string[] { };
        }
    }
}