﻿using System;
using System.IO;
using Data_Access;
using Data_Access.MongoDb;
using Domain.Helpers.IIS;
using Domain.ViewModels.Patients;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using Serilog;
using Serilog.Events;
using ILogger = Serilog.ILogger;

namespace Mis_api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // Регистрируем глобальный логгер
            Log.Logger = new LoggerConfiguration()
                .WriteTo.ColoredConsole(LogEventLevel.Debug)
                .WriteTo.File($"{Directory.GetCurrentDirectory()}/Logs/AppLog-{DateTime.Today:d}.txt", LogEventLevel.Error)
                .CreateLogger();

            var logger = Log.Logger;

            try
            {
                var config = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", false)
                    .Build();

                logger.Information("Получил конфигурацию системы");
                var host = CreateWebHostBuilder(config, args).Build();

                using (var scope = host.Services.CreateScope())
                {
                    InitPostgres(scope, logger);
                    InitMongoDb(scope);
                }

                host.Run();
            }
            catch (Exception e)
            {
                logger.Error(e, "Ошибка старта приложения.");
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(IConfiguration conf, string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseKestrel()
                .UseUrls(conf["Hosts:http"], conf["Hosts:https"])
                .UseContentRoot(IISHelper.GetContentRoot() ?? Directory.GetCurrentDirectory())
                .UseSerilog()
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    var env = hostingContext.HostingEnvironment;
                    config.SetBasePath(env.ContentRootPath);
                    config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                        .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true);
                    config.AddEnvironmentVariables();

                    if (args != null)
                    {
                        config.AddCommandLine(args);
                    }

                })
                .UseStartup<Startup>();

        /// <summary>
        /// Наполняем данными Mongo
        /// </summary>
        /// <param name="scope"></param>
        private static void InitMongoDb(IServiceScope scope)
        {
            try
            {
                var patients = scope.ServiceProvider.GetService<MongoContextBase>();
                var mongoInit = new MongoInitializer(patients);
                mongoInit.Init();
            }
            catch (Exception e)
            {
                var logger = scope.ServiceProvider.GetRequiredService<ILogger<Program>>();
                logger.LogError(e, "Возникло исключение при инициализации MongoDb");
            }
        }

        /// <summary>
        /// Наполняем данными Postgres
        /// </summary>
        /// <param name="scope"></param>
        private static void InitPostgres(IServiceScope scope, ILogger log)
        {
            try
            {
                var context = scope.ServiceProvider.GetService<OxyDataContext>();
                context.Database.Migrate();
                OxyDataInitializer.Initialize(context, log);
            }
            catch (Exception e)
            {
                var logger = scope.ServiceProvider.GetRequiredService<ILogger<Program>>();
                logger.LogError(e, "Возникло исключение при миграции или наполнении базы данных");
            }
        }
    }
}
