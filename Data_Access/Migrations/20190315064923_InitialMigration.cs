﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Data_Access.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Dictionaries",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    AuthorId = table.Column<Guid>(nullable: false),
                    AuthorName = table.Column<string>(nullable: true),
                    UpdatedBuId = table.Column<Guid>(nullable: false),
                    UpdatedByName = table.Column<string>(nullable: true),
                    SortOrder = table.Column<int>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Code804H = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dictionaries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Passports",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    AuthorId = table.Column<Guid>(nullable: false),
                    AuthorName = table.Column<string>(nullable: true),
                    UpdatedBuId = table.Column<Guid>(nullable: false),
                    UpdatedByName = table.Column<string>(nullable: true),
                    SortOrder = table.Column<int>(nullable: false),
                    Series = table.Column<string>(nullable: true),
                    Number = table.Column<string>(nullable: true),
                    IssuedDate = table.Column<DateTime>(nullable: false),
                    DepartmentCode = table.Column<string>(nullable: true),
                    DepartmentName = table.Column<string>(nullable: true),
                    PersonId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Passports", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PersonalFiles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    AuthorId = table.Column<Guid>(nullable: false),
                    AuthorName = table.Column<string>(nullable: true),
                    UpdatedBuId = table.Column<Guid>(nullable: false),
                    UpdatedByName = table.Column<string>(nullable: true),
                    SortOrder = table.Column<int>(nullable: false),
                    FileName = table.Column<string>(nullable: true),
                    FilePath = table.Column<string>(nullable: true),
                    FileSize = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersonalFiles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DictionaryItems",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    AuthorId = table.Column<Guid>(nullable: false),
                    AuthorName = table.Column<string>(nullable: true),
                    UpdatedBuId = table.Column<Guid>(nullable: false),
                    UpdatedByName = table.Column<string>(nullable: true),
                    SortOrder = table.Column<int>(nullable: false),
                    DictionaryId = table.Column<Guid>(nullable: false),
                    DictionaryFullValue = table.Column<string>(nullable: true),
                    DictionaryShortValue = table.Column<string>(nullable: true),
                    Code804 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DictionaryItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DictionaryItems_Dictionaries_DictionaryId",
                        column: x => x.DictionaryId,
                        principalTable: "Dictionaries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Persons",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    AuthorId = table.Column<Guid>(nullable: false),
                    AuthorName = table.Column<string>(nullable: true),
                    UpdatedBuId = table.Column<Guid>(nullable: false),
                    UpdatedByName = table.Column<string>(nullable: true),
                    SortOrder = table.Column<int>(nullable: false),
                    Fio = table.Column<string>(nullable: true),
                    Surname = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    MiddleName = table.Column<string>(nullable: true),
                    BirthDay = table.Column<DateTime>(nullable: false),
                    Gender = table.Column<int>(nullable: false),
                    PassportId = table.Column<Guid>(nullable: true),
                    Inn = table.Column<string>(nullable: true),
                    Snils = table.Column<string>(nullable: true),
                    Discriminator = table.Column<string>(nullable: false),
                    IsConfidential = table.Column<bool>(nullable: true),
                    DonorCode = table.Column<string>(nullable: true),
                    PhotosId = table.Column<Guid>(nullable: true),
                    ClinicUID = table.Column<string>(nullable: true),
                    Phones = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    PartnerId = table.Column<Guid>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    SourceOfInformationGuid = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Persons", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Persons_PersonalFiles_PhotosId",
                        column: x => x.PhotosId,
                        principalTable: "PersonalFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Persons_Persons_PartnerId",
                        column: x => x.PartnerId,
                        principalTable: "Persons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Persons_DictionaryItems_SourceOfInformationGuid",
                        column: x => x.SourceOfInformationGuid,
                        principalTable: "DictionaryItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Persons_Passports_PassportId",
                        column: x => x.PassportId,
                        principalTable: "Passports",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ReceptionTypes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    AuthorId = table.Column<Guid>(nullable: false),
                    AuthorName = table.Column<string>(nullable: true),
                    UpdatedBuId = table.Column<Guid>(nullable: false),
                    UpdatedByName = table.Column<string>(nullable: true),
                    SortOrder = table.Column<int>(nullable: false),
                    ReceptionTypeNameId = table.Column<Guid>(nullable: true),
                    Patient = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReceptionTypes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ReceptionTypes_DictionaryItems_ReceptionTypeNameId",
                        column: x => x.ReceptionTypeNameId,
                        principalTable: "DictionaryItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Addresses",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    AuthorId = table.Column<Guid>(nullable: false),
                    AuthorName = table.Column<string>(nullable: true),
                    UpdatedBuId = table.Column<Guid>(nullable: false),
                    UpdatedByName = table.Column<string>(nullable: true),
                    SortOrder = table.Column<int>(nullable: false),
                    Index = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    Region = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Street = table.Column<string>(nullable: true),
                    Building = table.Column<string>(nullable: true),
                    Room = table.Column<string>(nullable: true),
                    Latitude = table.Column<string>(nullable: true),
                    Longitude = table.Column<string>(nullable: true),
                    AddressTypeId = table.Column<Guid>(nullable: false),
                    PersonId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Addresses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Addresses_DictionaryItems_AddressTypeId",
                        column: x => x.AddressTypeId,
                        principalTable: "DictionaryItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Addresses_Persons_PersonId",
                        column: x => x.PersonId,
                        principalTable: "Persons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    AuthorId = table.Column<Guid>(nullable: false),
                    AuthorName = table.Column<string>(nullable: true),
                    UpdatedBuId = table.Column<Guid>(nullable: false),
                    UpdatedByName = table.Column<string>(nullable: true),
                    SortOrder = table.Column<int>(nullable: false),
                    CommentDate = table.Column<DateTime>(nullable: false),
                    Text = table.Column<string>(nullable: true),
                    PatientId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comments_Persons_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Persons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Comments_Persons_PatientId",
                        column: x => x.PatientId,
                        principalTable: "Persons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Polis",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    AuthorId = table.Column<Guid>(nullable: false),
                    AuthorName = table.Column<string>(nullable: true),
                    UpdatedBuId = table.Column<Guid>(nullable: false),
                    UpdatedByName = table.Column<string>(nullable: true),
                    SortOrder = table.Column<int>(nullable: false),
                    Number = table.Column<string>(nullable: true),
                    IssuedDate = table.Column<DateTime>(nullable: false),
                    IssuerCompanyId = table.Column<Guid>(nullable: false),
                    PersonGuid = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Polis", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Polis_DictionaryItems_IssuerCompanyId",
                        column: x => x.IssuerCompanyId,
                        principalTable: "DictionaryItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Polis_Persons_PersonGuid",
                        column: x => x.PersonGuid,
                        principalTable: "Persons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TimePeriods",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    AuthorId = table.Column<Guid>(nullable: false),
                    AuthorName = table.Column<string>(nullable: true),
                    UpdatedBuId = table.Column<Guid>(nullable: false),
                    UpdatedByName = table.Column<string>(nullable: true),
                    SortOrder = table.Column<int>(nullable: false),
                    OnDate = table.Column<DateTime>(nullable: false),
                    Start = table.Column<TimeSpan>(nullable: false),
                    End = table.Column<TimeSpan>(nullable: false),
                    DoctorId = table.Column<Guid>(nullable: false),
                    PeriodTypeId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TimePeriods", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Doctors_WorkTime",
                        column: x => x.DoctorId,
                        principalTable: "Persons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TimePeriods_DictionaryItems_PeriodTypeId",
                        column: x => x.PeriodTypeId,
                        principalTable: "DictionaryItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_AddressTypeId",
                table: "Addresses",
                column: "AddressTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_PersonId",
                table: "Addresses",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_AuthorId",
                table: "Comments",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_PatientId",
                table: "Comments",
                column: "PatientId");

            migrationBuilder.CreateIndex(
                name: "IX_DictionaryItems_DictionaryId",
                table: "DictionaryItems",
                column: "DictionaryId");

            migrationBuilder.CreateIndex(
                name: "IX_Persons_PhotosId",
                table: "Persons",
                column: "PhotosId");

            migrationBuilder.CreateIndex(
                name: "IX_Persons_PartnerId",
                table: "Persons",
                column: "PartnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Persons_SourceOfInformationGuid",
                table: "Persons",
                column: "SourceOfInformationGuid");

            migrationBuilder.CreateIndex(
                name: "IX_Persons_PassportId",
                table: "Persons",
                column: "PassportId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Polis_IssuerCompanyId",
                table: "Polis",
                column: "IssuerCompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Polis_PersonGuid",
                table: "Polis",
                column: "PersonGuid");

            migrationBuilder.CreateIndex(
                name: "IX_ReceptionTypes_ReceptionTypeNameId",
                table: "ReceptionTypes",
                column: "ReceptionTypeNameId");

            migrationBuilder.CreateIndex(
                name: "IX_TimePeriods_DoctorId",
                table: "TimePeriods",
                column: "DoctorId");

            migrationBuilder.CreateIndex(
                name: "IX_TimePeriods_PeriodTypeId",
                table: "TimePeriods",
                column: "PeriodTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Addresses");

            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "Polis");

            migrationBuilder.DropTable(
                name: "ReceptionTypes");

            migrationBuilder.DropTable(
                name: "TimePeriods");

            migrationBuilder.DropTable(
                name: "Persons");

            migrationBuilder.DropTable(
                name: "PersonalFiles");

            migrationBuilder.DropTable(
                name: "DictionaryItems");

            migrationBuilder.DropTable(
                name: "Passports");

            migrationBuilder.DropTable(
                name: "Dictionaries");
        }
    }
}
