﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data_Access.Migrations.OxyIdentityDb
{
    public partial class UpdateToOxyUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Fio",
                table: "AspNetUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Fio",
                table: "AspNetUsers");
        }
    }
}
