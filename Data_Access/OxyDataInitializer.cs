﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Constants;
using Domain.Entities.Dictionaries;
using Domain.Helpers.StringHelpers;
using Serilog;

namespace Data_Access
{
    public class OxyDataInitializer
    {
        #region Properties

        private ILogger _logger;

        //private readonly Dictionary<int, Person> _persons = new Dictionary<int, Person>();

        private readonly Dictionary<int, Dictionary> _dictionaries = new Dictionary<int, Dictionary>
        {
            {0, new Dictionary
            {
                Id = Guid.Parse("59BC26A1-C057-416B-98F2-4E37ABE0A79E"), Title = DictionaryNameConstants.AddressDictionaryName, Name = DictionaryNameConstants.AddressDictionaryName.TranslitToEnglish()
            } },
            {1, new Dictionary
            {
                Id = Guid.Parse("1A9C78C3-46F7-4A6A-BA24-699DB5356C56"), Title = DictionaryNameConstants.ReceptionTypeName, Name = DictionaryNameConstants.ReceptionTypeName.TranslitToEnglish()
            } },
            {2, new Dictionary
            {
                Id = Guid.Parse("14D671A5-0828-483F-8484-626BE2D5DC2C"), Title = DictionaryNameConstants.SourceOfInformation, Name = DictionaryNameConstants.SourceOfInformation.TranslitToEnglish()
            } },
            {3, new Dictionary
            {
                Id = Guid.Parse("7A9C9560-C4F5-468D-A6F9-2A176AE58362"), Title = DictionaryNameConstants.InsuranceCompanies, Name = DictionaryNameConstants.InsuranceCompanies.TranslitToEnglish()
            } },
            {4, new Dictionary
            {
                Id = Guid.Parse("5CB93683-3F8D-4271-BD64-0F29505A22A2"), Title = DictionaryNameConstants.TimePeriodType, Name = DictionaryNameConstants.TimePeriodType.TranslitToEnglish()
            } }
        };
        private readonly Dictionary<int, DictionaryItem> _dictionary = new Dictionary<int, DictionaryItem>
        {
            // Справочник значений типов адресов
            {0, new DictionaryItem{ DictionaryId = Guid.Parse("59BC26A1-C057-416B-98F2-4E37ABE0A79E"), DictionaryShortValue = "Регистрация", DictionaryFullValue = "Адрес регистрации" } },
            {1, new DictionaryItem{ DictionaryId = Guid.Parse("59BC26A1-C057-416B-98F2-4E37ABE0A79E"), DictionaryShortValue = "Проживания", DictionaryFullValue = "Адрес проживания" } },
            {2, new DictionaryItem{ DictionaryId = Guid.Parse("59BC26A1-C057-416B-98F2-4E37ABE0A79E"), DictionaryShortValue = "Рабочий", DictionaryFullValue = "Адрес по месту работы" } },
            {3, new DictionaryItem{ DictionaryId = Guid.Parse("59BC26A1-C057-416B-98F2-4E37ABE0A79E"), DictionaryShortValue = "Доп.", DictionaryFullValue = "Дополнительный адрес" } },
            // Справочник значений видов приема у врача
            {4, new DictionaryItem{ DictionaryId = Guid.Parse("1A9C78C3-46F7-4A6A-BA24-699DB5356C56"), DictionaryShortValue = "Первичный", DictionaryFullValue = "Первичный прием" } },
            {5, new DictionaryItem{ DictionaryId = Guid.Parse("1A9C78C3-46F7-4A6A-BA24-699DB5356C56"), DictionaryShortValue = "Повторный", DictionaryFullValue = "Повторный прием" } },
            {6, new DictionaryItem{ DictionaryId = Guid.Parse("1A9C78C3-46F7-4A6A-BA24-699DB5356C56"), DictionaryShortValue = "Консультация", DictionaryFullValue = "Консультация специалиста" } },
            {7, new DictionaryItem{ DictionaryId = Guid.Parse("1A9C78C3-46F7-4A6A-BA24-699DB5356C56"), DictionaryShortValue = "Гинеколог", DictionaryFullValue = "Прием гинеколога" } },
            // Источники информации
            {8, new DictionaryItem{ DictionaryId = Guid.Parse("14D671A5-0828-483F-8484-626BE2D5DC2C"), DictionaryShortValue = "Друзья", DictionaryFullValue = "Рекомендация друзей" } },
            {9, new DictionaryItem{ DictionaryId = Guid.Parse("14D671A5-0828-483F-8484-626BE2D5DC2C"), DictionaryShortValue = "Реклама щит", DictionaryFullValue = "Уличная реклама" } },
            {10, new DictionaryItem{ DictionaryId = Guid.Parse("14D671A5-0828-483F-8484-626BE2D5DC2C"), DictionaryShortValue = "Интернет", DictionaryFullValue = "Реклама в интернете" } },
            // Страховые компании
            {11, new DictionaryItem{ DictionaryId = Guid.Parse("7A9C9560-C4F5-468D-A6F9-2A176AE58362"), DictionaryShortValue = "АСКО", DictionaryFullValue = "ПАО \"АСКО-СТРАХОВАНИЕ\"" } },
            // Типы рабочего времени
            {12, new DictionaryItem{ DictionaryId = Guid.Parse("5CB93683-3F8D-4271-BD64-0F29505A22A2"), DictionaryShortValue = "Прием", DictionaryFullValue = "Приемные часы", SortOrder = 0} },
            {13, new DictionaryItem{ DictionaryId = Guid.Parse("5CB93683-3F8D-4271-BD64-0F29505A22A2"), DictionaryShortValue = "Обед", DictionaryFullValue = "Обеденные часы", SortOrder = 1 } },
            {14, new DictionaryItem{ DictionaryId = Guid.Parse("5CB93683-3F8D-4271-BD64-0F29505A22A2"), DictionaryShortValue = "Тех. пер.", DictionaryFullValue = "Технический перерыв", SortOrder = 2 } },
            {15, new DictionaryItem{ DictionaryId = Guid.Parse("5CB93683-3F8D-4271-BD64-0F29505A22A2"), DictionaryShortValue = "Совещания", DictionaryFullValue = "Время на совещания", SortOrder = 3 } },
            {16, new DictionaryItem{ DictionaryId = Guid.Parse("5CB93683-3F8D-4271-BD64-0F29505A22A2"), DictionaryShortValue = "Отсутствует", DictionaryFullValue = "Не рабочее время", SortOrder = 4 } },
        };

        #endregion

        public static void Initialize(OxyDataContext context, ILogger log)
        {

            var init = new OxyDataInitializer();
            init.Seed(context, log);
        }

        private void Seed(OxyDataContext context, ILogger logger)
        {
            _logger = logger ?? throw new NullReferenceException(nameof(logger));
            context.Database.EnsureCreated();

            if (context.Dictionaries.Any()) return;

            // Заполняем справочники начальными значениями
            SeedDictionaries(context);
        }

        /// <summary>
        /// Заполняем справочники начальными значениями
        /// </summary>
        /// <param name="context">Контекст основной базы данных</param>
        private void SeedDictionaries(OxyDataContext context)
        {
            context.DictionaryItems.RemoveRange(context.DictionaryItems);
            context.Dictionaries.RemoveRange(context.Dictionaries);

            _logger.Information("Заполняем таблицу со словарями");
            context.AddRange(_dictionaries.Values);

            _logger.Information("Заполняем значения для словарей");
            context.AddRange(_dictionary.Values);

            context.SaveChanges();
        }
    }
}