﻿using Domain.Entities.Common;
using Domain.Entities.Dictionaries;
using Domain.Entities.Doctors;
using Domain.Entities.Patients;
using Microsoft.EntityFrameworkCore;

namespace Data_Access
{
    /// <inheritdoc />
    /// <summary>
    /// Контекст, для работы с базой данных Postgres.
    /// </summary>
    public class OxyDataContext : DbContext
    {
        public OxyDataContext(DbContextOptions<OxyDataContext> options) : base(options) { }

        #region DbSets Таблицы БД

        #region Данные о людях

        /// <summary>
        /// Общие данные о человеке
        /// </summary>
        public DbSet<Person> Persons { get; set; }

        /// <summary>
        /// Паспортные данные
        /// </summary>
        public DbSet<Passport> Passports { get; set; }

        /// <summary>
        /// Адреса
        /// </summary>
        public DbSet<Address> Addresses { get; set; }

        /// <summary>
        /// Пациенты клиники
        /// </summary>
        public DbSet<Patient> Patients { get; set; }

        /// <summary>
        /// Доноры
        /// </summary>
        public DbSet<Donor> Donors { get; set; }

        /// <summary>
        /// Доктора
        /// </summary>
        public DbSet<Doctor> Doctors { get; set; }

        #endregion

        #region Справочники и все, что с ними связано

        /// <summary>
        /// Справочники
        /// </summary>
        public DbSet<Dictionary> Dictionaries { get; set; }

        /// <summary>
        /// Общие справочники
        /// </summary>
        public DbSet<DictionaryItem> DictionaryItems { get; set; }

        /// <summary>
        /// Типы приемов пациентов
        /// </summary>
        public DbSet<ReceptionType> ReceptionTypes { get; set; }

        /// <summary>
        /// Комментарии
        /// </summary>
        public DbSet<NamedComment> Comments { get; set; }

        #endregion

        #region Common entities

        public DbSet<TimePeriod> TimePeriods { get; set; }

        #endregion

        #endregion

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(typeof(OxyDataContext).Assembly);
        }
    }
}
