﻿using Domain.Entities.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Data_Access.Configuration
{
    public class TimePeriodConfiguration : IEntityTypeConfiguration<TimePeriod>
    {
        public void Configure(EntityTypeBuilder<TimePeriod> builder)
        {
            builder.ToTable("TimePeriods");

            builder.HasOne(x => x.Doctor)
                .WithMany(d => d.WorkTimes)
                .HasForeignKey(f => f.DoctorId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FK_Doctors_WorkTime");
        }
    }
}