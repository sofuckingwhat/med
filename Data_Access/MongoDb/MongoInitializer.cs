﻿using Domain.ViewModels.Patients;
using Microsoft.EntityFrameworkCore.Internal;
using MongoDB.Driver;

namespace Data_Access.MongoDb
{
    public class MongoInitializer
    {
        private readonly IMongoCollection<PatientDetailViewModel> _patients;

        public MongoInitializer(MongoContextBase context)
        {
            _patients = context.Patients;
        }

        public void Init()
        {
            var patients = _patients.Find(patientMongo => true).ToList();

            if (patients.Any())
            { return; }

            SeedPatients();
        }

        private static void SeedPatients()
        {
            var patient = new PatientDetailViewModel();

        }
    }
}