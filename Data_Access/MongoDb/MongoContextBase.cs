﻿using Domain.Configurations;
using Domain.ViewModels.Doctors;
using Domain.ViewModels.Patients;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Data_Access.MongoDb
{
    /// <summary>
    /// Контекст MongoDb
    /// </summary>
    public class MongoContextBase
    {
        #region Private properties

        private readonly IMongoDatabase _database;

        #endregion

        #region Public properties

        /// <summary>
        /// Коллекция пациентов клиники
        /// </summary>
        public IMongoCollection<PatientDetailViewModel> Patients => _database.GetCollection<PatientDetailViewModel>(nameof(PatientDetailViewModel));

        /// <summary>
        /// Коллекция докторов клиники
        /// </summary>
        public IMongoCollection<DoctorDetailDto> Doctors => _database.GetCollection<DoctorDetailDto>(nameof(DoctorDetailDto));

        #endregion

        public MongoContextBase(IOptions<MongoSettings> mongo)
        {
            var client = new MongoClient(mongo.Value.ConnectionString);
            _database = client.GetDatabase(mongo.Value.PatientsDb);
        }


    }
}
