﻿using identity_infrastructure.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Data_Access.Identity
{
    public class OxyIdentityDbContext : IdentityDbContext<OxyUser>
    {
        public OxyIdentityDbContext(DbContextOptions<OxyIdentityDbContext> options) : base(options)
        {

        }
    }
}
