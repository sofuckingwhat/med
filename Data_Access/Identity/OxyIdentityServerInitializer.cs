﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using identity_infrastructure.InMemory;
using identity_infrastructure.Models;
using identity_infrastructure.Resources;
using IdentityModel;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Data_Access.Identity
{
    /// <summary>
    /// Инициализация данных Identity Server
    /// </summary>
    public class OxyIdentityServerInitializer
    {
        #region Properties

        private static ConfigurationDbContext _context;
        private static UserManager<OxyUser> _userManager;
        private static ILogger<OxyIdentityServerInitializer> _logger;
        private static IConfiguration _config;
        #endregion

        public static async Task Initialize(IServiceScope scope, IConfiguration config)
        {
            scope.ServiceProvider.GetRequiredService<PersistedGrantDbContext>().Database.Migrate();
            scope.ServiceProvider.GetRequiredService<ConfigurationDbContext>().Database.Migrate();
            scope.ServiceProvider.GetRequiredService<OxyIdentityDbContext>().Database.Migrate();

            _context = scope.ServiceProvider.GetRequiredService<ConfigurationDbContext>();
            _userManager = scope.ServiceProvider.GetRequiredService<UserManager<OxyUser>>();

            _logger = scope.ServiceProvider.GetRequiredService<ILogger<OxyIdentityServerInitializer>>();

            _config = config;

            await CreateRootUser();
            await CreateResources();
            await CreateClients();
            await CreateApiResources();
        }

        /// <summary>
        /// Создаем пользователя с максимальными правами
        /// </summary>
        /// <returns></returns>
        private static async Task CreateRootUser()
        {
            var user = await _userManager.FindByNameAsync("Root");
            if (user != null) return;

            user = new OxyUser
            {
                UserName = "Root",
                Fio = "Поважный Петр Александрович",
                Email = "povazhnyipa@oxy-center.ru",
                EmailConfirmed = true,
                IsAdmin = true,
                IsOwner = true
            };

            _logger.LogDebug("Начинаю создание пользователя Root");
            var identityResult = await _userManager.CreateAsync(user, "Iichmwdb2019");

            if (identityResult.Succeeded)
            {
                _logger.LogDebug("Пользователь Root создан успешно");
                identityResult = await _userManager.AddClaimsAsync(user, new List<Claim>()
                {
                    new Claim(JwtClaimTypes.Name, user.UserName),
                    new Claim(JwtClaimTypes.Email, user.Email),
                    new Claim(OxyConstantsNames.Permission, "settings"),
                    new Claim(OxyConstantsNames.Permission, "settings.dictionary"),
                    new Claim(OxyConstantsNames.Permission, "settings.dictionary.write"),
                    new Claim(OxyConstantsNames.Fio, user.Fio),
                    new Claim(OxyConstantsNames.Position, "Администратор системы МИС"),
                    new Claim(OxyConstantsNames.Company, "Oxy-clinic"),
                    OxyClaims.Root,
                    OxyClaims.PermissionsAllAllow
                });
            }
            else
            {
                _logger.LogError("Ошибка создания пользователя Root");
            }

            if (!identityResult.Succeeded)
            {
                _logger.LogError("Ошибка создания прав пользователя Root");
            }
        }

        /// <summary>
        /// Инициализируем Resources
        /// </summary>
        /// <returns></returns>
        private static async Task CreateResources()
        {
            if (!_context.IdentityResources.Any())
            {
                _context.IdentityResources.AddRange(ResourcesFactory.Get().Select(r => r.ToEntity()));
            }

            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Инициализируем клиентские приложения
        /// </summary>
        /// <returns></returns>
        private static async Task CreateClients()
        {
            if (!_context.Clients.Any())
            {
                _context.Clients.AddRange(ClientsFactory.Get(_config).Select(c => c.ToEntity()));
            }

            await _context.SaveChangesAsync();
        }

        private static async Task CreateApiResources()
        {
            if (!_context.ApiResources.Any())
            {
                _context.ApiResources.AddRange(ApiResourceFactory.Get().Select(a => a.ToEntity()));
            }

            await _context.SaveChangesAsync();
        }
    }
}