﻿using System.Reflection;
using Data_Access.Identity;
using Data_Access.Infrastructure;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Options;
using Microsoft.EntityFrameworkCore;

namespace Data_Access
{
    public class OxyContextFactory : DesignTimeDbContextFactoryBase<OxyDataContext>
    {
        public OxyContextFactory() : base("OxyPostgresDatabase", typeof(OxyDataContext).GetTypeInfo().Assembly.GetName().Name)
        {

        }

        protected override OxyDataContext CreateNewInstance(DbContextOptions<OxyDataContext> options)
        {
            return new OxyDataContext(options);
        }
    }

    public class OxyIdentityContextFactory : DesignTimeDbContextFactoryBase<OxyIdentityDbContext>
    {
        public OxyIdentityContextFactory() : base("IdentityDataBase", typeof(OxyDataContext).GetTypeInfo().Assembly.GetName().Name)
        {

        }

        protected override OxyIdentityDbContext CreateNewInstance(DbContextOptions<OxyIdentityDbContext> options)
        {
            return new OxyIdentityDbContext(options);
        }
    }

    public class IdentityServerPFactory : DesignTimeDbContextFactoryBase<PersistedGrantDbContext>
    {
        public IdentityServerPFactory() : base("IdentityDataBase", typeof(OxyDataContext).GetTypeInfo().Assembly.GetName().Name)
        {

        }

        protected override PersistedGrantDbContext CreateNewInstance(DbContextOptions<PersistedGrantDbContext> options)
        {
            return new PersistedGrantDbContext(options, new OperationalStoreOptions());
        }
    }

    public class IdentityServerCFactory : DesignTimeDbContextFactoryBase<ConfigurationDbContext>
    {
        public IdentityServerCFactory() : base("IdentityDataBase", typeof(OxyDataContext).GetTypeInfo().Assembly.GetName().Name)
        {

        }

        protected override ConfigurationDbContext CreateNewInstance(DbContextOptions<ConfigurationDbContext> options)
        {
            return new ConfigurationDbContext(options, new ConfigurationStoreOptions());
        }
    }
}
