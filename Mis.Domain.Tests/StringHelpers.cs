using Domain.Helpers.Address;
using Domain.Helpers.StringHelpers;
using Domain.ViewModels.Common;
using Xunit;

namespace Mis.Domain.Tests
{
    public class StringHelpers
    {
        [Fact]
        public void TranslitToLatinFromRussian()
        {
            var result = "���� �������� �������".TranslitToEnglish();

            Assert.Equal("tipy-pochtovyx-adresov", result);
        }

        [Fact]
        public void CheckEmptyAddressDto()
        {
            var emptyAddress = new AddressDto();

            Assert.True(emptyAddress.AddressIsNullOrEmpty());
        }

        [Fact]
        public void CheckNotEmptyAddressDto()
        {
            var address = new AddressDto
            {
                Region = "Krasnodar"
            };
            Assert.False(address.AddressIsNullOrEmpty());
        }

    }
}
