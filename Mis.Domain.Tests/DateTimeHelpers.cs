﻿using System;
using Domain.Helpers.DateTime;
using Xunit;

namespace Mis.Domain.Tests
{
    public class DateTimeHelpers
    {
        [Fact]
        public void DateTimeToHtmlInputDate()
        {
            var date = new DateTime(1984, 12, 31).ToHtmlInputDateFormat();

            Assert.Equal("1984-12-31", date);
        }
    }
}